/*
This section documents a collection of algorithms used for the representation 
of the object modeland view planning.
View planning is mainly about how to choose subsequent views to capture 
more new images step by step.
*/

#ifndef _POSE_PLANNING_H_
#define _POSE_PLANNING_H_

//System header files

//C++ header files
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <string>

//Third Party header files
#include "opencv2/opencv.hpp"
#include "pcl/io/pcd_io.h"
#include "pcl/point_types.h"
#include "pcl/visualization/cloud_viewer.h"
#include "pcl/visualization/pcl_visualizer.h"
#include "pcl/features/normal_3d.h"
#include "pcl/registration/icp.h"
#include "pcl/registration/icp_nl.h"
#include "pcl/registration/transforms.h"
#include "pcl/kdtree/kdtree_flann.h"
#include "pcl/common/common.h"

#include "pcl/registration/ndt.h"
#include "pcl/filters/approximate_voxel_grid.h"
#include "pcl/filters/statistical_outlier_removal.h"

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

struct BttmChgSol{
  int index_grasp_side;
  int index_set_side;
  Eigen::Vector3f direction_grasp;
  Eigen::Vector3f direction_set;
};

/**********
 * To obtain a oriented bounding box which is parallel to the XY plane for all points
 * @param ptr_pcl: all the points
 * @param bbox_centroid: the centroid position (x, y, z)
 * @param bbox_directions: the three directions of the bounding box
 * @param length_1st: the length of the bounding box in the direction of bbox_directions.col(0)
 * @param length_2nd: the length of the bounding box in the direction of bbox_directions.col(1)
 * @param length_3rd: the length of the bounding box in the direction of bbox_directions.col(2)
**********/
bool obtain_oriented_bounding_box_on_XY_plane(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl,
                                              Eigen::Vector3f &bbox_centroid,
                                              Eigen::Matrix3f &bbox_directions,
                                              float &length_1st, float &length_2nd, float &length_3rd);
                                              
/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the grasp difficulties for the gripper to grasp the 
 * modeling object from 5 bounding box sides in 6 different 
 * ways(-x side, +x side, -y side, +y side, +z side in x direction, +z side in y direction)
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return: a vector in which each entry evaluates the grasp difficulty for the gripper to grasp 
 *          the modeling object from a pre-defined bounding box side; the object can not be graped 
 *          from the pre-defined bounding box side if its entry value is negative.
**********/
std::vector<float> evaluate_grasp_difficulty_for_five_bounding_box_sides(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                         float &length_x, float &length_y, float &length_z);

/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the push difficulties for the gripper after using 
 * the other 5 bounding box sides (+x, -x, +y, -y, +z) above the table as the bottom sides
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return: a vector in which each entry evaluates the push difficulty for the gripper 
 *   after using the other 5 bounding box sides (+x, -x, +y, -y, +z) above the table 
 *   as the bottom sides; the object can not be pushed from the pre-defined bounding box 
 *   side if its entry value is zero.
**********/
std::vector<float> evaluate_push_difficulty_for_using_other_five_sides_as_bottom(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                                 float &length_x, float &length_y, float &length_z);
                                                                         
/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the stand stabilities of the modeling 
 * object if it stands on the table with 5 different bounding box sides (+x side, 
 * -x side, +y side, -y side, +z side) as the bottom sides.
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return: a vector in which each entry evaluates the stand stability if the modeling 
 *          object stands with a pre-defined bounding box side as the bottom side; the object can not 
 *          stand on the table with the pre-defined bounding box side as the bottom side 
 *          if its entry value is negative.
**********/
std::vector<float> evaluate_stand_stability_for_five_bounding_box_sides(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                        float &length_x, float &length_y, float &length_z);
                                                                        
/**********
 * Generate all the grasp solutions for changing the bottom of the modeling object 
 * based on the bounding box in the bounding box coordinate system.
 * @return: return all the default grasp solutions
**********/
std::vector<BttmChgSol> generate_all_change_bottom_solutions_for_bounding_box();
                                                                                
/***********************************************************************
 * Evaluate the area factor using other five sides as the new bottoms, 
 * considering both the overlap for registration and the new unseen part for 
 * completing the object model.
 * @param path_current_root: the root working directory
 * @param index_rotation: the index of the 360 degrees rotation procedure to read all the data
 * @param transform_to_robot_cs: the transformation from the camera CS to the robot CS
 * @return : the area factor evaluation for each side
 **********************************************************************/
std::vector<float> evaluate_overlap_and_new_for_using_other_five_sides_as_bottom(const std::string &path_current_root,
                                                                                 const int &index_rotation,
                                                                                 const Eigen::Matrix4f &transform_to_robot_cs);

/***********************************************************************
 * Evaluate the quality factor using other five sides as the new bottoms;
 * check more details from the paper "Volumetirc Next-best-view Planning 
 * for 3D Object Reconstruction with Positioning Error"
 * @param path_current_root: the root working directory
 * @param index_rotation: the index of the 360 degrees rotation procedure to read all the data
 * @param transform_to_robot_cs: the transformation from the camera CS to the robot CS
 * @return : the quality factor evaluation for each side
 **********************************************************************/
std::vector<float> evaluate_quality_of_model_for_using_other_five_sides_as_bottom(const std::string &path_current_root,
                                                                                  const int &index_rotation,
                                                                                  const Eigen::Matrix4f &transform_to_robot_cs);
#endif  //  _POSE_PLANNING_H_
