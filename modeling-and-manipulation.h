/*
This section documents a collection of algorithms used for the autonomous object 
modeling and manipulation.
*/

#ifndef _MODELING_AND_MANIPULATION_H_
#define _MODELING_AND_MANIPULATION_H_

//System header files
#include <unistd.h>

//C++ header files
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <deque>

//Third Party header files
#include "opencv2/opencv.hpp"
#include "pcl/io/pcd_io.h"
#include "pcl/point_types.h"
#include "pcl/visualization/cloud_viewer.h"
#include "pcl/visualization/pcl_visualizer.h"
#include "pcl/features/normal_3d.h"
#include "pcl/registration/icp.h"
#include "pcl/registration/icp_nl.h"
#include "pcl/registration/transforms.h"
#include "pcl/kdtree/kdtree_flann.h"
#include "pcl/filters/voxel_grid.h"

#include "pcl/registration/ndt.h"
#include "pcl/filters/approximate_voxel_grid.h"
#include "pcl/filters/statistical_outlier_removal.h"
#include "pcl/filters/voxel_grid.h"

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//Project header files
#include "objects-localization.h"
#include "pose-planning.h"

struct MatchedPointPairs{
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src;
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_tgt;
};

// define a new point representation for < x, y, z, curvature >
class MyPointRepresentation: public pcl::PointRepresentation <pcl::PointNormal>
{
  using pcl::PointRepresentation<pcl::PointNormal>::nr_dimensions_;
 public:
  MyPointRepresentation ()
  {
    // define the number of dimensions
    nr_dimensions_ = 4;
  }

  // override the copyToFloatArray method to define our feature vector
  virtual void copyToFloatArray (const pcl::PointNormal &p, float * out) const
  {
    // < x, y, z, curvature >
    out[0] = p.x;
    out[1] = p.y;
    out[2] = p.z;
    out[3] = p.curvature;
  }
};

/***********************************************************************
* Update the object model based on the current perception
* @param path_current_root: the root working directory
* @param path_current_model: the file path to load a point cloud of an existing object model built gradually from all pervious perception
* @param path_current_perception: the file path to load a point cloud of the current perception
* @param index_perception: the index for each perception
* @return : returns the parameters of a manipulation to be executed by the robot manipulator
***********************************************************************/
std::vector<float> autonomous_modeling_and_manipulation(const std::string &path_current_root,
                                                        const std::string &path_current_model,
                                                        const std::string &path_current_perception,
                                                        const int &index_perception);
                                                         
/***********************************************************************
* Align two point clouds based on XYZ coordinates with ICP algorithm
* @param cloud_src: the source point cloud
* @param cloud_tgt: the target point cloud
* @return : returns a transformation matrix to align the two point clouds
***********************************************************************/
Eigen::Matrix4f align_point_clouds_using_icp(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_src, 
                                             const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_tgt);
                                             
/***********************************************************************
* Align two point clouds based on the point coordinate, normal and color information 
* with ICP algorithm, implemented by ourselves
* @param cloud_src: the source point cloud
* @param cloud_tgt: the target point cloud
* @param transform_initial: the initial transformation matrix for the ICP algorithm
* @param register_accuracy: return the average similarity distance of all matched 
*   points based on the returned transformation matrix
* @return : returns a transformation matrix to align the two point clouds
***********************************************************************/
Eigen::Matrix4f align_point_clouds_using_icp_plus_color(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_src, 
                                                        const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_tgt,
                                                        const Eigen::Matrix4f &transform_initial,
                                                        double &register_accuracy);
                                                        
/*
  Align two point clouds of partial object modelsbased on the point coordinate, normal and color information 
  with ICP algorithm, implemented by ourselves
  @param cloud_src: the source point cloud
  @param cloud_tgt: the target point cloud
  @param transform_initial: the initial transformation matrix for the ICP algorithm
  @param register_accuracy: return the average similarity distance of all matched 
    points based on the returned transformation matrix
  @return: returns a transformation matrix to align the two point clouds
*/
Eigen::Matrix4f align_point_clouds_using_icp_plus_color_between_models(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_src, 
                                                                       const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_tgt,
                                                                       const Eigen::Matrix4f &transform_initial,
                                                                       double &register_accuracy);
                                   
/***********************************************************************
* Align two point clouds based on XYZ coordinates with NDT algorithm
* @param cloud_src: the source point cloud
* @param cloud_tgt: the target point cloud
* @return : returns a transformation matrix to align the two point clouds
***********************************************************************/
Eigen::Matrix4f align_point_clouds_using_ndt(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_src, 
                                             const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_tgt);

/***********************************************************************
* Register two partial object point clouds captured from different viewpoints;
* before apply this function, the following 6 files should already be saved in the 
* $(path_current_root):
* object_point_cloud_in_scene_$(index_src).pcd, object_point_cloud_in_scene_$(index_tgt).pcd
* object_point_cloud_in_scene_$(index_src)_organized.pcd, object_point_cloud_in_scene_$(index_tgt)_organized.pcd
* object_point_cloud_in_scene_$(index_src)_organized.png, object_point_cloud_in_scene_$(index_tgt)_organized.png
*  
* @param path_current_root: the root working directory
* @param index_src: the index of the source view for registration
* @param index_tgt: the index of the target view for registration
* @return : the transformation matrix from the source view to the target view
***********************************************************************/                                        
Eigen::Matrix4f register_point_clouds_from_two_views(const std::string &path_current_root,
                                                     const int &index_src, const int &index_tgt);
                                                  
/***********************************************************************
* Detect the overlap between the two point clouds, sample points in both 
* point clouds, and generate mates to otain virtual matched point pairs;
* see more details in the paper "Multivew Registration for Large Data 
* Set".
* @param ptr_pcl_src: the source point cloud
* @param ptr_pcl_tgt: the target point cloud
* @param transfrom: the transformation from the source point cloud to the 
*   target point cloud
* @return : the generated virtual matched point pairs only for the points
*   subsampled in the source point cloud
***********************************************************************/
MatchedPointPairs generate_virtual_mates(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl_src,
                                         const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl_tgt,
                                         const Eigen::Matrix4f &transform);
                                         
/***********************************************************************
 * align the point cloud in the current view to both its left and right 
 * views.
 * @param matched_pairs_left: matched point pairs in the current point 
 *   cloud to the point cloud in the left view
 * @param matched_pairs_right: matched point pairs in the current point
 *   cloud to the point cloud in the right view
 * @param pose_left: the pose of the point cloud in the left view
 * @param pose_right: the pose of the point cloud in the right view
 * @return : the refined pose of the point cloud in the current view
 **********************************************************************/
Eigen::Matrix4f align_point_cloud_to_left_and_right_neighboring_views(const MatchedPointPairs &matched_pairs_left,  
                                                                      const MatchedPointPairs &matched_pairs_right,
                                                                      const Eigen::Matrix4f &pose_left,
                                                                      const Eigen::Matrix4f &pose_right);
                                             
/***********************************************************************
* At the end of the 360 degrees rotation modeling procedure, for all the 
* object point clouds captured in this 360 degrees rotation, globally 
* optimize all the registration between the point cloud and its 
* neighboring point cloud.
* @param transforms_pairwise: the initial pairwise transformation matrices 
*   and each indicates the transformtion from the (n-1)th object point cloud 
*   to the nth object point cloud; if n = 0, suppose N is the toal number of 
*   the object point clouds, then the matrix indicates the transformation 
*   from the Nth object point cloud to the 1st object point cloud
* @param ptr_obj_pcls: all the object point clouds
* @return : all the transformation matrice for all point clouds, using 
*   the coordinate system of the last point cloud (the nth point cloud) 
*   as the base coordinate system
***********************************************************************/
std::vector<Eigen::Matrix4f> optimize_globally_registration_in_360_rotation(const std::vector<Eigen::Matrix4f> &transforms_pairwise,
                                                                            const std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> &ptr_obj_pcls);

/***********************************************************************
 * Merge the partial object model from the current 360 degrees rotation 
 * procedure to the object model after all previous 360 degrees rotation 
 * procedures;
 * The transformation from the object model in the last 360 degree rotation 
 * to the partial object model in the current 360 degree rotation is saved;
 * The merged object model is also saved.
 * @param path_current_root: the root working directory
 * @param model_index_src: the source object model index
 * @param model_index_tgt: the target object model index
 * @param transform_estimated: the estimated transformation from the source 
 *   object model to the target object model;
 * @return : the accurate transformation from the source object model to
 *   the target object model
***********************************************************************/
Eigen::Matrix4f register_models_incrementally_between_360_rotations(const std::string &path_current_root,
                                                                    const int &model_index_src,
                                                                    const int &model_index_tgt,
                                                                    const Eigen::Matrix4f &transform_estimated);
#endif //_MODELING_AND_MANIPULATION_H_
