#include "modeling-and-manipulation.h"

//#define VISUALIZE_AMM
//#define VISUALIZE_ALL
//#define VISUALIZE_SINGLE

std::vector<float> autonomous_modeling_and_manipulation(const std::string &path_current_root,
                                                        const std::string &path_current_model,
                                                        const std::string &path_current_perception,
                                                        const int &index_perception)
{
  std::vector<float> params_manipulation(100, 0.0f);

  #ifdef VISUALIZE_AMM
  pcl::visualization::PCLVisualizer *pcd_viewer;
  pcd_viewer = new pcl::visualization::PCLVisualizer("Autonomous Object Modeling");
  pcd_viewer->addCoordinateSystem(1.0);
  pcd_viewer->initCameraParameters();
  #endif
 
  #if defined(VISUALIZE_AMM) && defined(VISUALIZE_ALL)
  int viewport1, viewport2, viewport3, viewport4;
  pcd_viewer->createViewPort(0.0, 0.0, 0.5, 0.5, viewport1);
  pcd_viewer->createViewPort(0.5, 0.0, 1.0, 0.5, viewport2);
  pcd_viewer->createViewPort(0.0, 0.5, 0.5, 1.0, viewport3);
  pcd_viewer->createViewPort(0.5, 0.5, 1.0, 1.0, viewport4);
  #endif
  
  //candidate colors to show the results
  unsigned char colors[100][3];
  colors[0][0] = colors[0][1] = colors[0][2] = 0;
  for(int i = 1; i < 100; ++i)
  {
	colors[i][0] = (unsigned char)rand()%256;
	colors[i][1] = (unsigned char)rand()%256;
	colors[i][2] = (unsigned char)rand()%256;
  }
  colors[0][0] = 0;   colors[0][1] = 0;   colors[0][2] = 0;
  colors[2][0] = 0;   colors[2][1] = 255; colors[2][2] = 0; //green
  colors[3][0] = 0;   colors[3][1] = 0;   colors[3][2] = 255;  //red
  colors[4][0] = 255; colors[4][1] = 0;   colors[4][2] = 0;  //blue
  
  /*****************************************************************************
   * STEP1: load the point cloud of the scene from the current perception
   ****************************************************************************/
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_cur_scene(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_current_perception, *ptr_cur_scene);

  #if defined(VISUALIZE_AMM) && defined(VISUALIZE_ALL)
  pcd_viewer->addPointCloud(ptr_cur_scene, "current scene", viewport1);
  pcd_viewer->spinOnce(1000);
  #endif
  #if defined(VISUALIZE_AMM) && defined(VISUALIZE_SINGLE)
  pcd_viewer->addPointCloud(ptr_cur_scene, "current scene");
  pcd_viewer->spinOnce(1000);
  pcd_viewer->removePointCloud("current scene");
  #endif
  
  /*****************************************************************************
   * STEP2: separate the point cloud of the current observed object part from the background
   ****************************************************************************/
  //show the original scene in a bgr image
  cv::Mat img_bgr(ptr_cur_scene->height, ptr_cur_scene->width, CV_8UC3);
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  int index = i * ptr_cur_scene->width + j;
	  img_bgr.at<cv::Vec3b>(i, j)[0] = ptr_cur_scene->points[index].b;
	  img_bgr.at<cv::Vec3b>(i, j)[1] = ptr_cur_scene->points[index].g;
	  img_bgr.at<cv::Vec3b>(i, j)[2] = ptr_cur_scene->points[index].r;
	}
  }
  //cv::imshow("img_bgr", img_bgr);
  //cv::waitKey(1000);
  
  //find out the assumed table plane
  double dist_c = 0.01;
  std::vector<std::vector<bool> > table_plane = localization_for_table_plane(*ptr_cur_scene, dist_c);
  
  //show the table plane
  cv::Mat img_bgr_plane(ptr_cur_scene->height, ptr_cur_scene->width, CV_8UC3);
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  if(table_plane[i][j])
	  {
	    img_bgr_plane.at<cv::Vec3b>(i, j)[0] = 255;
	    img_bgr_plane.at<cv::Vec3b>(i, j)[1] = 0;
	    img_bgr_plane.at<cv::Vec3b>(i, j)[2] = 255;		  
	  }else
	  {
	    img_bgr_plane.at<cv::Vec3b>(i, j)[0] = 0;
	    img_bgr_plane.at<cv::Vec3b>(i, j)[1] = 0;
	    img_bgr_plane.at<cv::Vec3b>(i, j)[2] = 0;	
	  }
	}
  }
  cv::addWeighted(img_bgr_plane, 0.5, img_bgr, 0.5, 0.0,img_bgr_plane);
  //cv::imshow("img_bgr_plane", img_bgr_plane);
  //cv::waitKey(1000);
  
  //detect the table region, which includes both the table plane and 
  //objects on the table.
  std::vector<std::vector<bool> > table_region = detect_table_region(table_plane, ptr_cur_scene->height, ptr_cur_scene->width);
  
  //show the table region
  cv::Mat img_bgr_region(ptr_cur_scene->height, ptr_cur_scene->width, CV_8UC3); 
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  if(table_region[i][j])
	  {
	    img_bgr_region.at<cv::Vec3b>(i, j)[0] = 255;
	    img_bgr_region.at<cv::Vec3b>(i, j)[1] = 0;
	    img_bgr_region.at<cv::Vec3b>(i, j)[2] = 255;		  
	  }else
	  {
	    img_bgr_region.at<cv::Vec3b>(i, j)[0] = 0;
	    img_bgr_region.at<cv::Vec3b>(i, j)[1] = 0;
	    img_bgr_region.at<cv::Vec3b>(i, j)[2] = 0;	
	  }
	}
  } 
  cv::addWeighted(img_bgr_region, 0.5, img_bgr, 0.5, 0.0,img_bgr_region);
  //cv::imshow("img_bgr_region", img_bgr_region);
  //cv::waitKey(1000);
  
  //Segment the table region based on the connectivity to find out all 
  //the objects on the table.
  int num_segments;    //segment index from 1 to num_segments, 0 means the background
  std::vector<std::vector<int> > segment_indices =  detect_all_objects_on_table(table_region, table_plane, ptr_cur_scene->height, ptr_cur_scene->width, num_segments);
  
  //show the segments
  cv::Mat img_bgr_segments(ptr_cur_scene->height, ptr_cur_scene->width, CV_8UC3);
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  if(table_region[i][j] && table_plane[i][j])
	  {
        img_bgr_segments.at<cv::Vec3b>(i, j)[0] = 255;
        img_bgr_segments.at<cv::Vec3b>(i, j)[1] = 0;
        img_bgr_segments.at<cv::Vec3b>(i, j)[2] = 255;
	  }else
	  {
        img_bgr_segments.at<cv::Vec3b>(i, j)[0] = colors[segment_indices[i][j]][0];
        img_bgr_segments.at<cv::Vec3b>(i, j)[1] = colors[segment_indices[i][j]][1];
        img_bgr_segments.at<cv::Vec3b>(i, j)[2] = colors[segment_indices[i][j]][2];
	  }	  
	}
  } 
  cv::addWeighted(img_bgr_segments, 0.7, img_bgr, 0.3, 0.0,img_bgr_segments);
  cv::imshow("img_bgr_segments", img_bgr_segments);
  cv::waitKey(1000);
  
  //count the segment size
  std::vector<int> segment_size(num_segments+1);
  for(int i = 0; i < num_segments+1; ++i)
    segment_size[i] = 0;
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  ++segment_size[segment_indices[i][j]];
	}
  }
  std::cout<<"num_segments:"<<num_segments<<std::endl;
  for(int i = 0; i < num_segments+1; ++i)
    std::cout<<segment_size[i]<<"  ";
  std::cout<<std::endl;
  
  //find out the object segment index
  //simply choose the largest segment
  int index_object_seg = 1;
  for(int i = 1; i < num_segments+1; ++i)
    if(segment_size[i] > segment_size[index_object_seg])
      index_object_seg = i;
  std::cout<<"index_object_seg:"<<index_object_seg<<std::endl;
  
  /*********************************************************************
  * STEP3: update the object model with the current perception
  *********************************************************************/

  /*********************************************************************
  * STEP 3-A
  * initialize the new object model with the current observed object point cloud;
  * it is a un-organized point cloud right now;
  * the purpose is to finally merge the gradually built object model to this 
  * new object model.
  *********************************************************************/
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_new_model(new pcl::PointCloud<pcl::PointXYZRGBA>());
  ptr_new_model->points.clear();
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  int index = i * ptr_cur_scene->width + j;
	  if(segment_indices[i][j] == index_object_seg && !isnan(ptr_cur_scene->points[index].z))
	    ptr_new_model->points.push_back(ptr_cur_scene->points[index]);
    }
  }
  ptr_new_model->height = 1;
  ptr_new_model->width = ptr_new_model->points.size();
  ptr_new_model->is_dense = true;
    
  //filter all the noisy pixels
  pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor_object;
  sor_object.setInputCloud(ptr_new_model);
  sor_object.setMeanK(50);
  sor_object.setStddevMulThresh(3.0);
  sor_object.filter(*ptr_new_model);
  
  //save the object point cloud from each perception
  char path_object_point_cloud_backup[1024];
  sprintf(path_object_point_cloud_backup, "%s/object_point_cloud_in_scene_%03d.pcd", path_current_root.c_str(), index_perception);
  pcl::io::savePCDFileASCII(path_object_point_cloud_backup, *ptr_new_model); 
  
  /*********************************************************************
  * STEP 3-B
  * save the current observed object cloud in an organized way;
  * used for the registration based on the keypoint matching.
  *********************************************************************/
  int col_min, col_max, row_min, row_max;
  col_min = ptr_cur_scene->width - 1;
  col_max = 0;
  row_min = ptr_cur_scene->height - 1;
  row_max = 0;
  for(int i = 0; i < ptr_cur_scene->height; ++i)
  {
	for(int j = 0; j < ptr_cur_scene->width; ++j)
	{
	  int index = i * ptr_cur_scene->width + j;
	  if(segment_indices[i][j] == index_object_seg && !isnan(ptr_cur_scene->points[index].z))
	  {
		if(j < col_min) col_min = j;
		if(j > col_max) col_max = j;
		if(i < row_min) row_min = i;
		if(i > row_max) row_max = i;
      }
	}
  }
    
  //the current observed object cloud saved in its organized way
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_objt_cur_org_pcl(new pcl::PointCloud<pcl::PointXYZRGBA>());
  ptr_objt_cur_org_pcl->width = col_max - col_min + 1;
  ptr_objt_cur_org_pcl->height = row_max - row_min + 1;
  ptr_objt_cur_org_pcl->is_dense = false;
  ptr_objt_cur_org_pcl->points.clear();
  ptr_objt_cur_org_pcl->points.resize(ptr_objt_cur_org_pcl->width * ptr_objt_cur_org_pcl->height);
    
  cv::Mat img_bgr_object(ptr_objt_cur_org_pcl->height, ptr_objt_cur_org_pcl->width, CV_8UC3);
    
  for(int h = 0; h < ptr_objt_cur_org_pcl->height; ++h)
  {
	for(int w = 0; w < ptr_objt_cur_org_pcl->width; ++ w)
	{
      int index_object = h * ptr_objt_cur_org_pcl->width + w;
	  int index_scene = (h + row_min) * ptr_cur_scene->width + (w + col_min);
	  if(segment_indices[h+row_min][w+col_min] == index_object_seg && !isnan(ptr_cur_scene->points[index_scene].z))
      {
		ptr_objt_cur_org_pcl->points[index_object] = ptr_cur_scene->points[index_scene];
		img_bgr_object.at<cv::Vec3b>(h, w)[0] = ptr_objt_cur_org_pcl->points[index_object].b;
		img_bgr_object.at<cv::Vec3b>(h, w)[1] = ptr_objt_cur_org_pcl->points[index_object].g;
		img_bgr_object.at<cv::Vec3b>(h, w)[2] = ptr_objt_cur_org_pcl->points[index_object].r;
      }
	  else
      {
	    ptr_objt_cur_org_pcl->points[index_object].x = NAN;
		ptr_objt_cur_org_pcl->points[index_object].y = NAN;
		ptr_objt_cur_org_pcl->points[index_object].z = NAN;
		ptr_objt_cur_org_pcl->points[index_object].b = rand()%256;
		ptr_objt_cur_org_pcl->points[index_object].g = rand()%256;
		ptr_objt_cur_org_pcl->points[index_object].r = rand()%256;
		img_bgr_object.at<cv::Vec3b>(h, w)[0] = ptr_objt_cur_org_pcl->points[index_object].b;
		img_bgr_object.at<cv::Vec3b>(h, w)[1] = ptr_objt_cur_org_pcl->points[index_object].g;
		img_bgr_object.at<cv::Vec3b>(h, w)[2] = ptr_objt_cur_org_pcl->points[index_object].r;
	  }
	}
  }
	
  //save the organized object point cloud from each perception
  char path_object_point_cloud_organized_backup[1024];
  sprintf(path_object_point_cloud_organized_backup, "%s/object_point_cloud_in_scene_%03d_organized.pcd", path_current_root.c_str(), index_perception);
  pcl::io::savePCDFileASCII(path_object_point_cloud_organized_backup, *ptr_objt_cur_org_pcl);

  char path_object_bgr_img_backup[1024];
  sprintf(path_object_bgr_img_backup, "%s/object_point_cloud_in_scene_%03d_organized.png", path_current_root.c_str(), index_perception);
  cv::imwrite(path_object_bgr_img_backup, img_bgr_object);
  
  /*********************************************************************
  * STEP 3-C
  * Two ways to register the gradually built object model with the current 
  * object model from the currectly observed object point cloud;
  * try I first, if I doesnot work, try II(II also has two options).
  * I - registration based on the ASIFT keypoints
  * II - use ICP algorithms to compute the transformation matrix between the two point clouds,
  * two options:
  * (a) registration directly between the gradually built object model and 
  * the currently observed object point cloud;
  * (b) registration between the previously observed (n-1)th object point cloud and 
  * the currently observed nth object point cloud, and then use the transformation 
  * matrix result to merge the gradually built object model with the currently 
  * observed nth object point cloud.
  *********************************************************************/
  //transformation matrix from the object model coordinate system to the camera coordinate system in the current scene
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  
  //index_perception = 1 : do nothing, use ptr_new_model directly as the current object model
  //index_perception > 1 : need to merge the previous model
  if(index_perception > 1)
  {
    //read the manipulation information and decide the registration strategy
 	//read the manipulation information in the last scene
	char path_manipulate_last[1024];
    sprintf(path_manipulate_last, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception - 1); 
    std::ifstream cin_last_manipulate;
    cin_last_manipulate.open(path_manipulate_last);
	if(!cin_last_manipulate.is_open())
	  std::cout<<"Fail to open the file to obtain the mainpulation information in the last scene!\n"<<std::endl;
	float manipulate_type;
	cin_last_manipulate>>manipulate_type;
	cin_last_manipulate.close();
	
	//registration during the 360 rotation: the last manipulation is push or grasp to rotate the modeling object
	if(fabsf(manipulate_type - 1.0) < 0.001f || fabsf(manipulate_type - 3.0) < 0.001f)
    {    
	  //register the current point cloud with the point cloud in the last view
	  transform = register_point_clouds_from_two_views(path_current_root, index_perception-1, index_perception);

	  //save the pairwise transformation information between the last and current 
	  //object point clouds for future optimization
	  char path_transformation_backup[1024];
	  sprintf(path_transformation_backup, "%s/transformation_from_%03d_to_%03d.txt",
	          path_current_root.c_str(), index_perception-1, index_perception);
	  std::ofstream cout_transformation;
	  cout_transformation.open(path_transformation_backup);
	  if (!cout_transformation.is_open())
	    std::cout<<"Fail to open the file to save the transformation information!\n"<<std::endl;
	  cout_transformation<<transform<<std::endl;
	  cout_transformation.close();
	  
      /***************************************************************************
       * STEP 3-D
       * transform the previously gradually built object model to the new object model coordinate system;
       * merge the old and new modeling data together
       **************************************************************************/
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_update_model(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_current_model, *ptr_update_model);
      
      pcl::PointCloud<pcl::PointXYZRGBA> transformed_update_model;
      pcl::transformPointCloud(*ptr_update_model, transformed_update_model, transform);
      *ptr_new_model += transformed_update_model;	  
    }
    else if (fabsf(manipulate_type - 2.0) < 0.001f)
    //the first frame of a new 360 rotation registration
    //the last manipulation is grasp to change the standing bottom side of the modeling object 
    //only need to initialize the updated model: use ptr_new_model directly
    {
	  //do nothing, save ptr_new_model directly as the new updating model
	  std::cout<<"A new 360 degrees rotation for object modeling starts!"<<std::endl;
	  
	  /*****************************************************************
	  * save the pairwise transformation information between the first frame
	  * of the current 360 degrees rotation registration and the last frame 
	  * of the last 360 degrees rotation registration;
	  * this saved transformation information is used to register two partial 
	  * models from the last and current 360 degrees rotation 
	  * registration procedures.
	  ****************************************************************/
	  //Transformation information between 360 degrees rotation 
      //registration procedures is saved by Huitan during the manipulation.
	}
	else
	//the manipulation type should be only 1, 2 or 3 
	//0 indicates the end of the registration and does not go through here
	{
	  std::cout<<"ERROR: Incorrect Manipulation Type!"<<std::endl;
	  return params_manipulation;
	}
  }
  
  /*********************************************************************
  * STEP 3-E backup all the model information for the future use
  *********************************************************************/
  char path_update_model_backup[1024];
  sprintf(path_update_model_backup, "%s/object_model_backup_after_scene_%03d.pcd", path_current_root.c_str(), index_perception);
  pcl::io::savePCDFileASCII(path_update_model_backup, *ptr_new_model);
  //real time updating copy
  pcl::io::savePCDFileASCII(path_current_model, *ptr_new_model);
    
  /*********************************************************************
  * STEP 3-F transform the object model point cloud to the robot coordinate system
  *********************************************************************/
  //updated object model point cloud in the robot coordinate system
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
  Eigen::Matrix4f transform_to_robot_cs;
  transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                        -0.0303733,   0.628287,  -0.777389,   0.597786,
                        -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                 0,          0,          0,          1;                                
  std::cout<<"*****transform_to_robot_cs:*****"<<std::endl<<transform_to_robot_cs<<std::endl;
  pcl::transformPointCloud(*ptr_new_model, *ptr_model_robot_cs, transform_to_robot_cs);
  
  /*****************************************************************************
   * STEP 4
   * Generate the parameters for pushing or grasping
   ****************************************************************************/
   
  /*****************************************************************************
  * Strategy 4-1: if the manipulation is push, always push the same point;
  * (1) choose the leftmost point (according to the robot CS) in the first scene of push;
  * (2) always push this point in the last scenes.
  * (3) the push stops until the modeling object has been rotated by 360 degrees
  ****************************************************************************/
  
  //read the manipulation information in the last scene
  std::ifstream cin_last_manipulation;
  float manipulation_type = -1.0f;
  
  if (index_perception > 1)
  {
    char path_manipulation_last[1024];
    sprintf(path_manipulation_last, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception - 1); 
    cin_last_manipulation.open(path_manipulation_last);
    if(!cin_last_manipulation.is_open())
      std::cout<<"Fail to open the file to obtain the mainpulation information in the last scene!\n"<<std::endl;
    cin_last_manipulation>>manipulation_type;
  }
  
  /*********************************************************************
  * If it is the first frame of the 360 degrees rotation procedure 
  *********************************************************************/
  
  //save the index of the first frame for each 360 degrees rotation procedure
  if (index_perception == 1)
  {
    //create a new file to save the index
    char path_model_indices_backup[1024];
    sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
    std::ofstream cout_model_indices;
    cout_model_indices.open(path_model_indices_backup);
    if(!cout_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to save model indices!"<<std::endl;
      return params_manipulation;
    }
    
    cout_model_indices<<"1"<<std::endl;
    cout_model_indices<<"1 1"<<std::endl;
    
    cout_model_indices.close();
  }
  
  if (fabsf(manipulation_type - 2.0) < 0.001f)
  {
    //read the file which save all the previous indices information first
    char path_model_indices_backup[1024];
    sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
    std::ifstream cin_model_indices;
    cin_model_indices.open(path_model_indices_backup);
    if (!cin_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to read model indices information!"<<std::endl;
      return params_manipulation;
    }
    
    int num_rotations;
    std::vector<int> indices_begin, indices_end;
    indices_begin.clear();
    indices_end.clear();
    
    cin_model_indices>>num_rotations;
    indices_begin.resize(num_rotations);
    indices_end.resize(num_rotations);
    
    int tmp;
    for (int i = 0; i < num_rotations; ++i)
      cin_model_indices>>tmp>>indices_begin[i]>>tmp>>indices_end[i];
    
    cin_model_indices.close();
    
    //save all the previous and current indices information
    std::ofstream cout_model_indices;
    cout_model_indices.open(path_model_indices_backup);
    if (!cout_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to save model indices!"<<std::endl;
      return params_manipulation;
    }
    
    cout_model_indices<<num_rotations + 1<<std::endl;
    for (int i = 0; i < num_rotations; ++i)
    {
      cout_model_indices<<i+1<<" "<<indices_begin[i]<<std::endl;
      cout_model_indices<<i+1<<" "<<indices_end[i]<<std::endl;
    }
    
    cout_model_indices<<num_rotations+1<<" "<<index_perception<<std::endl;
    
    cout_model_indices.close();
  }
  
  //determine how to push the object
  if (index_perception == 1 || fabsf(manipulation_type - 2.0) < 0.001f)
  //index_perception == 1 : the beginning of the registration, starting from push
  //manipulation_type = 2 : the last manipulation is grasp to change the standing bottom side; the first frame of a new 360 rotation registration by grasp
  //mode codes are required to record the index information for each 360 degrees rotation procedure
  {
    pcl::NormalEstimation<pcl::PointXYZRGBA, pcl::Normal> norm_est;
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr ptr_search_tree(new pcl::search::KdTree<pcl::PointXYZRGBA> ());
    norm_est.setSearchMethod(ptr_search_tree);
    norm_est.setRadiusSearch(0.02);   //0.02m = 20mm
  
    //the normals of the target point cloud are fixed
    pcl::PointCloud<pcl::Normal>::Ptr ptr_model_robot_cs_normals(new pcl::PointCloud<pcl::Normal> ());  
    norm_est.setInputCloud(ptr_model_robot_cs);
    norm_est.compute(*ptr_model_robot_cs_normals);
  
    //compute the XYZ position of the center point for the current object model
    pcl::PointXYZ center_point(0.0f, 0.0f, 0.0f);
    std::cout<<center_point.x<<"  "<<center_point.y<<"  "<<center_point.z<<std::endl;
    int num_points_average = 0;
    for(int i = 0; i < ptr_model_robot_cs->points.size(); ++i)
    {
	  if(isnan(ptr_model_robot_cs->points[i].x))
	    continue;
	    
	  ++num_points_average;
	  center_point.x += ptr_model_robot_cs->points[i].x;
	  center_point.y += ptr_model_robot_cs->points[i].y;
	  center_point.z += ptr_model_robot_cs->points[i].z;
    }
	  
    if(num_points_average)
    {
	  center_point.x /= num_points_average;
	  center_point.y /= num_points_average;
	  center_point.z /= num_points_average;
    }
    std::cout<<"num_points_average:  "<<num_points_average<<std::endl;
    std::cout<<"Updated Center Point Position:  "<<center_point.x<<"  "<<center_point.y<<"  "<<center_point.z<<std::endl;
      
    //adjust the normal directions so that the point normals always point to the outside of the object model
    //the current strategy only works on convex objects or the convex 
    for(int i = 0; i < ptr_model_robot_cs->points.size(); ++i)
    {
	  if(isnan(ptr_model_robot_cs->points[i].x) || isnan(ptr_model_robot_cs_normals->points[i].normal_x))
	    continue;
	    
	  float dot_sign;
	  dot_sign = (center_point.x - ptr_model_robot_cs->points[i].x) * ptr_model_robot_cs_normals->points[i].normal_x + 
	             (center_point.y - ptr_model_robot_cs->points[i].y) * ptr_model_robot_cs_normals->points[i].normal_y +
	             (center_point.z - ptr_model_robot_cs->points[i].z) * ptr_model_robot_cs_normals->points[i].normal_z;
	  if(dot_sign > 0.0f)
	  {
        ptr_model_robot_cs_normals->points[i].normal_x = - ptr_model_robot_cs_normals->points[i].normal_x;
	    ptr_model_robot_cs_normals->points[i].normal_y = - ptr_model_robot_cs_normals->points[i].normal_y;
	    ptr_model_robot_cs_normals->points[i].normal_z = - ptr_model_robot_cs_normals->points[i].normal_z;
	  }
    }
  
	//0: stop; 1: push; 2: grasp
	params_manipulation[0] = 1.0f;
	
	//choose the leftmost point in the first scene (averaged point positions from num_points_push_total points)
	//to determine the intial push position and the initial push direction.
	
	//sort the cloud points from the left to the right
    for(int i = 0; i < ptr_model_robot_cs->points.size(); ++i)
    {
	  for(int j = i + 1; j < ptr_model_robot_cs->points.size(); ++j)
	  {
	    if(isnan(ptr_model_robot_cs->points[i].y) || isnan(ptr_model_robot_cs_normals->points[i].normal_y) ||
	       isnan(ptr_model_robot_cs->points[j].y) || isnan(ptr_model_robot_cs_normals->points[j].normal_y))
	      continue;
	  
	    if(ptr_model_robot_cs->points[i].y < ptr_model_robot_cs->points[j].y)
	    {
	      pcl::PointXYZRGBA tmp1;
	      tmp1 = ptr_model_robot_cs->points[i];
	      ptr_model_robot_cs->points[i] = ptr_model_robot_cs->points[j];
	      ptr_model_robot_cs->points[j] = tmp1;
	    
	      pcl::Normal tmp2;
	      tmp2 = ptr_model_robot_cs_normals->points[i];
	      ptr_model_robot_cs_normals->points[i] = ptr_model_robot_cs_normals->points[j];
	      ptr_model_robot_cs_normals->points[j] = tmp2;
	    }//if
	  
	  }//for
    }//for
    
    //since the points are sorted, simply check the 100 leftmost points
    //obtain the initial push position and direction
    const int num_points_push_total = 100;
    int num_push_points_true = 0;
    int index_pcd = -1;
    while(num_push_points_true < num_points_push_total)
    {
	  ++index_pcd;
	  if(isnan(ptr_model_robot_cs->points[index_pcd].y) || isnan(ptr_model_robot_cs_normals->points[index_pcd].normal_y))
	    continue;
	
	  params_manipulation[1] += ptr_model_robot_cs->points[index_pcd].x;
	  params_manipulation[2] += ptr_model_robot_cs->points[index_pcd].y;
	  params_manipulation[3] += ptr_model_robot_cs->points[index_pcd].z;
	  params_manipulation[4] += ptr_model_robot_cs_normals->points[index_pcd].normal_x;
	  params_manipulation[5] += ptr_model_robot_cs_normals->points[index_pcd].normal_y;
	  params_manipulation[6] += ptr_model_robot_cs_normals->points[index_pcd].normal_z;
	  ++num_push_points_true;
    }
  
    for(int i = 1; i <= 6; ++i)
      params_manipulation[i] /= num_push_points_true;
      
    //normalize the normal and set the z to 0, since it is only the rotation on the table plane (XY plane)
    float normal_len = sqrtf(params_manipulation[4] * params_manipulation[4] + params_manipulation[5] * params_manipulation[5]);
    params_manipulation[4] = params_manipulation[4] / normal_len;
    params_manipulation[5] = params_manipulation[5] / normal_len;
    params_manipulation[6] = 0.0f;
  
    std::cout<<"manipulation parameters:"<<std::endl;
    for(int i = 0; i <= 6; ++i)
      std::cout<<params_manipulation[i]<<"  ";
    std::cout<<std::endl;
    
    //save the push position for future use
    char path_push_position_backup[1024];
    sprintf(path_push_position_backup, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception);    
    std::ofstream cout_push;
    cout_push.open(path_push_position_backup);
	if(!cout_push.is_open())
	  std::cout<<"Fail to open the file to save the push positions!\n"<<std::endl;
	//save the manipulation information first
	//push or grasp
	cout_push<<params_manipulation[0]<<std::endl;
	//push or grasp parameters
	for(int i = 1; i <= 6; ++i)
	  cout_push<<params_manipulation[i]<<"  ";
	cout_push<<std::endl;
	//save how many degrees the object has been rotated by push
	cout_push<<"0"<<std::endl;
	cout_push.close();
	
    if (index_perception > 1)
      cin_last_manipulation.close();
    
    return params_manipulation;	
  }
  
  /*********************************************************************
  * If it is in the middle or at the end of the 360 degrees rotation procedure 
  *********************************************************************/
  
  //the last manipulation is push to rotate the modeling object  
  //based on the rotation angle, decide which manipulation to execute in the next step
  //check if the current status is during the 360 rotation registration or at the end of the 360 rotation registration
	
  //the last push position (x, y, z, 1.0)
  Eigen::Vector4f push_position_last;
  Eigen::Vector4f push_direction_last;
  float angle_rotated_last;
	  
  //read the last push information and display all the information
  cin_last_manipulation>>push_position_last(0)>>push_position_last(1)>>push_position_last(2);
  cin_last_manipulation>>push_direction_last(0)>>push_direction_last(1)>>push_direction_last(2);
  cin_last_manipulation>>angle_rotated_last;
  push_position_last(3) = 1.0f;  //a position vector
  push_direction_last(3) = 0.0f;  //a direction vector
	  
  std::cout<<"Push position in the last scene:"<<std::endl;
  std::cout<<push_position_last<<std::endl;
  std::cout<<push_direction_last<<std::endl;
  std::cout<<"angle rotated:"<<angle_rotated_last<<std::endl;
	  
  //compute how many degrees the object is rotated by from the last scene to the current scene
  Eigen::Vector4f x_normal_last(1.0f, 0.0f, 0.0f, 0.0f); 
  Eigen::Vector4f x_normal_cur = transform_to_robot_cs * transform * transform_to_robot_cs.inverse() * x_normal_last;
  float angle_rotated_cur = acos(x_normal_cur(0)) * 180.0 / 3.14159265f;
  std::cout<<"new angle rotated:"<<angle_rotated_cur<<std::endl;
  std::cout<<"total angle rotated:"<<(angle_rotated_last + angle_rotated_cur)<<std::endl;
  
  //save the index if it is the last frame for each 360 degrees rotation procedure
  if (!((angle_rotated_last + angle_rotated_cur) < 360.0f))
  {
    //read the file which save all the previous indices information first
    char path_model_indices_backup[1024];
    sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
    std::ifstream cin_model_indices;
    cin_model_indices.open(path_model_indices_backup);
    if (!cin_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to read model indices information!"<<std::endl;
      return params_manipulation;
    }
    
    int num_rotations;
    std::vector<int> indices_begin, indices_end;
    indices_begin.clear();
    indices_end.clear();
    
    cin_model_indices>>num_rotations;
    indices_begin.resize(num_rotations);
    indices_end.resize(num_rotations);
    
    int tmp;
    for (int i = 0; i <= num_rotations-2; ++i)
      cin_model_indices>>tmp>>indices_begin[i]>>tmp>>indices_end[i];
    //the current rotation only has the begin index
    cin_model_indices>>tmp>>indices_begin[num_rotations-1];
    
    cin_model_indices.close();
    
    //save all the previous and current indices information
    std::ofstream cout_model_indices;
    cout_model_indices.open(path_model_indices_backup);
    if (!cout_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to save model indices!"<<std::endl;
      return params_manipulation;
    }
    
    indices_end[num_rotations-1] = index_perception;
    
    cout_model_indices<<num_rotations<<std::endl;
    for (int i = 0; i < num_rotations; ++i)
    {
      cout_model_indices<<i+1<<" "<<indices_begin[i]<<std::endl;
      cout_model_indices<<i+1<<" "<<indices_end[i]<<std::endl;
    }
    
    cout_model_indices.close();
  }
	  
  //based on the rotation angle, decide which manipulation to execute in the next step
  //check if the current status is during the 360 rotation registration or at the end of the 360 rotation registration
	  
  if ((angle_rotated_last + angle_rotated_cur) < 360.0f)
  //the current status is IN THE MIDDLE of the 360 rotation registration
  //continue the push until the modeling object is rotated by 360 degrees
  { 
    //obtain the new push position by transformation
    Eigen::Vector4f push_position_cur = transform_to_robot_cs * transform * transform_to_robot_cs.inverse() * push_position_last;
	std::cout<<"Push position in the current scene:"<<std::endl<<push_position_cur<<std::endl;
	    
	//obtain the new push direction by transformation
	Eigen::Vector4f push_direction_cur = transform_to_robot_cs * transform * transform_to_robot_cs.inverse() * push_direction_last;
    std::cout<<"Push direction in the current scene:"<<std::endl;
    //normalize the normal and set the z to 0, since it is only the rotation on the table plane (XY plane)
    float normal_length = sqrtf(push_direction_cur(0) * push_direction_cur(0) + push_direction_cur(1) * push_direction_cur(1));
    push_direction_cur(0) = push_direction_cur(0) / normal_length;
    push_direction_cur(1) = push_direction_cur(1) / normal_length;
    push_direction_cur(2) = 0.0f;
    std::cout<<"By Transformation: "<<std::endl<<push_direction_cur<<std::endl; 

    //save the push position for future use
    char path_push_position_backup[1024];
    sprintf(path_push_position_backup, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception);    
    std::ofstream cout_push;
    cout_push.open(path_push_position_backup);
    if(!cout_push.is_open())
      std::cout<<"Fail to open the file to save the push positions!\n"<<std::endl;
    //push or grasp
    cout_push<<"1.0"<<std::endl;
    //push or grasp parameters
    cout_push<<push_position_cur(0)<<"    "<<push_position_cur(1)<<"    "<<push_position_cur(2)<<"    ";
    cout_push<<push_direction_cur(0)<<"    "<<push_direction_cur(1)<<"    "<<push_direction_cur(2)<<std::endl;
    //save how many degrees the object has been rotated by push
    cout_push<<(angle_rotated_last + angle_rotated_cur)<<std::endl;
    cout_push.close();

    //0.0: stop; 1.0: push; 2.0:grasp.
    params_manipulation[0] = 1.0f;
    params_manipulation[1] = push_position_cur(0);
    params_manipulation[2] = push_position_cur(1);
    params_manipulation[3] = push_position_cur(2);
    params_manipulation[4] = push_direction_cur(0);
    params_manipulation[5] = push_direction_cur(1);
    params_manipulation[6] = push_direction_cur(2);
  } // the rotation angle < 360
  else 
  //the modeling object has been rotated by 360 degrees
  //the current status is AT THE END of the 360 rotation registration 
  {
    /*******************************************************************
    * For the partial object model in the current 360 degrees rotation 
    * procedure, firstly assign the coordinate system of the last view 
    * as the coordinate system of the object model, for the reason that 
    * all the pose planing work is going to be done in this coordinate 
    * system; then, optimize the poses of all the object point clouds 
    * from different views to obtain a more accurate partial object model
    *******************************************************************/
    
    //read the begin and end indices first
    char path_model_indices_backup[1024];
    sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
    std::ifstream cin_model_indices;
    cin_model_indices.open(path_model_indices_backup);
    if (!cin_model_indices.is_open())
    {
      std::cout<<"Error: fail to open the text file to read model indices information!"<<std::endl;
      return params_manipulation;
    }
    
    int num_rotations;
    std::vector<int> indices_begin, indices_end;
    indices_begin.clear();
    indices_end.clear();
    
    cin_model_indices>>num_rotations;
    indices_begin.resize(num_rotations);
    indices_end.resize(num_rotations);
    
    int tmp;
    for (int i = 0; i < num_rotations; ++i)
      cin_model_indices>>tmp>>indices_begin[i]>>tmp>>indices_end[i];
    
    cin_model_indices.close();
    
    printf("Optimization starts for Rotation %03d: View %03d - View %03d\n", 
           num_rotations, indices_begin[num_rotations-1], indices_end[num_rotations-1]);
    
    /*******************************************************************
    * optimize the partial object model in the current 360 degrees rotation
    *******************************************************************/
    std::vector<Eigen::Matrix4f> transforms_pairwise;
    std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> ptr_obj_pcls;
      
    transforms_pairwise.clear();
    ptr_obj_pcls.clear();
    
    //read all the point clouds in the current 360 degrees rotation
    for (int index_view = indices_begin[num_rotations-1]; index_view <= indices_end[num_rotations-1]; ++index_view)
    {
      //load the object point cloud in the perception
      char path_pcl[1024];
      sprintf(path_pcl, "%s/object_point_cloud_in_scene_%03d.pcd", path_current_root.c_str(), index_view);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl, *ptr_pcl);
        
      if (ptr_pcl == NULL)
      {
        printf("Error: fail to load the object point cloud %03d\n", index_view);
        return params_manipulation;
      }
        
      ptr_obj_pcls.push_back(ptr_pcl);
      printf("Loaded View %03d \n", index_view);
    }
    
    //read all the transformation matrices
    Eigen::Matrix4f transform_end_to_begin = register_point_clouds_from_two_views(path_current_root, indices_end[num_rotations-1], indices_begin[num_rotations-1]);
    transforms_pairwise.push_back(transform_end_to_begin); 
    
    for (int index_view = indices_begin[num_rotations-1]; index_view <= indices_end[num_rotations-1]-1; ++index_view)
    {
      std::ifstream cin_transform;
      char path_transform[1024];
      sprintf(path_transform, "%s/transformation_from_%03d_to_%03d.txt", path_current_root.c_str(), index_view, index_view + 1); 
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information!"<<std::endl;
        return params_manipulation;
      }
	    
      Eigen::Matrix4f transform;
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform(i, j);
	
      cin_transform.close();
	    
      transforms_pairwise.push_back(transform);
    }
    
    std::vector<Eigen::Matrix4f> poses = optimize_globally_registration_in_360_rotation(transforms_pairwise, ptr_obj_pcls);
    
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_refined_model(new pcl::PointCloud<pcl::PointXYZRGBA>());
    ptr_refined_model->points.clear();
    ptr_refined_model->height = 1;
    ptr_refined_model->width = ptr_refined_model->points.size();
    ptr_refined_model->is_dense = true;
    
    for(int i = 0; i < ptr_obj_pcls.size(); ++i)
    {
      //transform the point cloud in each view to the base coordinate system
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_to_base(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::transformPointCloud(*ptr_obj_pcls[i], *ptr_to_base, poses[i]);
      *ptr_refined_model += *ptr_to_base;
    }
    
    //save the refined partial model for future use
    char path_refined_model_backup[1024];
    sprintf(path_refined_model_backup, "%s/refined_partial_object_model_%03d.pcd", path_current_root.c_str(), num_rotations);
    pcl::io::savePCDFileASCII(path_refined_model_backup, *ptr_refined_model); 
    
    //save the transformation information from each view to the model base CS;
    //the model base CS is actually the CS of the last view
    for(int i = 0; i < poses.size(); ++i)
    {
      std::ofstream cout_transform;
      char path_transform[1024];
      sprintf(path_transform, "%s/transformation_to_model_for_view_%03d.txt", path_current_root.c_str(), indices_begin[num_rotations-1] + i); 
      cout_transform.open(path_transform);
      if (!cout_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to save the model transform information!"<<std::endl;
        return params_manipulation;
      }
      
      cout_transform<<poses[i]<<std::endl;
      
      cout_transform.close();
    }
    
    /*******************************************************************
    * Compute the transformation from the model in the last 360 degrees 
    * rotation to the current 360 degrees rotation, if num_rotations > 1.
    *******************************************************************/
    
    if(num_rotations > 1)
    {
      //transformation from the end view CS of the last partial model to the begin view CS of the current partial model
      Eigen::Matrix4f transform_from_last_model;
      //transformation from the begin view CS of the current partial model to the (end view) CS of the current partial model
      Eigen::Matrix4f transform_from_begin_view;
      
      std::ifstream cin_transform;
      char path_transform[1024];
      
      //transformation from the end view CS of the last partial model to the begin view CS of the current partial model;
      //Attention: this is with respect to the robot coordinate system
      sprintf(path_transform, "%s/transformation_from_%03d_to_%03d_in_robot_cs.txt", path_current_root.c_str(), indices_end[num_rotations-2], indices_begin[num_rotations-1]); 
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information from last rotation in robot CS!"<<std::endl;
        return params_manipulation;
      }
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform_from_last_model(i, j);
      cin_transform.close();
      
      //transformation from the begin view CS of the current partial model to the (end view) CS of the current partial model
      sprintf(path_transform, "%s/transformation_to_model_for_view_%03d.txt", path_current_root.c_str(), indices_begin[num_rotations-1]); 
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information from the begin view!"<<std::endl;
        return params_manipulation;
      }
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform_from_begin_view(i, j);
      cin_transform.close();
      
      //The raw model point clouds are always in the camera CS;
      //However the transformation between 360 degrees rotation procedures are in the robot CS;
      //From right to left: Camera CS -> Robot CS -> Transform in Robot CS -> Camera CS -> Transform in Camera CS.
      Eigen::Matrix4f transform_estimated =  transform_from_begin_view * transform_to_robot_cs.inverse() * transform_from_last_model * transform_to_robot_cs;
      
      //obtain the transformation between two different 360 degrees rotation
      //procedures and merge all the models together;
      //both the transformation and the merged model are saved.
      register_models_incrementally_between_360_rotations(path_current_root, num_rotations-1, num_rotations, transform_estimated);
    }
    else //num_rotations = 1
    {
      //save the model 
      char path_refined_model_backup[1024];
      sprintf(path_refined_model_backup, "%s/object_model_after_360_rotation_001.pcd", path_current_root.c_str());
      pcl::io::savePCDFileASCII(path_refined_model_backup, *ptr_refined_model);
    }
    
    /*******************************************************************
    * if the next step is to use the grasp to change the standing bottom side
    ********************************************************************/
    //read the current model after the current 360 degrees rotation procedure;
    //the model is always in the CS of the last view
    char path_model_after_rotations[1024];
    sprintf(path_model_after_rotations, "%s/object_model_after_360_rotation_%03d.pcd", path_current_root.c_str(), num_rotations);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_after_rotations(new pcl::PointCloud<pcl::PointXYZRGBA>());
    pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_model_after_rotations, *ptr_model_after_rotations);
    
    //transform the refined model to the robot coordinate system
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_refined_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
    pcl::transformPointCloud(*ptr_model_after_rotations, *ptr_refined_model_robot_cs, transform_to_robot_cs);
    
    //create the filtering object
    /*
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor;
    sor.setInputCloud(ptr_refined_model_robot_cs);
    sor.setMeanK(50);
    sor.setStddevMulThresh(3.0);
    sor.filter(*ptr_refined_model_robot_cs);
    std::cout<<"Filtered Point Cloud Size:"<<ptr_refined_model_robot_cs->points.size()<<std::endl;
    */
        
    //compute a bounding box for the point cloud of the modeling object in robot CS
    Eigen::Vector3f bbox_centroid;
    Eigen::Matrix3f bbox_directions;
    float length_1st, length_2nd, length_3rd;
    obtain_oriented_bounding_box_on_XY_plane(ptr_refined_model_robot_cs, bbox_centroid, bbox_directions,
                                             length_1st, length_2nd, length_3rd);
        
    //transform the object model to the bounding box coordinate system
    Eigen::Matrix4f bb_transform(Eigen::Matrix4f::Identity());
    bb_transform.block<3, 3>(0, 0) = bbox_directions.transpose();
    bb_transform.block<3, 1>(0, 3) = -1.f * (bb_transform.block<3, 3>(0, 0) * bbox_centroid);
        
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_bb_cs(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::transformPointCloud(*ptr_refined_model_robot_cs, *ptr_model_bb_cs, bb_transform);

    //The point cloud of the modeling object is transformed to the bounding box coordinate system;
    //This function is used for evaluating the push difficulties for the gripper after using 
    //the other 5 bounding box sides (+x, -x, +y, -y, +z) above the table as the bottom sides.
    std::vector<float> push_difficulty = evaluate_push_difficulty_for_using_other_five_sides_as_bottom(ptr_model_bb_cs, length_1st, length_2nd, length_3rd);
    printf("Push Difficulty Evaluation Results for using (+x, -x, +y, -y, +z in x direction, +z in y direction) sides as the new bottoms: \n");
    for(int i = 0; i < push_difficulty.size(); ++i)
      printf("%10f ", push_difficulty[i]);
    printf("\n");
        
    //The point cloud of the modeling object is transformed to the bounding box coordinate system;
    //evaluate the grasp difficulties for the gripper to grasp the 
    //modeling object from 5 bounding box sides in 6 different ways (+x side in y direction, 
    //-x side in y direction, +y side in x direction, -y side in x direction, 
    //+z side in x direction, +z side in y direction)
    std::vector<float> grasp_difficulty = evaluate_grasp_difficulty_for_five_bounding_box_sides(ptr_model_bb_cs, length_1st, length_2nd, length_3rd);
    printf("Grasp Difficulty Evaluation Results for using (+x, -x, +y, -y, +z in x direction, +z in y direction) sides as the new bottoms: \n");
    for(int i = 0; i < grasp_difficulty.size(); ++i)
      printf("%10f ", grasp_difficulty[i]);
    printf("\n");
        
    //The point cloud of the modeling object is transformed to the bounding box coordinate system;
    //evaluate the stand stabilities of the modeling object if it stands on the table with 
    //5 different bounding box sides (+x side, -x side, +y side, -y side, +z side) as the bottom sides.
    std::vector<float> stand_stability = evaluate_stand_stability_for_five_bounding_box_sides(ptr_model_bb_cs, length_1st, length_2nd, length_3rd);
    printf("Stand Stability Evaluation Results for using (+x, -x, +y, -y, +z) sides as the new bottoms: \n");
    for(int i = 0; i < stand_stability.size(); ++i)
      printf("%10f ", stand_stability[i]);
    printf("\n");
    
    //The area evaluation for overlap and new information,using 5 different bounding box sides (+x side, -x side, +y side, -y side, +z side) as the new bottom sides;
    //generate the same bounding box inside the function and evluate with the camera origins.
    std::vector<float> area_evaluation = evaluate_overlap_and_new_for_using_other_five_sides_as_bottom(path_current_root, num_rotations, transform_to_robot_cs);
    printf("Area Evaluation Results for using (+x, -x, +y, -y, +z) sides as the new bottoms: \n");
    for(int i = 0; i < area_evaluation.size(); ++i)
      printf("%10f ", area_evaluation[i]);
    printf("\n");
    
    //The quality of the model ,using 5 different bounding box sides (+x side, -x side, +y side, -y side, +z side) as the new bottom sides;
    //generate the same bounding box inside the function and evluate with the camera origins.
    std::vector<float> quality_evaluation = evaluate_quality_of_model_for_using_other_five_sides_as_bottom(path_current_root, num_rotations, transform_to_robot_cs);
    printf("Quality Evaluation Results for using (+x, -x, +y, -y, +z) sides as the new bottoms: \n");
    for(int i = 0; i < quality_evaluation.size(); ++i)
      printf("%10f ", quality_evaluation[i]);
    printf("\n");
    
    //Generate all the grasp solutions for changing the bottom of the modeling object based on the bounding box in the bounding box coordinate system.
    std::vector<BttmChgSol> solutions_all = generate_all_change_bottom_solutions_for_bounding_box();
        
    std::vector<float> manipulation_cost(solutions_all.size(), 1.0f);  //more implementation from huitan
    printf("Manipulation Cost Evaluation Results for solutions to change the bottom side: \n");
    for(int i = 0; i < manipulation_cost.size(); ++i)
      printf("%10f ", manipulation_cost[i]);
    printf("\n");
    
    std::vector<float> solution_scores(solutions_all.size(), 1.0f);
    for(int i = 0; i < solution_scores.size(); ++i)
    {
      solution_scores[i] = push_difficulty[solutions_all[i].index_set_side] 
                         * grasp_difficulty[solutions_all[i].index_grasp_side]
                         * stand_stability[solutions_all[i].index_set_side]
                         * area_evaluation[solutions_all[i].index_set_side]
                         * (1.0f + quality_evaluation[solutions_all[i].index_set_side] + manipulation_cost[i]);
    }
    printf("Scores of solutions to change the bottom side: \n");
    for(int i = 0; i < solution_scores.size(); ++i)
      printf("%10f ", solution_scores[i]);
    printf("\n");
    
    //choose one to grasp the top temporarily
    BttmChgSol solution_execute;
    float score_best = 0.0f;
    for(int i = 0; i < solution_scores.size(); ++i)
    {
      if(solution_scores[i] > score_best)
      {
        score_best = solution_scores[i];
        solution_execute = solutions_all[i];
      }
    }
    
    const float modeling_termination = 0.001f;
    if(score_best > modeling_termination)
    {
      params_manipulation[0] = 2.0f;        //2.0f: grasp to change the bottom
      params_manipulation[1] = bbox_centroid(0);   //bbox_centroid.x in robot CS
      params_manipulation[2] = bbox_centroid(1);   //bbox_centroid.y in robot CS
      params_manipulation[3] = bbox_centroid(2);   //bbox_centroid.z in robot CS
      //bbox_directions: x_axis in robot CS
      params_manipulation[4] = bbox_directions(0, 0);   
      params_manipulation[5] = bbox_directions(1, 0);
      params_manipulation[6] = bbox_directions(2, 0);
      //bbox_directions: y_axis in robot CS
      params_manipulation[7] = bbox_directions(0, 1);   
      params_manipulation[8] = bbox_directions(1, 1);
      params_manipulation[9] = bbox_directions(2, 1);
      //bbox_directions: z_axis in robot CS
      params_manipulation[10] = bbox_directions(0, 2);   
      params_manipulation[11] = bbox_directions(1, 2);
      params_manipulation[12] = bbox_directions(2, 2);
      //length
      params_manipulation[13] = length_1st; //length in x-axis direction
      params_manipulation[14] = length_2nd; //length in y-axis direction
      params_manipulation[15] = length_3rd; //lenght in z-axis direction
      //grasp side direction in robot CS
      params_manipulation[16] = solution_execute.direction_grasp(0);   
      params_manipulation[17] = solution_execute.direction_grasp(1);
      params_manipulation[18] = solution_execute.direction_grasp(2);	 
      //set side direction in robot CS 
      params_manipulation[19] = solution_execute.direction_set(0);   
      params_manipulation[20] = solution_execute.direction_set(1);
      params_manipulation[21] = solution_execute.direction_set(2);
      
      //save the manipulation information for the future use
      char path_manipulation_info_backup[1024];
      sprintf(path_manipulation_info_backup, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception);    
      std::ofstream cout_manipulation;
      cout_manipulation.open(path_manipulation_info_backup);
      if(!cout_manipulation.is_open())
      {
        std::cout<<"ERROR: fail to open the file to save the manipulation information!\n"<<std::endl;
        return params_manipulation;
      }
      std::cout<<"**********Grasp Information**********"<<std::endl;
      for(int i = 0; i <= 21; ++i)
      {
        std::cout<<i<<"  "<<params_manipulation[i]<<std::endl;
        cout_manipulation<<params_manipulation[i]<<std::endl;
      }
      cout_manipulation.close();
    }
    else
    {
      params_manipulation[0] = 0.0f;    //0.0: end the modeling procedure
      
      //save the manipulation information for the future use
      char path_manipulation_info_backup[1024];
      sprintf(path_manipulation_info_backup, "%s/manipulation_after_scene_%03d.txt", path_current_root.c_str(), index_perception);    
      std::ofstream cout_manipulation;
      cout_manipulation.open(path_manipulation_info_backup);
      if(!cout_manipulation.is_open())
      {
        std::cout<<"ERROR: fail to open the file to save the manipulation information!\n"<<std::endl;
        return params_manipulation;
      }
      for(int i = 0; i <= 0; ++i)
        cout_manipulation<<params_manipulation[i]<<std::endl;
      cout_manipulation.close();
    }
    
    
  }   // the rotation angle > 360 degrees 
	  
  if (index_perception > 1)
    cin_last_manipulation.close();
  
  return params_manipulation;
}

/*
  Align two point clouds based on XYZ coordinates
  @param cloud_src: the source point cloud
  @param cloud_tgt: the target point cloud
  @return: returns a transformation matrix to align the two point clouds
*/
Eigen::Matrix4f align_point_clouds_using_icp(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_src, 
                                             const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_tgt)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr src(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr tgt(new pcl::PointCloud<pcl::PointXYZ>);
  
  src = cloud_src;
  tgt = cloud_tgt;
  
  //compute surface normals and curvature
  pcl::PointCloud<pcl::PointNormal>::Ptr points_with_normals_src(new pcl::PointCloud<pcl::PointNormal>);
  pcl::PointCloud<pcl::PointNormal>::Ptr points_with_normals_tgt(new pcl::PointCloud<pcl::PointNormal>);
  
  pcl::NormalEstimation<pcl::PointXYZ, pcl::PointNormal> norm_est;
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ> ());
  norm_est.setSearchMethod(tree);
  norm_est.setKSearch(30);
  
  norm_est.setInputCloud(src);
  norm_est.compute(*points_with_normals_src);
  pcl::copyPointCloud(*src, *points_with_normals_src); //copy xyz from src to points_with_normals_src
  
  norm_est.setInputCloud(tgt);
  norm_est.compute(*points_with_normals_tgt);
  pcl::copyPointCloud(*tgt, *points_with_normals_tgt);
  
  //Instantiate our custom point representation (defined above) ...
  MyPointRepresentation point_representation;
  // ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
  float alpha[4] = {1.0, 1.0, 1.0, 1.0};
  point_representation.setRescaleValues (alpha);  
  
  // Align
  pcl::IterativeClosestPointNonLinear<pcl::PointNormal, pcl::PointNormal> reg;
  reg.setTransformationEpsilon(0.0001);
  // Set the maximum distance between two correspondences (src<->tgt) to 3cm
  // Note: adjust this based on the size of your datasets
  reg.setMaxCorrespondenceDistance(0.3);  
  // Set the point representation
  //reg.setPointRepresentation(boost::make_shared<const MyPointRepresentation> (point_representation));

  reg.setInputCloud(points_with_normals_src);
  reg.setInputTarget(points_with_normals_tgt);  
  
  pcl::visualization::PCLVisualizer *pcd_viewer;
  pcd_viewer = new pcl::visualization::PCLVisualizer("Pairwise Incremental Registration example");
  
  // Run the same optimization in a loop
  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
  pcl::PointCloud<pcl::PointNormal>::Ptr reg_result = points_with_normals_src;
  reg.setMaximumIterations(2);
  
  for(int i = 0; i < 100; ++i)
  {
    PCL_INFO("Iteration Nr. %d.\n", i);

    // save cloud for visualization purpose
    points_with_normals_src = reg_result;

    // Estimate
    reg.setInputCloud(points_with_normals_src);
    reg.align(*reg_result);

	//accumulate transformation between each Iteration
    Ti = reg.getFinalTransformation() * Ti;

	//if the difference between this transformation and the previous one
	//is smaller than the threshold, refine the process by reducing
	//the maximal correspondence distance
    if(fabs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon())
      reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance () - 0.003);
      
    if(fabs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon()/1000.0f)
      break;
    
    prev = reg.getLastIncrementalTransformation();
    
    std::cout<<"MaxCorrespondenceDistance:"<<reg.getMaxCorrespondenceDistance()<<std::endl;
    std::cout<<prev<<std::endl;
    
    //show the current registration state
    pcd_viewer->removePointCloud("source");
    pcd_viewer->removePointCloud("target");

    PointCloudColorHandlerGenericField<pcl::PointNormal> tgt_color_handler(points_with_normals_tgt, "curvature");
    if(!tgt_color_handler.isCapable())
      PCL_WARN("Cannot create curvature color handler!");

    PointCloudColorHandlerGenericField<pcl::PointNormal> src_color_handler(points_with_normals_src, "curvature");
    if(!src_color_handler.isCapable())
      PCL_WARN("Cannot create curvature color handler!");

    pcd_viewer->addPointCloud(points_with_normals_tgt, tgt_color_handler, "target");
    pcd_viewer->addPointCloud(points_with_normals_src, src_color_handler, "source");
    
    PCL_INFO("Press q to continue the registration.\n");
    pcd_viewer->spin();
  }
  
  return Ti;																 
}

/*
  Align two point clouds based on the point coordinate, normal and color information 
  with ICP algorithm, implemented by ourselves
  @param cloud_src: the source point cloud
  @param cloud_tgt: the target point cloud
  @param transform_initial: the initial transformation matrix for the ICP algorithm
  @param register_accuracy: return the average similarity distance of all matched 
    points based on the returned transformation matrix
  @return: returns a transformation matrix to align the two point clouds
*/
pcl::visualization::PCLVisualizer *pcd_viewer_align_plus_color = new pcl::visualization::PCLVisualizer("Pairwise Registration");
Eigen::Matrix4f align_point_clouds_using_icp_plus_color(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_src, 
                                                        const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_tgt,
                                                        const Eigen::Matrix4f &transform_initial,
                                                        double &register_accuracy)
{
  /*****************************************************************************
   * STEP1: initialize the registration
   ****************************************************************************/
  pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGBA,pcl::PointXYZRGBA> tf_est;
  Eigen::Matrix4f transform = transform_initial;
  
  /*****************************************************************************
   * STEP2: compute the normals for all points
   ****************************************************************************/
  pcl::NormalEstimation<pcl::PointXYZRGBA, pcl::Normal> norm_est;
  pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr ptr_search_tree(new pcl::search::KdTree<pcl::PointXYZRGBA> ());
  norm_est.setSearchMethod(ptr_search_tree);
  norm_est.setRadiusSearch(0.02);   //0.02m = 20mm
  
  //the normals of the target point cloud are fixed
  pcl::PointCloud<pcl::Normal>::Ptr ptr_tgt_normals(new pcl::PointCloud<pcl::Normal> ());  
  norm_est.setInputCloud(ptr_tgt);
  norm_est.compute(*ptr_tgt_normals);
  
  //create a kdtree using the target point cloud for searching
  pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
  kdtree.setInputCloud(ptr_tgt);
  
  /*****************************************************************************
   * STEP3: icp iterations based on the point coordinate, normal and color
   ****************************************************************************/
  #define NUM_MAX_ICP_ITERATION 50
  int num_iteration = NUM_MAX_ICP_ITERATION;
  
  while(num_iteration--)
  {
	std::cout<<"*****icp plus color iteration index: "<<NUM_MAX_ICP_ITERATION - num_iteration<<std::endl;
	
	//transform the original source point cloud to a better position to fit the target point cloud
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed(new pcl::PointCloud<pcl::PointXYZRGBA> ());
    pcl::transformPointCloud(*ptr_src, *ptr_src_transformed, transform);
    
    pcd_viewer_align_plus_color->removePointCloud("source");
    pcd_viewer_align_plus_color->removePointCloud("target");
    pcd_viewer_align_plus_color->addPointCloud(ptr_tgt, "target");
    pcd_viewer_align_plus_color->addPointCloud(ptr_src_transformed, "source");
    //PCL_INFO("Press q to continue the registration.\n");
    //pcd_viewer_align_plus_color->spin();
    pcd_viewer_align_plus_color->spinOnce(1000);
    
    //the normals of the transformed source point cloud are changing gradually
    pcl::PointCloud<pcl::Normal>::Ptr ptr_src_transformed_normals(new pcl::PointCloud<pcl::Normal> ());
    norm_est.setInputCloud(ptr_src_transformed);
    norm_est.compute(*ptr_src_transformed_normals);
    
    //used to reject some matched pairs
    std::vector<float> matched_distances;
    matched_distances.clear();
	
	//initialization the matched keypoint pairs between the transformed source point cloud 
	//and the original target point cloud for computing the rigid transformation
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed_match(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_tgt_match(new pcl::PointCloud<pcl::PointXYZRGBA>);
    ptr_src_transformed_match->points.clear();
    ptr_tgt_match->points.clear();
    ptr_src_transformed_match->height = 1;
    ptr_tgt_match->height = 1;
	
	//assgin the corresponding matched point pairs between the ptr_tgt and ptr_src_transform
	//kdtree search for neighbors within radius search
	double accuracy_sum = 0.0;
	int accuracy_num = 0;
    const double ICP_REJECTION_XYZ_DISTANCE = 0.03; //0.01m = 1cm = 10mm, with keypoints
    for(int index_src = 0; index_src < ptr_src_transformed->points.size(); ++index_src)
    {
	  
	  //remove the points with nan values
	  if(isnan(ptr_src_transformed->points[index_src].z) || isnan(ptr_src_transformed_normals->points[index_src].normal_z))
	    continue;
	  
	  std::vector<int> result_indices;
	  std::vector<float> result_square_distance;
	  kdtree.radiusSearch(ptr_src_transformed->points[index_src], ICP_REJECTION_XYZ_DISTANCE, result_indices, result_square_distance);
	  //no matched point is found within the threshold distance (all points in the target point cloud are rejected)
	  if(result_indices.empty())
	    continue;
	  
	  //find the point in the target point cloud with the minimal distance considering the XYZ coordinate, normal and color
	  int best_match_index_tgt = -1;
	  double best_match_distance = 10000000000.0;
	  for(int index_result = 0; index_result < result_indices.size(); ++index_result)
	  {
		int cur_tgt_index = result_indices[index_result];
		//remove the points with nan values
		if(isnan(ptr_tgt->points[cur_tgt_index].z) || isnan(ptr_tgt_normals->points[cur_tgt_index].normal_z))
		  continue;
		
		pcl::PointXYZRGBA src_point = ptr_src_transformed->points[index_src];
		pcl::PointXYZRGBA tgt_point = ptr_tgt->points[cur_tgt_index];	

		pcl::Normal src_normal = ptr_src_transformed_normals->points[index_src];
		pcl::Normal tgt_normal = ptr_tgt_normals->points[cur_tgt_index];
        
		double square_distance_normal = (src_normal.normal_x - tgt_normal.normal_x) * (src_normal.normal_x - tgt_normal.normal_x) + 
		                                (src_normal.normal_y - tgt_normal.normal_y) * (src_normal.normal_y - tgt_normal.normal_y) +
		                                (src_normal.normal_z - tgt_normal.normal_z) * (src_normal.normal_z - tgt_normal.normal_z);

		double square_distance_xyz_plane = (src_point.x - tgt_point.x) * (src_point.x - tgt_point.x) * tgt_normal.normal_x * tgt_normal.normal_x + 
		                                   (src_point.y - tgt_point.y) * (src_point.y - tgt_point.y) * tgt_normal.normal_y * tgt_normal.normal_y +
		                                   (src_point.z - tgt_point.z) * (src_point.z - tgt_point.z) * tgt_normal.normal_z * tgt_normal.normal_z;
                                           
		double square_distance_xyz = (src_point.x - tgt_point.x) * (src_point.x - tgt_point.x) + 
		                             (src_point.y - tgt_point.y) * (src_point.y - tgt_point.y) +
		                             (src_point.z - tgt_point.z) * (src_point.z - tgt_point.z);
                                           
		double square_distance_bgr = (src_point.b - tgt_point.b) * (src_point.b - tgt_point.b) + 
		                             (src_point.g - tgt_point.g) * (src_point.g - tgt_point.g) +
		                             (src_point.r - tgt_point.r) * (src_point.r - tgt_point.r);
		
		//two important kinds of coefficients here: one is the weight, the other is for scaling all the values into the range 0-1
        /*
		double cur_match_distance = 0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz_plane) + 
		                            0.25 * (1/1.0f) * sqrt(square_distance_normal) + 
		                            0.50 * (1/255.0f) * sqrt(square_distance_bgr);*/
		double cur_match_distance = 0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz_plane) + 
                                    0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz) + 
		                            0.25 * (1/1.0f) * sqrt(square_distance_normal) + 
		                            0.25 * (1/255.0f) * sqrt(square_distance_bgr);
		  
		if(cur_match_distance < best_match_distance)
		{
		  best_match_distance = cur_match_distance;
		  best_match_index_tgt = cur_tgt_index;
		}
	  }
	  
	  //save the matched point pair for the subsequent registration
	  if(best_match_index_tgt >= 0)
	  {
	    ptr_src_transformed_match->points.push_back(ptr_src_transformed->points[index_src]);
	    ptr_tgt_match->points.push_back(ptr_tgt->points[best_match_index_tgt]);
	    accuracy_sum += best_match_distance;
	    ++accuracy_num;
	    matched_distances.push_back(best_match_distance);
	  }
	}
    
    ptr_src_transformed_match->width = ptr_src_transformed_match->points.size();
	ptr_tgt_match->width = ptr_tgt_match->points.size();
	
	//Notice that although register_accuracy returns the accuracy of (n-1)th iteration, 
	//not the exact final nth iteration, however, the accuracy is almost the same when the 
	//iteration converges, as a result, it is ok here.
	if(accuracy_num)
	{
	  register_accuracy = accuracy_sum / accuracy_num;
	}else
	{
	  register_accuracy = 100000.0;
	}
	
	if(ptr_src_transformed_match->width == 0)
	{
	  std::cout<<"ERROR: Fail to establish the correspondences!"<<std::endl;
	  break;
	}
	
	//we reject pairs with distances more than 2.0 times the standard deviation
	//VERY IMPORTANT: GREATLY IMPROVE THE REGISTRATION QUALITY	
	float distance_average = register_accuracy;
	float distance_variance = 0.0f;
	for(int i = 0; i < matched_distances.size(); ++i)
	  distance_variance += (matched_distances[i] - distance_average)*(matched_distances[i] - distance_average);
	distance_variance = sqrt(distance_variance/matched_distances.size());
	std::cout<<"distance_average: "<<distance_average<<"  distance_variance: "<<distance_variance<<std::endl;

	//initialization the remain matched keypoint pairs between the transformed source point cloud 
	//and the original target point cloud for computing the rigid transformation
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed_match_valid(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_tgt_match_valid(new pcl::PointCloud<pcl::PointXYZRGBA>);
    ptr_src_transformed_match_valid->points.clear();
    ptr_tgt_match_valid->points.clear();
    ptr_src_transformed_match_valid->height = 1;
    ptr_tgt_match_valid->height = 1;
    
    for(int i = 0; i < matched_distances.size(); ++i)
    {
	  if(matched_distances[i] < distance_average + 2.0f * distance_variance)
	  {
	    ptr_src_transformed_match_valid->points.push_back(ptr_src_transformed_match->points[i]);
	    ptr_tgt_match_valid->points.push_back(ptr_tgt_match->points[i]);
	  }
	}
    
    ptr_src_transformed_match_valid->width = ptr_src_transformed_match_valid->points.size();
	ptr_tgt_match_valid->width = ptr_tgt_match_valid->points.size();
	
	//compute the rigid transformation matrix
	Eigen::Matrix4f transform_local;
	tf_est.estimateRigidTransformation(*ptr_src_transformed_match_valid, *ptr_tgt_match_valid, transform_local);
	std::cout<<transform_local<<std::endl;
	
	//obtain the new global transformation matrix between the original source and target point clouds
	//accumulate transformation between each Iteration
	transform = transform_local * transform;
	
	//terminate the iteration if it has converged (the difference is smaller than a threshold)
    const double ICP_CONVERGENCE_ACCURAY = 0.003;  //with keypoints
    Eigen::Matrix4f transform_difference = transform_local - Eigen::Matrix4f::Identity();
    std::cout<<"convergence difference (transformation matrix difference):"<<fabs(transform_difference.sum())<<std::endl;
	if(fabs(transform_difference.sum()) < ICP_CONVERGENCE_ACCURAY)
	  break;
  }
  
  return transform;												
}

/*
  Align two point clouds of partial object modelsbased on the point coordinate, normal and color information 
  with ICP algorithm, implemented by ourselves
  @param cloud_src: the source point cloud
  @param cloud_tgt: the target point cloud
  @param transform_initial: the initial transformation matrix for the ICP algorithm
  @param register_accuracy: return the average similarity distance of all matched 
    points based on the returned transformation matrix
  @return: returns a transformation matrix to align the two point clouds
*/
Eigen::Matrix4f align_point_clouds_using_icp_plus_color_between_models(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_src, 
                                                                       const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_tgt,
                                                                       const Eigen::Matrix4f &transform_initial,
                                                                       double &register_accuracy)
{
  /*****************************************************************************
   * STEP1: initialize the registration
   ****************************************************************************/
  pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGBA,pcl::PointXYZRGBA> tf_est;
  Eigen::Matrix4f transform = transform_initial;
  
  /*****************************************************************************
   * STEP2: compute the normals for all points
   ****************************************************************************/
  pcl::NormalEstimation<pcl::PointXYZRGBA, pcl::Normal> norm_est;
  pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr ptr_search_tree(new pcl::search::KdTree<pcl::PointXYZRGBA> ());
  norm_est.setSearchMethod(ptr_search_tree);
  norm_est.setRadiusSearch(0.02);   //0.02m = 20mm
  
  //the normals of the target point cloud are fixed
  pcl::PointCloud<pcl::Normal>::Ptr ptr_tgt_normals(new pcl::PointCloud<pcl::Normal> ());  
  norm_est.setInputCloud(ptr_tgt);
  norm_est.compute(*ptr_tgt_normals);
  
  //create a kdtree using the target point cloud for searching
  pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
  kdtree.setInputCloud(ptr_tgt);
  
  /*****************************************************************************
   * STEP3: icp iterations based on the point coordinate, normal and color
   ****************************************************************************/
  #define NUM_MAX_ICP_ITERATION 50
  int num_iteration = NUM_MAX_ICP_ITERATION;
  
  while(num_iteration--)
  {
	std::cout<<"*****icp plus color iteration index: "<<NUM_MAX_ICP_ITERATION - num_iteration<<std::endl;
	
	//transform the original source point cloud to a better position to fit the target point cloud
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed(new pcl::PointCloud<pcl::PointXYZRGBA> ());
    pcl::transformPointCloud(*ptr_src, *ptr_src_transformed, transform);
    
    pcd_viewer_align_plus_color->removePointCloud("source");
    pcd_viewer_align_plus_color->removePointCloud("target");
    pcd_viewer_align_plus_color->addPointCloud(ptr_tgt, "target");
    pcd_viewer_align_plus_color->addPointCloud(ptr_src_transformed, "source");
    //PCL_INFO("Press q to continue the registration.\n");
    //pcd_viewer_align_plus_color->spin();
    pcd_viewer_align_plus_color->spinOnce(1000);
    
    //the normals of the transformed source point cloud are changing gradually
    pcl::PointCloud<pcl::Normal>::Ptr ptr_src_transformed_normals(new pcl::PointCloud<pcl::Normal> ());
    norm_est.setInputCloud(ptr_src_transformed);
    norm_est.compute(*ptr_src_transformed_normals);
    
    //used to reject some matched pairs
    std::vector<float> matched_distances;
    matched_distances.clear();
	
	//initialization the matched keypoint pairs between the transformed source point cloud 
	//and the original target point cloud for computing the rigid transformation
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed_match(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_tgt_match(new pcl::PointCloud<pcl::PointXYZRGBA>);
    ptr_src_transformed_match->points.clear();
    ptr_tgt_match->points.clear();
    ptr_src_transformed_match->height = 1;
    ptr_tgt_match->height = 1;
	
	//assgin the corresponding matched point pairs between the ptr_tgt and ptr_src_transform
	//kdtree search for neighbors within radius search
	double accuracy_sum = 0.0;
	int accuracy_num = 0;

    //const double ICP_REJECTION_XYZ_DISTANCE = 0.03;  //for milk box
    const double ICP_REJECTION_XYZ_DISTANCE = 0.015;  //for spray
    //const double ICP_REJECTION_XYZ_DISTANCE = 0.025;  //for coffee box
    for(int index_src = 0; index_src < ptr_src_transformed->points.size(); ++index_src)
    {
	  
	  //remove the points with nan values
	  if(isnan(ptr_src_transformed->points[index_src].z) || isnan(ptr_src_transformed_normals->points[index_src].normal_z))
	    continue;
	  
	  std::vector<int> result_indices;
	  std::vector<float> result_square_distance;
	  kdtree.radiusSearch(ptr_src_transformed->points[index_src], ICP_REJECTION_XYZ_DISTANCE, result_indices, result_square_distance);
	  //no matched point is found within the threshold distance (all points in the target point cloud are rejected)
	  if(result_indices.empty())
	    continue;
	  
	  //find the point in the target point cloud with the minimal distance considering the XYZ coordinate, normal and color
	  int best_match_index_tgt = -1;
	  double best_match_distance = 10000000000.0;
	  for(int index_result = 0; index_result < result_indices.size(); ++index_result)
	  {
		int cur_tgt_index = result_indices[index_result];
		//remove the points with nan values
		if(isnan(ptr_tgt->points[cur_tgt_index].z) || isnan(ptr_tgt_normals->points[cur_tgt_index].normal_z))
		  continue;
		
		pcl::PointXYZRGBA src_point = ptr_src_transformed->points[index_src];
		pcl::PointXYZRGBA tgt_point = ptr_tgt->points[cur_tgt_index];
		
		pcl::Normal src_normal = ptr_src_transformed_normals->points[index_src];
		pcl::Normal tgt_normal = ptr_tgt_normals->points[cur_tgt_index];
        
		double square_distance_normal = (src_normal.normal_x - tgt_normal.normal_x) * (src_normal.normal_x - tgt_normal.normal_x) + 
		                                (src_normal.normal_y - tgt_normal.normal_y) * (src_normal.normal_y - tgt_normal.normal_y) +
		                                (src_normal.normal_z - tgt_normal.normal_z) * (src_normal.normal_z - tgt_normal.normal_z);

		double square_distance_xyz_plane = (src_point.x - tgt_point.x) * (src_point.x - tgt_point.x) * tgt_normal.normal_x * tgt_normal.normal_x + 
		                                   (src_point.y - tgt_point.y) * (src_point.y - tgt_point.y) * tgt_normal.normal_y * tgt_normal.normal_y +
		                                   (src_point.z - tgt_point.z) * (src_point.z - tgt_point.z) * tgt_normal.normal_z * tgt_normal.normal_z;
                                           
		double square_distance_xyz = (src_point.x - tgt_point.x) * (src_point.x - tgt_point.x) + 
		                             (src_point.y - tgt_point.y) * (src_point.y - tgt_point.y) +
		                             (src_point.z - tgt_point.z) * (src_point.z - tgt_point.z);
                                           
		double square_distance_bgr = (src_point.b - tgt_point.b) * (src_point.b - tgt_point.b) + 
		                             (src_point.g - tgt_point.g) * (src_point.g - tgt_point.g) +
		                             (src_point.r - tgt_point.r) * (src_point.r - tgt_point.r);
		
		//two important kinds of coefficients here: one is the weight, the other is for scaling all the values into the range 0-1
        //for milk box
        /*
		double cur_match_distance = 0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz_plane) + 
                                    0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz) + 
		                            0.25 * (1/1.0f) * sqrt(square_distance_normal) + 
		                            0.25 * (1/255.0f) * sqrt(square_distance_bgr);*/

        //for spray
		double cur_match_distance = 0.0 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz_plane) + 
                                    0.50 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz) + 
		                            0.25 * (1/1.0f) * sqrt(square_distance_normal) + 
		                            0.25 * (1/255.0f) * sqrt(square_distance_bgr);
        //for coffee box
        /*
		double cur_match_distance = 0.0 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz_plane) + 
                                    0.25 * (1/ICP_REJECTION_XYZ_DISTANCE) * sqrt(square_distance_xyz) + 
		                            0.25 * (1/1.0f) * sqrt(square_distance_normal) + 
		                            0.50 * (1/255.0f) * sqrt(square_distance_bgr);
                                    */
		  
		if(cur_match_distance < best_match_distance)
		{
		  best_match_distance = cur_match_distance;
		  best_match_index_tgt = cur_tgt_index;
		}
	  }
	  
	  //save the matched point pair for the subsequent registration
	  if(best_match_index_tgt >= 0)
	  {
	    ptr_src_transformed_match->points.push_back(ptr_src_transformed->points[index_src]);
	    ptr_tgt_match->points.push_back(ptr_tgt->points[best_match_index_tgt]);
	    accuracy_sum += best_match_distance;
	    ++accuracy_num;
	    matched_distances.push_back(best_match_distance);
	  }
	}
    
    ptr_src_transformed_match->width = ptr_src_transformed_match->points.size();
	ptr_tgt_match->width = ptr_tgt_match->points.size();
	
	//Notice that although register_accuracy returns the accuracy of (n-1)th iteration, 
	//not the exact final nth iteration, however, the accuracy is almost the same when the 
	//iteration converges, as a result, it is ok here.
	if(accuracy_num)
	{
	  register_accuracy = accuracy_sum / accuracy_num;
	}else
	{
	  register_accuracy = 100000.0;
	}
	
	if(ptr_src_transformed_match->width == 0)
	{
	  std::cout<<"ERROR: Fail to establish the correspondences!"<<std::endl;
	  break;
	}
	
	//we reject pairs with distances more than 2.0 times the standard deviation
	//VERY IMPORTANT: GREATLY IMPROVE THE REGISTRATION QUALITY	
	float distance_average = register_accuracy;
	float distance_variance = 0.0f;
	for(int i = 0; i < matched_distances.size(); ++i)
	  distance_variance += (matched_distances[i] - distance_average)*(matched_distances[i] - distance_average);
	distance_variance = sqrt(distance_variance/matched_distances.size());
	std::cout<<"distance_average: "<<distance_average<<"  distance_variance: "<<distance_variance<<std::endl;

	//initialization the remain matched keypoint pairs between the transformed source point cloud 
	//and the original target point cloud for computing the rigid transformation
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_src_transformed_match_valid(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_tgt_match_valid(new pcl::PointCloud<pcl::PointXYZRGBA>);
    ptr_src_transformed_match_valid->points.clear();
    ptr_tgt_match_valid->points.clear();
    ptr_src_transformed_match_valid->height = 1;
    ptr_tgt_match_valid->height = 1;
    
    for(int i = 0; i < matched_distances.size(); ++i)
    {
	  if(matched_distances[i] < distance_average + 2.0f * distance_variance)
	  {
	    ptr_src_transformed_match_valid->points.push_back(ptr_src_transformed_match->points[i]);
	    ptr_tgt_match_valid->points.push_back(ptr_tgt_match->points[i]);
	  }
	}
    
    ptr_src_transformed_match_valid->width = ptr_src_transformed_match_valid->points.size();
	ptr_tgt_match_valid->width = ptr_tgt_match_valid->points.size();
	
	//compute the rigid transformation matrix
	Eigen::Matrix4f transform_local;
	tf_est.estimateRigidTransformation(*ptr_src_transformed_match_valid, *ptr_tgt_match_valid, transform_local);
	std::cout<<transform_local<<std::endl;
	
	//obtain the new global transformation matrix between the original source and target point clouds
	//accumulate transformation between each Iteration
	transform = transform_local * transform;
	
	//terminate the iteration if it has converged (the difference is smaller than a threshold)
    //const double ICP_CONVERGENCE_ACCURAY = 0.0001;  //for milkbox
    const double ICP_CONVERGENCE_ACCURAY = 0.0003;  //for spray
    Eigen::Matrix4f transform_difference = transform_local - Eigen::Matrix4f::Identity();
    std::cout<<"convergence difference (transformation matrix difference):"<<fabs(transform_difference.sum())<<std::endl;
	if(fabs(transform_difference.sum()) < ICP_CONVERGENCE_ACCURAY)
	  break;
  }
  
  return transform;												
}

/*
  Align two point clouds based on XYZ coordinates
  @param cloud_src: the source point cloud
  @param cloud_tgt: the target point cloud
  @return: returns a transformation matrix to align the two point clouds
*/
Eigen::Matrix4f align_point_clouds_using_ndt(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_src, 
                                             const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_tgt)
{
  
  //Initializing Normal Distributions Transform (NDT).
  pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt;

  // Setting scale dependent NDT parameters
  // Setting minimum transformation difference for termination condition.
  ndt.setTransformationEpsilon(0.0001);
  // Setting maximum step size for More-Thuente line search.
  ndt.setStepSize(0.1);
  //Setting Resolution of NDT grid structure (VoxelGridCovariance).
  ndt.setResolution(0.01);

  // Setting max number of registration iterations.
  ndt.setMaximumIterations(50);
  
  //Filtering input scan to roughly 10% of original size to increase speed of registration.
  /*
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::ApproximateVoxelGrid<pcl::PointXYZ> approximate_voxel_filter;
  approximate_voxel_filter.setLeafSize(0.01, 0.01, 0.01);
  approximate_voxel_filter.setInputCloud(cloud_src);
  approximate_voxel_filter.filter (*filtered_cloud);
  std::cout<<"Original size:"<<cloud_src->points.size()<<std::endl;
  std::cout<<"Filtered cloud contains "<<filtered_cloud->size()<<" data points "<<std::endl;
  */

  // Setting point cloud to be aligned.
  ndt.setInputSource(cloud_src);
  // Setting point cloud to be aligned to.
  ndt.setInputTarget(cloud_tgt);

  // Set initial alignment estimate found using robot odometry.
  Eigen::AngleAxisf init_rotation (0, Eigen::Vector3f::UnitZ ());
  Eigen::Translation3f init_translation (0, 0, 0);
  Eigen::Matrix4f init_guess = (init_translation * init_rotation).matrix ();

  // Calculating required rigid transform to align the input cloud to the target cloud.
  pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  ndt.align (*output_cloud, init_guess);
  
  std::cout << "Normal Distributions Transform has converged:" << ndt.hasConverged ()
            << " score: " << ndt.getFitnessScore () << std::endl;
  
  return ndt.getFinalTransformation();
}

/*
  Register two partial object point clouds captured from different viewpoints;
  before apply this function, the following 6 files should already be saved in the 
  $(path_current_root):
  object_point_cloud_in_scene_$(index_src).pcd, object_point_cloud_in_scene_$(index_tgt).pcd
  object_point_cloud_in_scene_$(index_src)_organized.pcd, object_point_cloud_in_scene_$(index_tgt)_organized.pcd
  object_point_cloud_in_scene_$(index_src)_organized.png, object_point_cloud_in_scene_$(index_tgt)_organized.png
  
  @path_current_root : the root working directory
  @index_src : the index of the source view for registration
  @index_tgt : the index of the target view for registration
  @return : the transformation matrix from the source view to the target view
*/                                         
Eigen::Matrix4f register_point_clouds_from_two_views(const std::string &path_current_root,
                                                     const int &index_view_src, const int &index_view_tgt)
{
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  
  //try registration based on ASIFT matching first
  char asift_matching_command[1024];
  sprintf(asift_matching_command, "./demo_ASIFT %s/object_point_cloud_in_scene_%03d_organized.png %s/object_point_cloud_in_scene_%03d_organized.png imgOutVert.png imgOutHori.png matchings.txt keys1.txt keys2.txt",
	      path_current_root.c_str(), index_view_src, path_current_root.c_str(), index_view_tgt);
  system(asift_matching_command);
	
  std::ifstream match_cin;
  match_cin.open("matchings.txt");
  if(!match_cin.is_open())
  {
	std::cout<<"ERROR: Fail to open the keypoints matching file!"<<std::endl;
	return transform;
  }
    
  int num_matchings;
  match_cin>>num_matchings;
    
  const int num_ASIFT_matchings_threshold = 50;
  Eigen::Matrix4f initial_transform = Eigen::Matrix4f::Identity();
      
  //if ASIFT matching can provide a rough registration result
  if (num_matchings > num_ASIFT_matchings_threshold)
  {
	//load the organized object point cloud in the source view perception
	char path_pcl_organized_src[1024];
    sprintf(path_pcl_organized_src, "%s/object_point_cloud_in_scene_%03d_organized.pcd", path_current_root.c_str(), index_view_src);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl_org_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl_organized_src, *ptr_pcl_org_src);

	//load the organized object point cloud in the target view perception
	char path_pcl_organized_tgt[1024];
    sprintf(path_pcl_organized_tgt, "%s/object_point_cloud_in_scene_%03d_organized.pcd", path_current_root.c_str(), index_view_tgt);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl_org_tgt(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl_organized_tgt, *ptr_pcl_org_tgt);
	  
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr match_src_subset(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr match_tgt_subset(new pcl::PointCloud<pcl::PointXYZRGBA>);
    match_src_subset->points.clear();
    match_tgt_subset->points.clear();
    match_src_subset->height = 1;
    match_tgt_subset->height = 1;
      
    for (int i = 0; i < num_matchings; ++i)
    {
	  float f1, f2, f3, f4;
	  int p1_w, p1_h, p2_w, p2_h;
	  match_cin>>f1>>f2>>f3>>f4;
	  p1_w = (int)f1;
	  p1_h = (int)f2;
	  p2_w = (int)f3;
	  p2_h = (int)f4;
	    
	  int index_src = p1_h * ptr_pcl_org_src->width + p1_w;
	  int index_tgt = p2_h * ptr_pcl_org_tgt->width + p2_w;
	    
	  if(!isnan(ptr_pcl_org_src->points[index_src].z) && !isnan(ptr_pcl_org_tgt->points[index_tgt].z))
	  {
		match_src_subset->points.push_back(ptr_pcl_org_src->points[index_src]);
		match_tgt_subset->points.push_back(ptr_pcl_org_tgt->points[index_tgt]);
	  }
	}
    
    match_src_subset->width = match_src_subset->points.size();
	match_tgt_subset->width = match_tgt_subset->points.size();
	  
	pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGBA,pcl::PointXYZRGBA> tf_est;
	tf_est.estimateRigidTransformation(*match_src_subset, *match_tgt_subset, initial_transform);	
  }
	  
  match_cin.close();
  
  //load the object point cloud in the source view perception
  char path_pcl_src[1024];
  sprintf(path_pcl_src, "%s/object_point_cloud_in_scene_%03d.pcd", path_current_root.c_str(), index_view_src);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl_src, *ptr_pcl_src);
  
  //load the object point cloud in the target view perception
  char path_pcl_tgt[1024];
  sprintf(path_pcl_tgt, "%s/object_point_cloud_in_scene_%03d.pcd", path_current_root.c_str(), index_view_tgt);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl_tgt(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl_tgt, *ptr_pcl_tgt);
    
  /***************************************************************************
  * Since we have two point clouds A and B, no matter if A is the gradually
  * built object model or the object point cloud of the last scene, we need 
  * to choose (a) registering A to B or (b) registering B to A.
  * Currently we developed two strategies as follows:
  * (1) compare their point cloud sizes;
  * (2) try both (a) and (b), and then choose the one with better accuracy.
  **************************************************************************/
  std::cout<<"SRC Point Cloud Size: "<<ptr_pcl_src->points.size()<<std::endl;
  std::cout<<"TGT Point Cloud Size: "<<ptr_pcl_tgt->points.size()<<std::endl;
  Eigen::Matrix4f transform1, transform2;
  double register_accuracy1, register_accuracy2;
  
  //register A to B
  Eigen::Matrix4f inv_transform = align_point_clouds_using_icp_plus_color(ptr_pcl_tgt, ptr_pcl_src, initial_transform.inverse(), register_accuracy1);
  transform1 = inv_transform.inverse();
  printf("Register TGT view %03d to SRC view %03d, Accuracy: %f \n", index_view_tgt, index_view_src, register_accuracy1);
  
  //register B to A
  transform2 = align_point_clouds_using_icp_plus_color(ptr_pcl_src, ptr_pcl_tgt, initial_transform, register_accuracy2);
  printf("Register SRC view %03d to TGT view %03d, Accuracy: %f \n", index_view_src, index_view_tgt, register_accuracy2);
  
  //choose the better one
  transform = (register_accuracy1 < register_accuracy2) ? transform1 : transform2;
  
  return transform;
}

//#define DEBUG_VIRTUAL_MATES
#ifdef DEBUG_VIRTUAL_MATES
pcl::visualization::PCLVisualizer *pcd_viewer_virtual_mates = new pcl::visualization::PCLVisualizer("Generate Virtual Mates");
#endif
/***********************************************************************
* Detect the overlap between the two point clouds, sample points in both 
* point clouds, and generate mates to otain virtual matched point pairs;
* see more details in the paper "Multivew Registration for Large Data 
* Set".
* @param ptr_pcl_src: the source point cloud
* @param ptr_pcl_tgt: the target point cloud
* @param transfrom: the transformation from the source point cloud to the 
*   target point cloud
* @return : the generated virtual matched point pairs only for the points
*   subsampled in the source point cloud
***********************************************************************/
MatchedPointPairs generate_virtual_mates(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl_src,
                                         const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl_tgt,
                                         const Eigen::Matrix4f &transform)
{
  MatchedPointPairs matched_point_pairs;
  
  //create a kdtree using the target point cloud for searching
  pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
  std::vector<int> result_indices;
  std::vector<float> result_square_distance;
  
  //down-sampling filter
  pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
  sor.setLeafSize(0.005f, 0.005f, 0.005f);
  
  const Eigen::Matrix4f transform_inverse = transform.inverse();
  const float overlap_distance_threshold = 0.006f;  //6mm
  
  /*********************************************************************
  * sample and generate vitual matched point pairs in the SOURCE point cloud
  * transform: src -> tgt
  *********************************************************************/
  //transform the target point cloud to the source point cloud coordinate system;
  //the purpose is to find the overlap region and subsample points
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_transformed_tgt(new pcl::PointCloud<pcl::PointXYZRGBA>());
  pcl::transformPointCloud(*ptr_pcl_tgt, *ptr_transformed_tgt, transform_inverse);
  kdtree.setInputCloud(ptr_transformed_tgt);
  
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_valid_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
  ptr_valid_src->points.clear();
  ptr_valid_src->height = 1;
  ptr_valid_src->is_dense = true;
  
  for(int index_src = 0; index_src < ptr_pcl_src->points.size(); ++index_src)
  {
    if(isnan(ptr_pcl_src->points[index_src].z))
      continue;
    
    kdtree.radiusSearch(ptr_pcl_src->points[index_src], overlap_distance_threshold, result_indices, result_square_distance);
    //no matched point is found within the threshold distance;
    //the point is not in the overlap region
    if(result_indices.empty())
	  continue;
    
    ptr_valid_src->points.push_back(ptr_pcl_src->points[index_src]);
  }
  
  ptr_valid_src->width = ptr_valid_src->points.size();
  
  //subsample
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_filtered_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
  sor.setInputCloud(ptr_valid_src);
  sor.filter(*ptr_filtered_src);

  //generate all the virtual mates for all the subsampled points in the source CS;
  //virtual mates are the ideal mates in the target CS.
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_filtered_src_transformed(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_filtered_src, *ptr_filtered_src_transformed, transform);
  
  matched_point_pairs.ptr_src = ptr_filtered_src;
  matched_point_pairs.ptr_tgt = ptr_filtered_src_transformed;
  
  #ifdef DEBUG_VIRTUAL_MATES
  std::cout<<"ptr_pcl_src size: "<<ptr_pcl_src->points.size()<<std::endl;
  std::cout<<"ptr_valid_src size: "<<ptr_valid_src->points.size()<<std::endl;
  std::cout<<"ptr_filtered_src size: "<< matched_point_pairs.ptr_src->points.size()<<std::endl;
  std::cout<<"ptr_filtered_src_transformed size: "<<matched_point_pairs.ptr_tgt->points.size()<<std::endl;
  pcd_viewer_virtual_mates->removePointCloud("point cloud"); 
  pcd_viewer_virtual_mates->addPointCloud(matched_point_pairs.ptr_tgt, "point cloud");
  std::cout<<"Press q to continue"<<std::endl;
  pcd_viewer_virtual_mates->spin();
  #endif

  return matched_point_pairs;
} 

/***********************************************************************
 * align the point cloud in the current view to both its left and right 
 * views.
 * @param matched_pairs_left: matched point pairs in the current point 
 *   cloud to the point cloud in the left view
 * @param matched_pairs_right: matched point pairs in the current point
 *   cloud to the point cloud in the right view
 * @param pose_left: the pose of the point cloud in the left view
 * @param pose_right: the pose of the point cloud in the right view
 * @return : the refined pose of the point cloud in the current view
 **********************************************************************/
Eigen::Matrix4f align_point_cloud_to_left_and_right_neighboring_views(const MatchedPointPairs &matched_pairs_left,  
                                                                      const MatchedPointPairs &matched_pairs_right,
                                                                      const Eigen::Matrix4f &pose_left,
                                                                      const Eigen::Matrix4f &pose_right)
{
  Eigen::Matrix4f pose_cur_refined;
  
  int num_mates_left = matched_pairs_left.ptr_src->points.size();
  int num_mates_right = matched_pairs_right.ptr_src->points.size();
  const int num_mates = (num_mates_left < num_mates_right) ? num_mates_left : num_mates_right;
  
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_match_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_match_tgt(new pcl::PointCloud<pcl::PointXYZRGBA>);
  ptr_match_src->points.clear();
  ptr_match_tgt->points.clear();
  ptr_match_src->height = 1;
  ptr_match_tgt->height = 1;
  
  //add the virtual matched point pairs between the left and current views
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_base_left(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*matched_pairs_left.ptr_tgt, *ptr_base_left, pose_left);
  
  for(int i = 0; i < num_mates; ++i)
  {
    ptr_match_src->points.push_back(matched_pairs_left.ptr_src->points[i]);
    ptr_match_tgt->points.push_back(ptr_base_left->points[i]);
  }
  
  //add the virtual matched point pairs between the right and current views
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_base_right(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*matched_pairs_right.ptr_tgt, *ptr_base_right, pose_right); 

  for(int i = 0; i < num_mates; ++i)
  {
    ptr_match_src->points.push_back(matched_pairs_right.ptr_src->points[i]);
    ptr_match_tgt->points.push_back(ptr_base_right->points[i]);
  } 
  
  ptr_match_src->width = ptr_match_src->points.size();
  ptr_match_tgt->width = ptr_match_tgt->points.size();
  
  pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGBA,pcl::PointXYZRGBA> tf_est;
  tf_est.estimateRigidTransformation(*ptr_match_src, *ptr_match_tgt, pose_cur_refined);
  
  return pose_cur_refined;
}
                                                                                                                                                                                                           
//#define DEBUG_ROTATION_OPTIMIZATION
#ifdef DEBUG_ROTATION_OPTIMIZATION
pcl::visualization::PCLVisualizer *pcd_viewer_rotation_optimization = new pcl::visualization::PCLVisualizer("Rotation Optimization");
#endif
/***********************************************************************
 * At the end of the 360 degrees rotation modeling procedure, for all the 
 * object point clouds captured in this 360 degrees rotation, globally 
 * optimize all the registration between the point cloud and its 
 * neighboring point cloud.
 * @param transforms_pairwise: the initial pairwise transformation matrices 
 *   and each indicates the transformtion from the (n-1)th object point cloud 
 *   to the nth object point cloud; if n = 0, suppose N is the toal number of 
 *   the object point clouds, then the matrix indicates the transformation 
 *   from the Nth (the vector index is N-1) object point cloud to 
 *   the 1st object point cloud (the vector index is 0)
 * @param ptr_obj_pcls: all the object point clouds (the vector index is
 *   0 to N-1)
 * @return : all the transformation matrice (poses) for all point clouds, using 
 *   the coordinate system of the last point cloud (the nth point cloud) 
 *   as the base coordinate system; the pose indicates how to transform 
 *   the current point cloud to the point cloud in the base coordinate 
 *   system
 **********************************************************************/
std::vector<Eigen::Matrix4f> optimize_globally_registration_in_360_rotation(const std::vector<Eigen::Matrix4f> &transforms_pairwise,
                                                                            const std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> &ptr_obj_pcls)
{
  const int num_views = ptr_obj_pcls.size();
  
  std::vector<Eigen::Matrix4f> poses;
  poses.resize(num_views);
  
  /*********************************************************************
  * using the (num_views-1)th point cloud coordinate system as the base 
  * coordinate system of the object model;
  * initialize all the poses for all the point clouds from different views;
  *********************************************************************/
  poses[num_views - 1] = Eigen::Matrix4f::Identity();
  //view_index: 0 ~ (num_views-1)
  for(int view_index = num_views - 2; view_index >= 0; --view_index)
    poses[view_index] = poses[view_index + 1] * transforms_pairwise[view_index + 1];
    
  for(int view_index = 0; view_index < num_views; ++view_index)
    std::cout<<"Poses View "<<view_index<<" :"<<std::endl<<poses[view_index]<<std::endl;
    
  #ifdef DEBUG_ROTATION_OPTIMIZATION
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_new_model(new pcl::PointCloud<pcl::PointXYZRGBA>());
  ptr_new_model->points.clear();
  ptr_new_model->height = 1;
  ptr_new_model->width = ptr_new_model->points.size();
  ptr_new_model->is_dense = true;
    
  for(int i = 0; i < num_views; ++i)
  {
    //transform the point cloud in each view to the base coordinate system
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_to_base(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::transformPointCloud(*ptr_obj_pcls[i], *ptr_to_base, poses[i]);
    *ptr_new_model += *ptr_to_base;
  }
  
  Eigen::Matrix4f transform_center = Eigen::Matrix4f::Identity();
  for(int i = 0; i < ptr_new_model->points.size(); ++i)
  {
	transform_center(0, 3) += ptr_new_model->points[i].x;
	transform_center(1, 3) += ptr_new_model->points[i].y;
	transform_center(2, 3) += ptr_new_model->points[i].z;
  }
  transform_center(0, 3) = -1.0f * transform_center(0, 3) / ptr_new_model->points.size();
  transform_center(1, 3) = -1.0f * transform_center(1, 3) / ptr_new_model->points.size();
  transform_center(2, 3) = -1.0f * transform_center(2, 3) / ptr_new_model->points.size();
  pcl::transformPointCloud(*ptr_new_model, *ptr_new_model, transform_center);
	
  std::cout<<"Show the partial model before the optimization"<<std::endl;
  pcd_viewer_rotation_optimization->removePointCloud("point cloud"); 
  pcd_viewer_rotation_optimization->addPointCloud(ptr_new_model, "point cloud");
  std::cout<<"Press q to continue"<<std::endl;
  pcd_viewer_rotation_optimization->spin();
  #endif
  
  /*********************************************************************
  * generate virtual mates;
  * each view has two neighboring views: left and right;
  * each entry only stores the matched point pairs in its own point cloud;
  *********************************************************************/
  std::vector<MatchedPointPairs> matched_pairs_right(num_views);
  matched_pairs_right[num_views-1] = generate_virtual_mates(ptr_obj_pcls[num_views-1], ptr_obj_pcls[0], transforms_pairwise[0]);
  for(int view_index = 0; view_index <= num_views - 2; ++view_index)
    matched_pairs_right[view_index] = generate_virtual_mates(ptr_obj_pcls[view_index], ptr_obj_pcls[view_index+1], transforms_pairwise[view_index+1]);
    
  std::vector<MatchedPointPairs> matched_pairs_left(num_views);
  matched_pairs_left[0] = generate_virtual_mates(ptr_obj_pcls[0], ptr_obj_pcls[num_views-1], transforms_pairwise[0].inverse());
  for(int view_index = 1; view_index <= num_views - 1; ++view_index)
    matched_pairs_left[view_index] = generate_virtual_mates(ptr_obj_pcls[view_index], ptr_obj_pcls[view_index-1], transforms_pairwise[view_index].inverse());
  
  /*********************************************************************
  * globally optimize all the poses of all point clouds from different views;
  *********************************************************************/
  std::deque<int> deque_views;
  deque_views.clear();
  
  deque_views.push_back(num_views-1);
  
  int num_iteration = 0;
  const int maximum_num_iteration = num_views * 500;
  
  while((!deque_views.empty()) && (num_iteration < maximum_num_iteration))
  {
	++num_iteration;
	
    int view_index_cur = deque_views[0];
    int view_index_left = (view_index_cur == 0) ? (num_views-1) : (view_index_cur-1);
    int view_index_right = (view_index_cur == (num_views-1)) ? 0 : (view_index_cur+1);
    
    //align the point cloud in the current view to all its neighbors
    Eigen::Matrix4f pose_cur_refined = align_point_cloud_to_left_and_right_neighboring_views(matched_pairs_left[view_index_cur], matched_pairs_right[view_index_cur],
                                                                                             poses[view_index_left], poses[view_index_right]);
    
    const double convergence_threshold = 0.00001;
    Eigen::Matrix4f transform_difference = poses[view_index_cur] - pose_cur_refined;
    
    if(fabs(transform_difference.sum()) > convergence_threshold)
    {
      deque_views.push_back(view_index_left);
      deque_views.push_back(view_index_right);
    }
    
    poses[view_index_cur] = pose_cur_refined;
    deque_views.pop_front();
    
    printf("*******************************************************\n");                                                                                         
    printf("num_iteration %d - View vecotr index %03d \n", num_iteration, view_index_cur);
    std::cout<<"Convergence difference (transformation matrix difference):"<<fabs(transform_difference.sum())<<std::endl;
	std::cout<<"deque_views size: "<<deque_views.size()<<std::endl;
	for(int i = 0; i < deque_views.size(); ++i)
	  std::cout<<deque_views[i]<<" ";
	std::cout<<std::endl;
  }
  
  //project all the points clouds from different view to the coordinate system of the last view;
  //enforce poses[num_views - 1] to be Eigen::Matrix4f::Identity();
  //the main reason is that a lot of work related to pose planing is
  //carried out in the coordinate system of the last view.
  Eigen::Matrix4f transform_to_last = poses[num_views - 1].inverse();
  for(int i = 0; i < num_views; ++i)
    poses[i] = transform_to_last * poses[i];
  
  #ifdef DEBUG_ROTATION_OPTIMIZATION
  std::cout<<"Show the partial model after the optimization"<<std::endl;
  ptr_new_model->points.clear();
  ptr_new_model->height = 1;
  ptr_new_model->width = ptr_new_model->points.size();
  ptr_new_model->is_dense = true;
    
  for(int i = 0; i < num_views; ++i)
  {
    //transform the point cloud in each view to the base coordinate system
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_to_base(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::transformPointCloud(*ptr_obj_pcls[i], *ptr_to_base, poses[i]);
    *ptr_new_model += *ptr_to_base;
  }
	
  transform_center = Eigen::Matrix4f::Identity();
  for(int i = 0; i < ptr_new_model->points.size(); ++i)
  {
	  transform_center(0, 3) += ptr_new_model->points[i].x;
	  transform_center(1, 3) += ptr_new_model->points[i].y;
	  transform_center(2, 3) += ptr_new_model->points[i].z;
  }
  transform_center(0, 3) = -1.0f * transform_center(0, 3) / ptr_new_model->points.size();
  transform_center(1, 3) = -1.0f * transform_center(1, 3) / ptr_new_model->points.size();
  transform_center(2, 3) = -1.0f * transform_center(2, 3) / ptr_new_model->points.size();
  pcl::transformPointCloud(*ptr_new_model, *ptr_new_model, transform_center);
	
  pcd_viewer_rotation_optimization->removePointCloud("point cloud"); 
  pcd_viewer_rotation_optimization->addPointCloud(ptr_new_model, "point cloud");
  std::cout<<"Press q to continue"<<std::endl;
  pcd_viewer_rotation_optimization->spin();
  #endif
  
  return poses;
}

/***********************************************************************
 * Merge the partial object model from the current 360 degrees rotation 
 * procedure to the object model after all previous 360 degrees rotation 
 * procedures;
 * The transformation from the object model in the last 360 degree rotation 
 * to the partial object model in the current 360 degree rotation is saved;
 * The merged object model is also saved.
 * @param path_current_root: the root working directory
 * @param model_index_src: the source object model index
 * @param model_index_tgt: the target object model index
 * @param transform_estimated: the estimated transformation from the source 
 *   object model to the target object model;
 * @return : the accurate transformation from the source object model to
 *   the target object model
***********************************************************************/
Eigen::Matrix4f register_models_incrementally_between_360_rotations(const std::string &path_current_root,
                                                                    const int &model_index_src,
                                                                    const int &model_index_tgt,
                                                                    const Eigen::Matrix4f &transform_estimated)
{
  Eigen::Matrix4f transform_accurate = Eigen::Matrix4f::Identity();
  
  //save the refined partial model for future use
  char path_refined_model_backup[1024];
      
  sprintf(path_refined_model_backup, "%s/object_model_after_360_rotation_%03d.pcd", path_current_root.c_str(), model_index_src);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_src(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_src);
      
  sprintf(path_refined_model_backup, "%s/refined_partial_object_model_%03d.pcd", path_current_root.c_str(), model_index_tgt);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_tgt(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_tgt);
  
  //transform the source point cloud to the CS of the target point cloud based on the estimated transformation matrix
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_src_estimated(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_model_src, *ptr_model_src_estimated, transform_estimated);
  
  //register the two point clouds
  
  //two many points, downsample for the registration
  std::cout<<"SRC Model Size: "<<ptr_model_src_estimated->points.size()<<std::endl;
  std::cout<<"TGT Model Size: "<<ptr_model_tgt->points.size()<<std::endl;
      
  // Create the filtering object
  pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
  sor.setLeafSize(0.002f, 0.002f, 0.002f);
  
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_src_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
  sor.setInputCloud(ptr_model_src_estimated);
  sor.filter(*ptr_model_src_filtered);
      
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_tgt_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
  sor.setInputCloud(ptr_model_tgt);
  sor.filter(*ptr_model_tgt_filtered);
  
  std::cout<<"SRC Model Size After Filtering: "<<ptr_model_src_filtered->points.size()<<std::endl;
  std::cout<<"TGT Model Size After Filtering: "<<ptr_model_tgt_filtered->points.size()<<std::endl;

  //try registration in both directions
  Eigen::Matrix4f transform1, transform2;
  double register_accuracy1, register_accuracy2;
  
  //register A to B
  Eigen::Matrix4f inv_transform = align_point_clouds_using_icp_plus_color_between_models(ptr_model_tgt_filtered, ptr_model_src_filtered, Eigen::Matrix4f::Identity(), register_accuracy1);
  transform1 = inv_transform.inverse();
  std::cout<<"TGT to SRC registration accuracy: "<<register_accuracy1<<std::endl;
  
  //register B to A
  transform2 = align_point_clouds_using_icp_plus_color_between_models(ptr_model_src_filtered, ptr_model_tgt_filtered, Eigen::Matrix4f::Identity(), register_accuracy2);
  std::cout<<"SRC to TGT registration accuracy: "<<register_accuracy2<<std::endl;
  
  //choose the better one
  transform_accurate = (register_accuracy1 < register_accuracy2) ? transform1 : transform2;
  
  //backup the transformation between two models from different 360 degrees rotation procedures
  Eigen::Matrix4f transform_between_models = transform_accurate * transform_estimated;
  std::ofstream cout_transform;
  char path_transform[1024];
  sprintf(path_transform, "%s/transformation_from_model_%03d_to_model_%03d.txt", path_current_root.c_str(), model_index_src, model_index_tgt); 
  cout_transform.open(path_transform);
  if (!cout_transform.is_open())
  {
    std::cout<<"Error: fail to open the file to save the transform information between two models from different rotation procedures!"<<std::endl;
    return transform_accurate;
  }
  cout_transform<<transform_between_models<<std::endl;
  cout_transform.close();
  
  //backup the object model after the current 360 degree rotation procedure
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_src_accurate(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_model_src_estimated, *ptr_model_src_accurate, transform_accurate);
  *ptr_model_tgt += *ptr_model_src_accurate;
  
  sprintf(path_refined_model_backup, "%s/object_model_after_360_rotation_%03d.pcd", path_current_root.c_str(), model_index_tgt);
  pcl::io::savePCDFileASCII(path_refined_model_backup, *ptr_model_tgt);
      
  return transform_accurate;
}


