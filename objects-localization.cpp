#include "objects-localization.h"

/*
  Localize the white board by detecting the plane in the point cloud
  @param cloud : the given point cloud of the current scene
  @param dist_threshod : used for the plane detection
  @return : returns an array indicating which points belong to the table plane
*/
std::vector<std::vector<bool> > localization_for_table_plane(const pcl::PointCloud<pcl::PointXYZRGBA> &cloud,
                                                             const double &dist_threshold){
  std::vector<std::vector<bool> > table_plane(cloud.height, std::vector<bool>(cloud.width));																 
  
  int img_h = cloud.height;
  int img_w = cloud.width;

  //initilize with all false, no position
  for(int i = 0; i < img_h; ++i)
    for(int j = 0; j < img_w; ++j)
      table_plane[i][j] = false;

  //use plane detection algorithm
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  
  //create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
  //optional
  seg.setOptimizeCoefficients (true);
  //mandatory
  seg.setModelType(pcl::SACMODEL_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setDistanceThreshold(dist_threshold);

  seg.setInputCloud(cloud.makeShared ());
  seg.segment(*inliers, *coefficients);
	
  std::cout<<"plane ax+by+cz+d=0 coefficients:"<<std::endl;
  std::cout<<coefficients->values[0]<<" "\
           <<coefficients->values[1]<<" "\
           <<coefficients->values[2]<<" "\
           <<coefficients->values[3]<<std::endl;

  for(int i = 0; i < inliers->indices.size(); ++i){
	int index = inliers->indices[i];
	if(!isnan(cloud.points[index].x) && !isnan(cloud.points[index].y) && !isnan(cloud.points[index].z)){
	  table_plane[index/img_w][index%img_w] = true;
    }
  }

  return table_plane;
}

/*
  Find out the table region, which includes both the table plane and 
    objects on the table.
  Assumption : the table region is in the middle, which occupies the most 
    space of the image.
  @param table_plane : an array indicating which points belong to the table plane
  @param img_h : the height of the image
  @param img_w : the width of the image
  @return : returns an array indicating which points belong to the table region
*/
std::vector<std::vector<bool> > detect_table_region(const std::vector<std::vector<bool> > &table_plane,
                                                    const int &img_h, const int &img_w){
  std::vector<std::vector<bool> > table_region(img_h, std::vector<bool>(img_w));

  //initilize with all true
  for(int i = 0; i < img_h; ++i)
    for(int j = 0; j < img_w; ++j)
      table_region[i][j] = true;
      
  //start from the edges of the image, and find out all the regions which 
  //are outside the table plane.
  std::vector<std::vector<bool> > visited(img_h, std::vector<bool>(img_w));
  for(int i = 0; i < img_h; ++i)
    for(int j = 0; j < img_w; ++j)
      visited[i][j] = false;
  
  std::stack<cv::Point2i> st;
  //push all the points on the edges of the image if they are not on the table plane
  for(int i = 0; i < img_w; ++i){
	cv::Point2i push_node;
	if(!table_plane[0][i] && !visited[0][i]){
	  push_node.y = 0;
	  push_node.x = i;
	  st.push(push_node);
	  visited[0][i] = true;
	}
	if(!table_plane[img_h-1][i] && !visited[img_h-1][i]){
	  push_node.y = img_h - 1;
	  push_node.x = i;
	  st.push(push_node);
	  visited[img_h-1][i] = true;
	}
  }
  
  for(int i = 0; i < img_h; ++i){
    cv::Point2i push_node;
    if(!table_plane[i][0] && !visited[i][0]){
	  push_node.y = i;
	  push_node.x = 0;
	  st.push(push_node);
	  visited[i][0] = true;
	}
	if(!table_plane[i][img_w-1] && !visited[i][img_w-1]){
	  push_node.y = i;
	  push_node.x = img_w - 1;
	  st.push(push_node);
	  visited[i][img_w-1] = true;
	}
  }
  
  while(st.size()){
	cv::Point2i cur_node;
	cur_node = st.top();
	st.pop();
	
	table_region[cur_node.y][cur_node.x] = false;
	
	//push unvisited neighbors that do not belong to the talbe plane
	cv::Point2i push_node;
	
	push_node.y = cur_node.y - 1;
	push_node.x = cur_node.x - 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
	
	push_node.y = cur_node.y - 1;
	push_node.x = cur_node.x;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y - 1;
	push_node.x = cur_node.x + 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y;
	push_node.x = cur_node.x - 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y;
	push_node.x = cur_node.x + 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y + 1;
	push_node.x = cur_node.x - 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y + 1;
	push_node.x = cur_node.x;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
    
	push_node.y = cur_node.y + 1;
	push_node.x = cur_node.x + 1;
	if(push_node.y > 0 && push_node.y < img_h && 
	   push_node.x > 0 && push_node.x < img_w &&
	   !table_plane[push_node.y][push_node.x] &&
	   !visited[push_node.y][push_node.x]){
	  st.push(push_node);
	  visited[push_node.y][push_node.x] = true;
    }
	
  }
  
  return table_region;
}

/*
  Segment the table region based on the connectivity to find out all 
    the objects on the table.
  @param table_region : an array indicating which points belong to the table region
  @param table_plane : an array indicating which points belong to the table plane
  @param img_h : the height of the image
  @param img_w: the width of the image
  @num_seg: returns the number of the segments of the table region
  @return: returns all the segments of the table region based on the connectivity, 0 means the background
*/
std::vector<std::vector<int> > detect_all_objects_on_table(const std::vector<std::vector<bool> > &table_region,
                                                           const std::vector<std::vector<bool> > &table_plane,
                                                           const int &img_h, const int &img_w, int &num_seg){
  
  std::vector<std::vector<int> > segment_indices(img_h, std::vector<int>(img_w));
  num_seg = 0;
  
  //initilize with all true
  for(int i = 0; i < img_h; ++i)
    for(int j = 0; j < img_w; ++j)
      segment_indices[i][j] = 0;
      
  std::vector<std::vector<bool> > visited(img_h, std::vector<bool>(img_w));
  for(int i = 0; i < img_h; ++i)
    for(int j = 0; j < img_w; ++j)
      visited[i][j] = false;
      
  for(int i = 0; i < img_h; ++i){
    for(int j = 0; j < img_w; ++j){
	  if(visited[i][j] || !table_region[i][j] || table_plane[i][j])
	    continue;
	  
	  //find out a new segment
	  ++num_seg;
	  std::stack<cv::Point2i> st;
	  cv::Point2i fst_node;
	  fst_node.y = i;
	  fst_node.x = j;
	  st.push(fst_node);
	  visited[i][j] = true;
	  
	  while(st.size()){
		cv::Point2i cur_node;
		cur_node = st.top();
		st.pop();
		
		segment_indices[cur_node.y][cur_node.x] = num_seg;
		  
	    //push unvisited neighbors that belongs to the same segment
	    cv::Point2i push_node;
	
		push_node.y = cur_node.y - 1;
		push_node.x = cur_node.x - 1;
		if(push_node.y > 0 && push_node.y < img_h && 
		   push_node.x > 0 && push_node.x < img_w &&
		   table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
		   !visited[push_node.y][push_node.x]){
		  st.push(push_node);
		  visited[push_node.y][push_node.x] = true;
		}
	
	    push_node.y = cur_node.y - 1;
	    push_node.x = cur_node.x;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y - 1;
	    push_node.x = cur_node.x + 1;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y;
	    push_node.x = cur_node.x - 1;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y;
	    push_node.x = cur_node.x + 1;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y + 1;
	    push_node.x = cur_node.x - 1;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y + 1;
	    push_node.x = cur_node.x;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
    
	    push_node.y = cur_node.y + 1;
	    push_node.x = cur_node.x + 1;
	    if(push_node.y > 0 && push_node.y < img_h && 
	       push_node.x > 0 && push_node.x < img_w &&
	       table_region[push_node.y][push_node.x] &&
		   !table_plane[push_node.y][push_node.x] &&
	       !visited[push_node.y][push_node.x]){
	      st.push(push_node);
	      visited[push_node.y][push_node.x] = true;
        }
		
	  }
	}
  }  
  
  return segment_indices;
}


