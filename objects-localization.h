/*
This section documents a collection of algorithms used for the localization 
of objects in the image.
*/

#ifndef _OBJECTS_LOCALIZATION_H_
#define _OBJECTS_LOCALIZATION_H_

#include <iostream>
#include <cmath>
#include <vector>
#include <stack>

#include "opencv2/opencv.hpp"

#include "pcl/io/pcd_io.h"
#include "pcl/point_types.h"
#include "pcl/visualization/cloud_viewer.h"
#include "pcl/ModelCoefficients.h"
#include "pcl/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"

/*
  Localize the white board by detecting the plane in the point cloud
  @param cloud : the given point cloud of the current scene
  @param dist_threshod : used for the plane detection
  @return : returns an array indicating which points belong to the table plane
*/
std::vector<std::vector<bool> > localization_for_table_plane(const pcl::PointCloud<pcl::PointXYZRGBA> &cloud,
                                                             const double &dist_threshold);
                                                             
/*
  Find out the table region, which includes both the table plane and 
    objects on the table.
  Assumption : the table region is in the middle, which occupies the most 
    space of the image.
  @param table_plane : an array indicating which points belong to the table plane
  @param img_h : the height of the image
  @param img_w : the width of the image
  @return : returns an array indicating which points belong to the table region
*/
std::vector<std::vector<bool> > detect_table_region(const std::vector<std::vector<bool> > &table_plane,
                                                    const int &img_h, const int &img_w);  
                                                    
/*
  Segment the table region based on the connectivity to find out all 
    the objects on the table.
  @param table_region : an array indicating which points belong to the table region
  @param table_plane : an array indicating which points belong to the table plane
  @param img_h : the height of the image
  @param img_w: the width of the image
  @num_seg: returns the number of the segments of the table region
  @return: returns all the segments of the table region based on the connectivity, 0 means the background
*/
std::vector<std::vector<int> > detect_all_objects_on_table(const std::vector<std::vector<bool> > &table_region,
                                                           const std::vector<std::vector<bool> > &table_plane,
                                                           const int &img_h, const int &img_w, int &num_seg);                                                        

#endif //_OBJECTS_LOCALIZATION_H_
