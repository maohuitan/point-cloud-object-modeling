#include "pose-planning.h"

/**********
 * To obtain a oriented bounding box which is parallel to the XY plane for all points
 * @param ptr_pcl: all the points
 * @param bbox_centroid: the centroid position (x, y, z)
 * @param bbox_directions: the three directions of the bounding box
 * @param length_1st: the length of the bounding box in the direction of bbox_directions.col(0)
 * @param length_2nd: the length of the bounding box in the direction of bbox_directions.col(1)
 * @param length_3rd: the length of the bounding box in the direction of bbox_directions.col(2)
**********/
bool obtain_oriented_bounding_box_on_XY_plane(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_pcl,
                                              Eigen::Vector3f &bbox_centroid,
                                              Eigen::Matrix3f &bbox_directions,
                                              float &length_1st, float &length_2nd, float &length_3rd)
{
  //since the other direction is z axis, only focus on the XY plane first
  float sum_x = 0.0f;
  float sum_y = 0.0f;
  float sum_z = 0.0f;
  
  //compute the average value of x, y and z
  for(int i = 0; i < ptr_pcl->points.size(); ++i){
	sum_x += ptr_pcl->points[i].x;
	sum_y += ptr_pcl->points[i].y;
	sum_z += ptr_pcl->points[i].z;
  }
  
  float average_x = sum_x / ptr_pcl->points.size();
  float average_y = sum_y / ptr_pcl->points.size();
  float average_z = sum_z / ptr_pcl->points.size();
  
  float sum_xx = 0.0f;
  float sum_xy = 0.0f;
  float sum_yy = 0.0f;
  
  //compute the variances and covariances of x and y
  for(int i = 0; i < ptr_pcl->points.size(); ++i){
    sum_xx += (ptr_pcl->points[i].x - average_x) * (ptr_pcl->points[i].x - average_x);
    sum_xy += (ptr_pcl->points[i].x - average_x) * (ptr_pcl->points[i].y - average_y);
    sum_yy += (ptr_pcl->points[i].y - average_y) * (ptr_pcl->points[i].y - average_y);
  }
  
  float average_xx = sum_xx / ptr_pcl->points.size();
  float average_xy = sum_xy / ptr_pcl->points.size();
  float average_yy = sum_yy / ptr_pcl->points.size();
  
  Eigen::Matrix2f covariance;
  covariance(0, 0) = average_xx;
  covariance(0, 1) = average_xy;
  covariance(1, 0) = average_xy;
  covariance(1, 1) = average_yy;
  
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
  Eigen::Matrix2f pca_eigenvectors_xy = eigen_solver.eigenvectors();
  
  //recover back to the 3d
  Eigen::Matrix3f pca_eigenvectors_xyz;
  pca_eigenvectors_xyz(0, 0) = pca_eigenvectors_xy(0, 0);
  pca_eigenvectors_xyz(1, 0) = pca_eigenvectors_xy(1, 0);
  pca_eigenvectors_xyz(2, 0) = 0.0f;
  //ensure the z axis is (0.0f, 0.0f, 1.0f)  
  pca_eigenvectors_xyz(0, 2) = 0.0f;
  pca_eigenvectors_xyz(1, 2) = 0.0f;
  pca_eigenvectors_xyz(2, 2) = 1.0f;
  pca_eigenvectors_xyz.col(1) = pca_eigenvectors_xyz.col(2).cross(pca_eigenvectors_xyz.col(0));
  /*
  pca_eigenvectors_xyz(0, 1) = pca_eigenvectors_xy(0, 1);
  pca_eigenvectors_xyz(1, 1) = pca_eigenvectors_xy(1, 1);
  pca_eigenvectors_xyz(2, 1) = 0.0f;  
  pca_eigenvectors_xyz.col(2) = pca_eigenvectors_xyz.col(0).cross(pca_eigenvectors_xyz.col(1));
  */
  
  std::cout<<"pca_eigenvectors_xyz: "<<std::endl<<pca_eigenvectors_xyz<<std::endl;
  
  //transform the original cloud to the origin where the principal components correspond to the axes
  Eigen::Vector3f pca_centroid(average_x, average_y, average_z);
  Eigen::Matrix4f pca_transform(Eigen::Matrix4f::Identity());
  pca_transform.block<3, 3>(0, 0) = pca_eigenvectors_xyz.transpose();
  pca_transform.block<3, 1>(0, 3) = -1.f * (pca_transform.block<3, 3>(0, 0) * pca_centroid);
  
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pca_projected(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_pcl, *ptr_pca_projected, pca_transform);
  
  /*
  //visualize the point cloud in the PCA axes
  pcl::visualization::PCLVisualizer *pcd_viewer;
  pcd_viewer = new pcl::visualization::PCLVisualizer("Show the object model in PCA axes");
  pcd_viewer->addCoordinateSystem(1.0);
  pcd_viewer->initCameraParameters();
  
  pcd_viewer->addPointCloud(ptr_pca_projected, "final model in PCA axes");
  PCL_INFO("Press q to stop.\n");
  pcd_viewer->spin();
  */
  
  // Get the minimum and maximum points of the transformed cloud.
  pcl::PointXYZRGBA min_point, max_point;
  pcl::getMinMax3D(*ptr_pca_projected, min_point, max_point);
  const Eigen::Vector3f mean_diagonal = 0.5f * (max_point.getVector3fMap() + min_point.getVector3fMap());
  const Eigen::Vector3f bbox_translation = pca_eigenvectors_xyz * mean_diagonal + pca_centroid;
  
  bbox_centroid = bbox_translation;
  bbox_directions = pca_eigenvectors_xyz;
  length_1st = max_point.x - min_point.x;
  length_2nd = max_point.y - min_point.y;
  length_3rd = max_point.z - min_point.z;

  return true;
}

/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the grasp difficulties for the gripper to grasp the 
 * modeling object from 5 bounding box sides in 6 different ways (+x side in y direction, 
 * -x side in y direction, +y side in x direction, -y side in x direction, 
 * +z side in x direction, +z side in y direction)
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return: a vector in which each entry evaluates the grasp difficulty for the gripper to grasp 
 *          the modeling object from a pre-defined bounding box side; the object can not be grasped 
 *          from the pre-defined bounding box side if its entry value is zero.
**********/
std::vector<float> evaluate_grasp_difficulty_for_five_bounding_box_sides(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                         float &length_x, float &length_y, float &length_z)
{
  std::vector<float> grasp_difficulty(6, 0.0f);
  
  const float MAXIMUM_GRIPPER_OPEN_WIDTH = 0.15f;
  
  //validate the 5 bounding box sides in 6 different ways
  //(+x side in y direction, -x side in y direction, 
  // +y side in x direction, -y side in x direction, 
  // +z side in x direction, +z side in y direction) one by one
  
  //grasp_difficulty(0): +x side in y direction
  if(length_y < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[0] = 0.5f;
  
  //grasp_difficulty(1): -x side in y direction
  if(length_y < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[1] = 0.5f;
  
  //grasp_difficulty(2): +y side in x direction
  if(length_x < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[2] = 0.5f;
  
  //grasp_difficulty(3): -y side in x direction
  if(length_x < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[3] = 0.5f;
  
  //grasp_difficulty(4): +z side in x direction
  if(length_x < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[4] = 1.0f;
  
  //grasp_difficulty(5): +z side in y direction)
  if(length_y < MAXIMUM_GRIPPER_OPEN_WIDTH) 
    grasp_difficulty[5] = 1.0f;
  
  return grasp_difficulty;
}

/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the push difficulties for the gripper after using 
 * the other 5 bounding box sides (+x, -x, +y, -y, +z) above the table as the bottom sides
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return: a vector in which each entry evaluates the push difficulty for the gripper 
 *   after using the other 5 bounding box sides (+x, -x, +y, -y, +z) above the table 
 *   as the bottom sides; the object can not be pushed from the pre-defined bounding box 
 *   side if its entry value is zero.
**********/
std::vector<float> evaluate_push_difficulty_for_using_other_five_sides_as_bottom(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                                 float &length_x, float &length_y, float &length_z)
{
  std::vector<float> push_difficulty(5, 0.0f);
  
  const float MINIMUM_PUSH_WIDTH = 0.1f;
  
  //validate the push difficulty for using the other 5 bounding box sides as the bottom sides
  //+x side: push in y or z direction
  //-x side: push in y or z direction
  //+y side: push in x or z direction
  //-y side: push in x or z direction
  //+z side: push in x or y direction
  
  //push_difficulty(0): using +x side as the new bottom side
  if(length_y > MINIMUM_PUSH_WIDTH || length_z > MINIMUM_PUSH_WIDTH) 
    push_difficulty[0] = 1.0f;
  
  //push_difficulty(1): using -x side as the new bottom side
  if(length_y > MINIMUM_PUSH_WIDTH || length_z > MINIMUM_PUSH_WIDTH) 
    push_difficulty[1] = 1.0f;
  
  //push_difficulty(2): using +y side as the new bottom side
  if(length_x > MINIMUM_PUSH_WIDTH || length_z > MINIMUM_PUSH_WIDTH) 
    push_difficulty[2] = 1.0f;
  
  //push_difficulty(3): using -y side as the new bottom side
  if(length_x > MINIMUM_PUSH_WIDTH || length_z > MINIMUM_PUSH_WIDTH) 
    push_difficulty[3] = 1.0f;
  
  //push_difficulty(4): using +z side as the new bottom side
  if(length_x > MINIMUM_PUSH_WIDTH || length_y > MINIMUM_PUSH_WIDTH) 
    push_difficulty[4] = 1.0f;
  
  return push_difficulty;
}

/**********
 * The point cloud of the modeling object is transformed to the bounding box coordinate system;
 * This function is used for evaluating the stand stabilities of the modeling 
 * object if it stands on the table with 5 different bounding box sides (+x side, 
 * -x side, +y side, -y side, +z side) as the bottom sides.
 * @param ptr_object_model: the point cloud of the modeling object in the bounding box coordinate system
 * @param length_x: the side length of the bounding box in x axis direction
 * @param length_y: the side length of the bounding box in y axis direction
 * @param length_z: the side length of the bounding box in z axis direction
 * @return : a vector in which each entry evaluates the stand stability if the modeling 
 *          object stands with a pre-defined bounding box side as the bottom side; the object can not 
 *          stand on the table with the pre-defined bounding box side as the bottom side 
 *          if its entry value is zero.
**********/
std::vector<float> evaluate_stand_stability_for_five_bounding_box_sides(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &ptr_object_model,
                                                                        float &length_x, float &length_y, float &length_z)
{
  std::vector<float> stand_stability(5, 0.0f);
  
//the maximum distance of the point away from a plane, if the point is considered to be in the plane for object standing
#define MAX_AWAY_PLANE_DISTANCE 0.02

  /*********************************************************************
   * use a simple strategy to check if the object can stand stably:
   * (1)project all the points whose distance away from the pre-defined bounding box side
   *    is within MAX_AWAY_PLANE_DISTANCE to the pre-defined bounding box side;
   * (2)project the centroid of the bounding box to the pre-defined bounding box side, it 
   *    is always (0, 0) actually
   * (3)create a 0.04m * 0.04m rectangle (40*40 1mm*1mm grids), if 75% of the grids are 
   *    occupied by the projection of those points, then it can stand on the plane with 
   *    this pre-defined bounding box side as the bottom side
   ********************************************************************/
    
  std::vector<std::vector<std::vector<bool> > > grids(5, std::vector<std::vector<bool> >(40, std::vector<bool>(40, false)));
   
  for(int i = 0; i < ptr_object_model->points.size(); ++i)
  {
     
    //validate the 5 bounding box sides (+x side,-x side, +y side, -y side, +z side) one by one
     
    //+x side: grids[0][][]
    if(ptr_object_model->points[i].x > 0.5f * length_x - MAX_AWAY_PLANE_DISTANCE)
    {
	  //project the point within the standing rectangle to (y, z) grids
	  if(fabs(ptr_object_model->points[i].y) < 0.02f && fabs(ptr_object_model->points[i].z) < 0.02f)
	  {
	    int index1 = int((ptr_object_model->points[i].y + 0.02f) * 1000);
	    int index2 = int((ptr_object_model->points[i].z + 0.02f) * 1000);
	    grids[0][index1][index2] = true;
	  }
    }
  
    //-x side: grids[1][][]
    if(ptr_object_model->points[i].x < -0.5f * length_x + MAX_AWAY_PLANE_DISTANCE)
    {
	  //project the point within the standing rectangle to (y, z) grids
	  if(fabs(ptr_object_model->points[i].y) < 0.02f && fabs(ptr_object_model->points[i].z) < 0.02f)
	  {
	    int index1 = int((ptr_object_model->points[i].y + 0.02f) * 1000);
	    int index2 = int((ptr_object_model->points[i].z + 0.02f) * 1000);
	    grids[1][index1][index2] = true;
	  }
    }
    
    //+y side: grids[2][][]
    if(ptr_object_model->points[i].y > 0.5f * length_y - MAX_AWAY_PLANE_DISTANCE)
    {
	  //project the point within the standing rectangle to (x, z) grids
	  if(fabs(ptr_object_model->points[i].x) < 0.02f && fabs(ptr_object_model->points[i].z) < 0.02f)
	  {
	    int index1 = int((ptr_object_model->points[i].x + 0.02f) * 1000);
	    int index2 = int((ptr_object_model->points[i].z + 0.02f) * 1000);
	    grids[2][index1][index2] = true;
	  }
	}
    
    //-y side: grids[3][][]
    if(ptr_object_model->points[i].y < - 0.5f * length_y + MAX_AWAY_PLANE_DISTANCE)
    {
	  //project the point within the standing rectangle to (x, z) grids
	  if(fabs(ptr_object_model->points[i].x) < 0.02f && fabs(ptr_object_model->points[i].z) < 0.02f)
	  {
	    int index1 = int((ptr_object_model->points[i].x + 0.02f) * 1000);
	    int index2 = int((ptr_object_model->points[i].z + 0.02f) * 1000);
	    grids[3][index1][index2] = true;
	  }
	}
	
	//+z side: grids[4][][]
	if(ptr_object_model->points[i].z > 0.5f * length_z - MAX_AWAY_PLANE_DISTANCE)
	{
	  //project the point within the standing rectangle to (x, y) grids
	  if(fabs(ptr_object_model->points[i].x) < 0.02f && fabs(ptr_object_model->points[i].y) < 0.02f)
	  {
	    int index1 = int((ptr_object_model->points[i].x + 0.02f) * 1000);
	    int index2 = int((ptr_object_model->points[i].y + 0.02f) * 1000);
	    grids[4][index1][index2] = true;
	  }
	}
  }
  
  for(int k = 0; k <= 4; ++k)
  {
    int num_count = 0;
    for(int i = 0;i < 40; ++i)
      for(int j = 0; j < 40; ++j)
        if(grids[k][i][j])
          ++num_count;
    if(num_count > 1200)  //0.75*1600
      stand_stability[k] = num_count / 1600.0f;
  }
  
  return stand_stability;
}

/**********
 * Generate all the grasp solutions for changing the bottom of the modeling object 
 * based on the bounding box in the bounding box coordinate system.
 * @return: return all the default grasp solutions
**********/
std::vector<BttmChgSol> generate_all_change_bottom_solutions_for_bounding_box()
{
  std::vector<BttmChgSol> solutions_all(14);
  
  Eigen::Vector3f direction_x_pos( 1.0f,  0.0f,  0.0f);
  Eigen::Vector3f direction_x_neg(-1.0f,  0.0f,  0.0f);
  Eigen::Vector3f direction_y_pos( 0.0f,  1.0f,  0.0f);
  Eigen::Vector3f direction_y_neg( 0.0f, -1.0f,  0.0f);
  Eigen::Vector3f direction_z_pos( 0.0f,  0.0f,  1.0f);
  Eigen::Vector3f direction_z_neg( 0.0f,  0.0f, -1.0f);
  
  int index_grasp_side;
  int index_set_side;
  Eigen::Vector3f direction_grasp;
  Eigen::Vector3f direction_set;
  
  //grasp_difficulty(0): +x side in y direction
  //grasp_difficulty(1): -x side in y direction
  //grasp_difficulty(2): +y side in x direction
  //grasp_difficulty(3): -y side in x direction
  //grasp_difficulty(4): +z side in x direction
  //grasp_difficulty(5): +z side in y direction
  
  //+x side: grids[0][][]
  //-x side: grids[1][][]
  //+y side: grids[2][][]
  //-y side: grids[3][][]
  //+z side: grids[4][][]
  
  //grasp: +x side in y direction, set: -x side
  solutions_all[0].index_grasp_side = 0;
  solutions_all[0].index_set_side   = 1;
  solutions_all[0].direction_grasp  = direction_x_pos;
  solutions_all[0].direction_set    = direction_x_neg;
  
  //grasp: +x side in y direction, set: +z side
  solutions_all[1].index_grasp_side = 0;
  solutions_all[1].index_set_side   = 4;
  solutions_all[1].direction_grasp  = direction_x_pos;
  solutions_all[1].direction_set    = direction_z_pos;
  
  //grasp: -x side in y direction, set: +x side
  solutions_all[2].index_grasp_side = 1;
  solutions_all[2].index_set_side   = 0;
  solutions_all[2].direction_grasp  = direction_x_neg;
  solutions_all[2].direction_set    = direction_x_pos;
  
  //grasp: -x side in y direction, set: +z side
  solutions_all[3].index_grasp_side = 1;
  solutions_all[3].index_set_side   = 4;
  solutions_all[3].direction_grasp  = direction_x_neg;
  solutions_all[3].direction_set    = direction_z_pos;
  
  //grasp: +y side in x direction, set: -y side
  solutions_all[4].index_grasp_side = 2;
  solutions_all[4].index_set_side   = 3;
  solutions_all[4].direction_grasp  = direction_y_pos;
  solutions_all[4].direction_set    = direction_y_neg;
  
  //grasp: +y side in x direction, set: +z side
  solutions_all[5].index_grasp_side = 2;
  solutions_all[5].index_set_side   = 4;
  solutions_all[5].direction_grasp  = direction_y_pos;
  solutions_all[5].direction_set    = direction_z_pos;
  
  //grasp: -y side in x direction, set: +y side
  solutions_all[6].index_grasp_side = 3;
  solutions_all[6].index_set_side   = 2;
  solutions_all[6].direction_grasp  = direction_y_neg;
  solutions_all[6].direction_set    = direction_y_pos;
  
  //grasp: -y side in x direction, set: +z side
  solutions_all[7].index_grasp_side = 3;
  solutions_all[7].index_set_side   = 4;
  solutions_all[7].direction_grasp  = direction_y_neg;
  solutions_all[7].direction_set    = direction_z_pos;
  
  //grasp: +z side in x direction, set: +y side
  solutions_all[8].index_grasp_side = 4;
  solutions_all[8].index_set_side   = 2;
  solutions_all[8].direction_grasp  = direction_z_pos;
  solutions_all[8].direction_set    = direction_y_pos;
  
  //grasp: +z side in x direction, set: -y side
  solutions_all[9].index_grasp_side = 4;
  solutions_all[9].index_set_side   = 3;
  solutions_all[9].direction_grasp  = direction_z_pos;
  solutions_all[9].direction_set    = direction_y_neg;
  
  //grasp: +z side in y direction, set: +x side
  solutions_all[10].index_grasp_side = 5;
  solutions_all[10].index_set_side   = 0;
  solutions_all[10].direction_grasp  = direction_z_pos;
  solutions_all[10].direction_set    = direction_x_pos;
  
  //grasp: +z side in y direction, set: -x side
  solutions_all[11].index_grasp_side = 5;
  solutions_all[11].index_set_side   = 1;
  solutions_all[11].direction_grasp  = direction_z_pos;
  solutions_all[11].direction_set    = direction_x_neg;

  //grasp: +z side in x direction, set: +z side
  solutions_all[12].index_grasp_side = 4;
  solutions_all[12].index_set_side   = 4;
  solutions_all[12].direction_grasp  = direction_z_pos;
  solutions_all[12].direction_set    = direction_z_pos;
  
  //grasp: +z side in x direction, set: +z side
  solutions_all[13].index_grasp_side = 5;
  solutions_all[13].index_set_side   = 4;
  solutions_all[13].direction_grasp  = direction_z_pos;
  solutions_all[13].direction_set    = direction_z_pos;
  
  return solutions_all;  
}

static void print_voxel_map_information(const std::vector<std::vector<std::vector<int> > > &voxel_map)
{
  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_unmarked = 0;
  int num_empty = 0;
  int num_occupied = 0;
  int num_occluded = 0;
  int num_occplane = 0;
  
  for(int i = 0; i < voxel_map.size(); ++i)
    for(int j = 0; j < voxel_map[0].size(); ++j)
      for(int k = 0; k < voxel_map[0][0].size(); ++k)
        if(voxel_map[i][j][k] == 0)
          ++num_unmarked;
        else if(voxel_map[i][j][k] == 1)
          ++num_empty;
        else if(voxel_map[i][j][k] == 2)
          ++num_occupied;
        else if(voxel_map[i][j][k] == 3)
          ++num_occluded;
        else if(voxel_map[i][j][k] == 4)
          ++num_occplane;
  
  printf("**********Print Voxel Map Information**********\n");
  printf("Unmarked Voxels: %d\n", num_unmarked);
  printf("Empty Voxels   : %d\n", num_empty);
  printf("Occupied Voxels: %d\n", num_occupied);
  printf("Occluded Voxels: %d\n", num_occluded);
  printf("Occplane Voxels: %d\n", num_occplane);
}

//#define DEBUG_VOXEL_MAP
#ifdef DEBUG_VOXEL_MAP
pcl::visualization::PCLVisualizer *pcd_viewer_voxel_map = new pcl::visualization::PCLVisualizer("Voxel Map");
#endif
static void show_voxel_map_information(const std::vector<std::vector<std::vector<int> > > &voxel_map, const float &voxel_unit)
{
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_voxel_map(new pcl::PointCloud<pcl::PointXYZRGBA>());
  ptr_voxel_map->points.clear();
  ptr_voxel_map->height = 1;
  ptr_voxel_map->is_dense = true;
  
  for(int i = 0; i < voxel_map.size()/2; ++i) //show the cross section of the voxel map
    for(int j = 0; j < voxel_map[0].size(); ++j)  
      for(int k = 0; k < voxel_map[0][0].size(); ++k)
      {
        pcl::PointXYZRGBA tmp;
        tmp.x = i * voxel_unit + voxel_unit / 2;
        tmp.y = j * voxel_unit + voxel_unit / 2;
        tmp.z = k * voxel_unit + voxel_unit / 2;
        
        //0 - unmarked; 
        //1 - empty; 
        //2 - occupied; 
        //3 - occluded; 
        //4 - occplane.
        if(voxel_map[i][j][k] == 1)
        {
          tmp.b = 255;
          tmp.g = 0;
          tmp.r = 0;
        }
        
        if(voxel_map[i][j][k] == 2)
        {
          tmp.b = 0;
          tmp.g = 255;
          tmp.r = 0;
        }
        
        if(voxel_map[i][j][k] == 3)
        {
          tmp.b = 0;
          tmp.g = 0;
          tmp.r = 255;
        }
        
        if(voxel_map[i][j][k] == 4)
        {
          tmp.b = 255;
          tmp.g = 255;
          tmp.r = 255;
        }
        
        ptr_voxel_map->points.push_back(tmp);
      }
  
  ptr_voxel_map->width = ptr_voxel_map->points.size();
  
  #ifdef DEBUG_VOXEL_MAP
  pcd_viewer_voxel_map->addCoordinateSystem(0.1);
  pcd_viewer_voxel_map->initCameraParameters();
  pcd_viewer_voxel_map->removePointCloud("voxel map"); 
  pcd_viewer_voxel_map->addPointCloud(ptr_voxel_map, "voxel map");
  std::cout<<"Displayed the voxel map - Press q to continue"<<std::endl;
  pcd_viewer_voxel_map->spin();
  #endif
}

/***********************************************************************
 * Check if the voxel is occluded in the current voxel map according to 
 * all camera origins;
 * @param voxel_map: the voxel map; notice that it is in the first quadrant
 *   of the CS; the coordinates and the indices have strick corrsponding relations.
 * @param index_1st: the x index of the voxel
 * @param index_2nd: the y index of the voxel
 * @param index_3rd: the z index of the voxel
 * @param camera_origins_all: all the current camera origins
 * @return : if the voxel is occluded or not
 **********************************************************************/
static bool check_if_occluded_for_voxel(const std::vector<std::vector<std::vector<int> > > &voxel_map,
                                        const int &index_1st, const int &index_2nd, const int &index_3rd,
                                        const std::vector<Eigen::Vector3f> &camera_origins_all,
                                        const float &voxel_unit)
{
  float voxel_x = index_1st * voxel_unit + voxel_unit / 2;
  float voxel_y = index_2nd * voxel_unit + voxel_unit / 2;
  float voxel_z = index_3rd * voxel_unit + voxel_unit / 2;
  
  //check if the voxel can be seen by one of the camera origins by ray tracing.
  for(int i = 0; i < camera_origins_all.size(); ++i)
  {
    //obtain the ray tracing direction from the voxel to the camera origin
    Eigen::Vector3f ray_voxel_to_origin;
    ray_voxel_to_origin(0) = camera_origins_all[i](0) - voxel_x;
    ray_voxel_to_origin(1) = camera_origins_all[i](1) - voxel_y;
    ray_voxel_to_origin(2) = camera_origins_all[i](2) - voxel_z;
    
    float dist = sqrt(ray_voxel_to_origin(0) * ray_voxel_to_origin(0) + ray_voxel_to_origin(1) * ray_voxel_to_origin(1) + ray_voxel_to_origin(2) * ray_voxel_to_origin(2));
    
    int num_step = (int)(dist/voxel_unit);
    
    for(int index_step = 1; index_step < num_step; ++index_step)
    {
      Eigen::Vector3f voxel_in_ray;
      voxel_in_ray(0) = voxel_x + voxel_unit * index_step * (ray_voxel_to_origin(0) / dist);
      voxel_in_ray(1) = voxel_y + voxel_unit * index_step * (ray_voxel_to_origin(1) / dist);
      voxel_in_ray(2) = voxel_z + voxel_unit * index_step * (ray_voxel_to_origin(2) / dist);
      
      int x_index = (int)(voxel_in_ray(0)/voxel_unit);
      int y_index = (int)(voxel_in_ray(1)/voxel_unit);
      int z_index = (int)(voxel_in_ray(2)/voxel_unit);
      
      if(x_index == index_1st && y_index == index_2nd && z_index == index_3rd)
        continue;
      
      //traced outside the voxel map to the camera origin successfully
      if(x_index < 1 || y_index < 1 || z_index < 1 || x_index > (voxel_map.size()-2) || y_index > (voxel_map[0].size()-2) || z_index > (voxel_map[0][0].size()-2))
        return false;
      
      //0 - unmarked; 
      //1 - empty; 
      //2 - occupied; 
      //3 - occluded; 
      //4 - occplane.
      //a voxel is not occluded only if there only exists empty voxels on the ray between the voxel and the camera origin
      if(voxel_map[x_index][y_index][z_index] == 2 || voxel_map[x_index][y_index][z_index] == 3 || voxel_map[x_index][y_index][z_index] == 4)
        break;
    }
    
  }
  
  return true;
}
                                        
#define DEBUG_POSE_PLANNING
#ifdef DEBUG_POSE_PLANNING
pcl::visualization::PCLVisualizer *pcd_viewer_pose_planning = new pcl::visualization::PCLVisualizer("Pose Planning");
#endif
/***********************************************************************
 * Evaluate the area factor using other five sides as the new bottoms, 
 * considering both the overlap for registration and the new unseen part for 
 * completing the object model.
 * @param path_current_root: the root working directory
 * @param index_rotation: the index of the 360 degrees rotation procedure to read all the data
 * @param transform_to_robot_cs: the transformation from the camera CS to the robot CS
 * @return : the area factor evaluation for each side
 **********************************************************************/
std::vector<float> evaluate_overlap_and_new_for_using_other_five_sides_as_bottom(const std::string &path_current_root,
                                                                                 const int &index_rotation,
                                                                                 const Eigen::Matrix4f &transform_to_robot_cs)
{
  const float voxel_unit = 0.003f;
  
  //+x side: grids[0][][]
  //-x side: grids[1][][]
  //+y side: grids[2][][]
  //-y side: grids[3][][]
  //+z side: grids[4][][]
  std::vector<float> area_evaluation(5, 0.0f);
  
  printf("*************Start Area Factor Evaluation****************\n");
  
  /*********************************************************************
   * read the begin and end indices first
   ********************************************************************/
  char path_model_indices_backup[1024];
  sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
  std::ifstream cin_model_indices;
  cin_model_indices.open(path_model_indices_backup);
  if (!cin_model_indices.is_open())
  {
    std::cout<<"Error: fail to open the text file to read model indices information!"<<std::endl;
    return area_evaluation;
  }
    
  int num_rotations;
  std::vector<int> indices_begin, indices_end;
  indices_begin.clear();
  indices_end.clear();
    
  cin_model_indices>>num_rotations;
  indices_begin.resize(num_rotations);
  indices_end.resize(num_rotations);
    
  int tmp;
  for (int i = 0; i < num_rotations; ++i)
    cin_model_indices>>tmp>>indices_begin[i]>>tmp>>indices_end[i];
    
  cin_model_indices.close();
    
  printf("Rotation %03d: View %03d - View %03d\n", index_rotation, indices_begin[index_rotation-1], indices_end[index_rotation-1]);
  
  /*********************************************************************
   * read the current model after the current 360 degrees rotation procedure;
   * the model is always in the CS of the last view.
   ********************************************************************/
  char path_model_after_rotations[1024];
  sprintf(path_model_after_rotations, "%s/object_model_after_360_rotation_%03d.pcd", path_current_root.c_str(), index_rotation);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_after_rotations(new pcl::PointCloud<pcl::PointXYZRGBA>());
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_model_after_rotations, *ptr_model_after_rotations);
  
  //transform the object model from the camera coordinate system to the robot coordinate system
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
  pcl::transformPointCloud(*ptr_model_after_rotations, *ptr_model_robot_cs, transform_to_robot_cs);

  #ifdef DEBUG_POSE_PLANNING
  pcd_viewer_pose_planning->addCoordinateSystem(0.1);
  pcd_viewer_pose_planning->initCameraParameters();
  pcd_viewer_pose_planning->removePointCloud("point cloud"); 
  pcd_viewer_pose_planning->addPointCloud(ptr_model_robot_cs, "point cloud");
  std::cout<<"Displayed the model in robot CS - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  //compute a bounding box for the point cloud of the modeling object in robot CS
  Eigen::Vector3f bbox_centroid;
  Eigen::Matrix3f bbox_directions;
  float length_1st, length_2nd, length_3rd;
  obtain_oriented_bounding_box_on_XY_plane(ptr_model_robot_cs, bbox_centroid, bbox_directions,
                                           length_1st, length_2nd, length_3rd);

  //transform the object model to the bounding box coordinate system
  Eigen::Matrix4f transform_to_bb_cs(Eigen::Matrix4f::Identity());
  //so that we can compute the voxel map more conveniently
  Eigen::Vector3f translate_voxel_map(length_1st/2.0f + voxel_unit, length_2nd/2.0f + voxel_unit, length_3rd/2.0f + voxel_unit);
  transform_to_bb_cs.block<3, 3>(0, 0) = bbox_directions.transpose();
  transform_to_bb_cs.block<3, 1>(0, 3) = -1.f * (transform_to_bb_cs.block<3, 3>(0, 0) * bbox_centroid) + translate_voxel_map;
        
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_bb_cs(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_model_robot_cs, *ptr_model_bb_cs, transform_to_bb_cs);
  
  #ifdef DEBUG_POSE_PLANNING
  pcd_viewer_pose_planning->removePointCloud("point cloud"); 
  pcd_viewer_pose_planning->addPointCloud(ptr_model_bb_cs, "point cloud");
  std::cout<<"Displayed the model in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif

  /*********************************************************************
   * compute all the coordinates of the camera origins from 
   * all the views from all previous 360 degree rotation procedures in bb CS
   ********************************************************************/
  std::vector<Eigen::Matrix4f> transform_model_to_final;
  transform_model_to_final.clear();
  transform_model_to_final.resize(index_rotation);
  
  //compute the transformation from each model to the final CS first
  transform_model_to_final[index_rotation-1] = Eigen::Matrix4f::Identity();
  for(int index_model = index_rotation-2; index_model >= 0; --index_model)
  {
    std::ifstream cin_transform;
    char path_transform[1024];
    sprintf(path_transform, "%s/transformation_from_model_%03d_to_model_%03d.txt", path_current_root.c_str(), index_model+1, index_model+2);
    cin_transform.open(path_transform);
    if (!cin_transform.is_open())
    {
      std::cout<<"Error: fail to open the file to obtain the transformation from model to model!"<<std::endl;
      return area_evaluation;
    }
    Eigen::Matrix4f transform_neighbor_model;
    for(int i = 0; i < 4; ++i)
      for(int j = 0; j < 4; ++j)
        cin_transform>>transform_neighbor_model(i, j);
    cin_transform.close();
    transform_model_to_final[index_model] = transform_model_to_final[index_model+1] * transform_neighbor_model;
  }
  
  //compute the transformation from each view of the model to the final CS
  std::vector<Eigen::Vector3f> camera_origins_all;
  camera_origins_all.clear();
  for(int index_model = 0; index_model <= index_rotation - 1; ++index_model)
  {
    for(int index_view = indices_begin[index_model]; index_view <= indices_end[index_model]; ++index_view)
    {
      Eigen::Matrix4f transform_to_model_cs;
      //transformation from the view_index view CS to the model CS in the final view
      std::ifstream cin_transform;
      char path_transform[1024];
      sprintf(path_transform, "%s/transformation_to_model_for_view_%03d.txt", path_current_root.c_str(), index_view);
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information for each view!"<<std::endl;
        return area_evaluation;
      }
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform_to_model_cs(i, j);
      cin_transform.close();
      
      //the origin camera 
      Eigen::Vector4f camera_origin_view_cs(0.0f, 0.0f, 0.0f, 1.0f);
      Eigen::Vector4f camera_origin_bb_cs = transform_to_bb_cs * transform_to_robot_cs * transform_model_to_final[index_model] * transform_to_model_cs * camera_origin_view_cs;
      Eigen::Vector3f camera_origin(camera_origin_bb_cs(0), camera_origin_bb_cs(1), camera_origin_bb_cs(2));
      camera_origins_all.push_back(camera_origin);
      
      #ifdef DEBUG_POSE_PLANNING
      pcl::PointXYZ origin_show(camera_origin(0), camera_origin(1), camera_origin(2));
      char origin_name[20];
      sprintf(origin_name, "origin all %03d", index_view);
      pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 1.0, 0.0, 0.0, origin_name);
      #endif
    }
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  std::cout<<camera_origins_all.size()<<"  Camera Origin List: "<<std::endl;
  for(int i = 0; i < camera_origins_all.size(); ++i)
    printf("%6f %6f %6f\n", camera_origins_all[i](0), camera_origins_all[i](1), camera_origins_all[i](2));

  /*********************************************************************
   * create the Voxel Map
   ********************************************************************/
  printf("*****************Create the Voxel Map***************** \n");
  
  //the size of the voxel map;
  //2 empty grids on the boundary;
  //build the voxel map in the corner of the origin.
  int num_grids_1st, num_grids_2nd, num_grids_3rd;
  num_grids_1st = (int)(length_1st/voxel_unit)+3;
  num_grids_2nd = (int)(length_2nd/voxel_unit)+3;
  num_grids_3rd = (int)(length_3rd/voxel_unit)+3;
  printf("Voxel Map Size: %f  %f  %f\n", length_1st, length_2nd, length_3rd);
  printf("Voxel Map Grid Size: %05d  %05d  %05d\n", num_grids_1st, num_grids_2nd, num_grids_3rd);
  
  std::vector<std::vector<std::vector<int> > > voxel_map(num_grids_1st, std::vector<std::vector<int> >(num_grids_2nd, std::vector<int>(num_grids_3rd)));
  
  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  //Initialize the voxel map
  std::cout<<"**********Initialize the voxel map**********"<<std::endl;
  for(int i = 0; i < num_grids_1st; ++i)
    for(int j = 0; j < num_grids_2nd; ++j)
      for(int k = 0; k < num_grids_3rd; ++k)
        if( i == 0 || i == num_grids_1st-1 || j == 0 || j == num_grids_2nd-1 || k == 0 || k == num_grids_3rd-1)
          voxel_map[i][j][k] = 1;
        else
          voxel_map[i][j][k] = 0;
  print_voxel_map_information(voxel_map);
  #ifdef DEBUG_VOXEL_MAP
  show_voxel_map_information(voxel_map, voxel_unit);
  #endif
  
  //Fill the occupied voxels based on the object model point cloud
  std::cout<<"**********Label occupied voxels**********"<<std::endl;
  for(int i = 0; i < ptr_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_model_bb_cs->points[i].z))
      continue;
    
    int index_1st = (int)(ptr_model_bb_cs->points[i].x/voxel_unit);
    int index_2nd = (int)(ptr_model_bb_cs->points[i].y/voxel_unit);
    int index_3rd = (int)(ptr_model_bb_cs->points[i].z/voxel_unit);
    
    voxel_map[index_1st][index_2nd][index_3rd] = 2;
  }
  print_voxel_map_information(voxel_map);
  #ifdef DEBUG_VOXEL_MAP
  show_voxel_map_information(voxel_map, voxel_unit);
  #endif
  
  //Label all the empty, occluded voxels;
  //empty voxel: a Non-occupied voxel which can be seen from a camera origin;
  //occluded voxel: a Non-occupied voxel which can not be seen from any camera origin;
  //The object model point cloud is always in the cube cage surrounded by empty voxels.
  std::cout<<"**********Label empty and occluded voxels**********"<<std::endl;
  std::cout<<"**********Please wait >>>>>>>>>>>>>>>>>>>**********"<<std::endl;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if(voxel_map[i][j][k] == 0)
        {
          if(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_all, voxel_unit))
            voxel_map[i][j][k] = 3;
          else
            voxel_map[i][j][k] = 1;
        }
  print_voxel_map_information(voxel_map);
  #ifdef DEBUG_VOXEL_MAP
  show_voxel_map_information(voxel_map, voxel_unit);
  #endif
  
  //Label all the occplane voxels
  //occplane voxel: an occluded voxel which has at least one neighbor which is empty
  std::cout<<"**********Label occplane voxels**********"<<std::endl;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if(voxel_map[i][j][k] == 3)
        {
          if(voxel_map[i-1][j][k] == 1 || voxel_map[i+1][j][k] == 1 ||
             voxel_map[i][j-1][k] == 1 || voxel_map[i][j+1][k] == 1 ||
             voxel_map[i][j][k-1] == 1 || voxel_map[i][j][k+1] == 1)
            voxel_map[i][j][k] = 4;  
        }
  print_voxel_map_information(voxel_map);
  #ifdef DEBUG_VOXEL_MAP
  show_voxel_map_information(voxel_map, voxel_unit);
  #endif
  
  /*********************************************************************
   * Evaluate the area factor using other five sides as the new bottoms;
   * use the camera origins in the current 360 degrees rotation procedure.
   ********************************************************************/
  
  //read all the camera origins from the current 360 degrees rotation procedure
  std::vector<Eigen::Vector3f> camera_origins_cur;
  camera_origins_cur.clear();
  
  for(int index_view = indices_begin[index_rotation-1]; index_view <= indices_end[index_rotation-1]; ++index_view)
  {
    Eigen::Matrix4f transform_to_model_cs;
    //transformation from the view_index view CS to the model CS in the final view
    std::ifstream cin_transform;
    char path_transform[1024];
    sprintf(path_transform, "%s/transformation_to_model_for_view_%03d.txt", path_current_root.c_str(), index_view);
    cin_transform.open(path_transform);
    if (!cin_transform.is_open())
    {
      std::cout<<"Error: fail to open the file to obtain the transform information from the begin view!"<<std::endl;
      return area_evaluation;
    }
    for(int i = 0; i < 4; ++i)
      for(int j = 0; j < 4; ++j)
        cin_transform>>transform_to_model_cs(i, j);
    cin_transform.close();
    
    //the origin camera 
    Eigen::Vector4f camera_origin_view_cs(0.0f, 0.0f, 0.0f, 1.0f);
    Eigen::Vector4f camera_origin_bb_cs = transform_to_bb_cs * transform_to_robot_cs * transform_to_model_cs * camera_origin_view_cs;
    Eigen::Vector3f camera_origin(camera_origin_bb_cs(0), camera_origin_bb_cs(1), camera_origin_bb_cs(2));
    camera_origins_cur.push_back(camera_origin);
    
    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origin(0), camera_origin(1), camera_origin(2));
    char origin_name[20];
    sprintf(origin_name, "origin %03d", index_view);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 1.0, 1.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of the current rotation procedure in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  std::cout<<camera_origins_cur.size()<<"  Camera Origin List In the Current Rotation Procedure: "<<std::endl;
  for(int i = 0; i < camera_origins_cur.size(); ++i)
    printf("%6f %6f %6f\n", camera_origins_cur[i](0), camera_origins_cur[i](1), camera_origins_cur[i](2));
  
  //evaluate the area factor for each side
  //+x side: grids[0][][]
  //-x side: grids[1][][]
  //+y side: grids[2][][]
  //-y side: grids[3][][]
  //+z side: grids[4][][]
  
  //Notice that for the convenience of computing the voxel map, the origin 
  // of the current bounding box CS is not in the center of the bounding box;
  const float MY_PI = 3.14159f;
  std::vector<int> num_occupied_plan(5, 0);
  std::vector<int> num_occplane_plan(5, 0);

  std::cout<<"**********Evaluate the +x side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_x_plus;
  rotation_x_plus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(-0.5*MY_PI, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_x_plus;
  camera_origins_x_plus.clear();
  camera_origins_x_plus.resize(camera_origins_cur.size());
  
  for(int i = 0; i < camera_origins_cur.size(); ++i)
  {
    camera_origins_x_plus[i] = rotation_x_plus * (camera_origins_cur[i] - translate_voxel_map) + translate_voxel_map;

    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origins_x_plus[i](0), camera_origins_x_plus[i](1), camera_origins_x_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin x_plus %03d", i);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 1.0, 0.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of +x side - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif

  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_occupied_x_plus = 0;
  int num_occplane_x_plus = 0;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if((voxel_map[i][j][k] == 2 || voxel_map[i][j][k] == 4) && !(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_x_plus, voxel_unit)))
        {
          if(voxel_map[i][j][k] == 2)
            ++num_occupied_plan[0];
          else if(voxel_map[i][j][k] == 4)
            ++num_occplane_plan[0];
        }
  std::cout<<"num_occupied_x_plus: "<<num_occupied_plan[0]<<std::endl;
  std::cout<<"num_occplane_x_plus: "<<num_occplane_plan[0]<<std::endl;
  
  

  std::cout<<"**********Evaluate the -x side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_x_minus;
  rotation_x_minus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                   * Eigen::AngleAxisf(0.5*MY_PI, Eigen::Vector3f::UnitY()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_x_minus;
  camera_origins_x_minus.clear();
  camera_origins_x_minus.resize(camera_origins_cur.size());
  
  for(int i = 0; i < camera_origins_cur.size(); ++i)
  {
    camera_origins_x_minus[i] = rotation_x_minus * (camera_origins_cur[i] - translate_voxel_map) + translate_voxel_map;

    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origins_x_minus[i](0), camera_origins_x_minus[i](1), camera_origins_x_minus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin x_minus %03d", i);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 0.0, 1.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of -x side - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_occupied_x_minus = 0;
  int num_occplane_x_minus = 0;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if((voxel_map[i][j][k] == 2 || voxel_map[i][j][k] == 4) && !(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_x_minus, voxel_unit)))
        {
          if(voxel_map[i][j][k] == 2)
            ++num_occupied_plan[1];
          else if(voxel_map[i][j][k] == 4)
            ++num_occplane_plan[1];
        }
  std::cout<<"num_occupied_x_minus: "<<num_occupied_plan[1]<<std::endl;
  std::cout<<"num_occplane_x_minus: "<<num_occplane_plan[1]<<std::endl;


  
  std::cout<<"**********Evaluate the +y side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_y_plus;
  rotation_y_plus = Eigen::AngleAxisf(0.5*MY_PI, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_y_plus;
  camera_origins_y_plus.clear();
  camera_origins_y_plus.resize(camera_origins_cur.size());
  
  for(int i = 0; i < camera_origins_cur.size(); ++i)
  {
    camera_origins_y_plus[i] = rotation_y_plus * (camera_origins_cur[i] - translate_voxel_map) + translate_voxel_map;

    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origins_y_plus[i](0), camera_origins_y_plus[i](1), camera_origins_y_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin y_plus %03d", i);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 0.0, 0.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of +y side - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif

  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_occupied_y_plus = 0;
  int num_occplane_y_plus = 0;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if((voxel_map[i][j][k] == 2 || voxel_map[i][j][k] == 4) && !(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_y_plus, voxel_unit)))
        {
          if(voxel_map[i][j][k] == 2)
            ++num_occupied_plan[2];
          else if(voxel_map[i][j][k] == 4)
            ++num_occplane_plan[2];
        }
  std::cout<<"num_occupied_y_plus: "<<num_occupied_plan[2]<<std::endl;
  std::cout<<"num_occplane_y_plus: "<<num_occplane_plan[2]<<std::endl;
  
  
  
  std::cout<<"**********Evaluate the -y side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_y_minus;
  rotation_y_minus = Eigen::AngleAxisf(-0.5*MY_PI, Eigen::Vector3f::UnitX()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_y_minus;
  camera_origins_y_minus.clear();
  camera_origins_y_minus.resize(camera_origins_cur.size());
  
  for(int i = 0; i < camera_origins_cur.size(); ++i)
  {
    camera_origins_y_minus[i] = rotation_y_minus * (camera_origins_cur[i] - translate_voxel_map) + translate_voxel_map;

    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origins_y_minus[i](0), camera_origins_y_minus[i](1), camera_origins_y_minus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin y_minus %03d", i);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 1.0, 0.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of -y side - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_occupied_y_minus = 0;
  int num_occplane_y_minus = 0;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if((voxel_map[i][j][k] == 2 || voxel_map[i][j][k] == 4) && !(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_y_minus, voxel_unit)))
        {
          if(voxel_map[i][j][k] == 2)
            ++num_occupied_plan[3];
          else if(voxel_map[i][j][k] == 4)
            ++num_occplane_plan[3];
        }
  std::cout<<"num_occupied_y_minus: "<<num_occupied_plan[3]<<std::endl;
  std::cout<<"num_occplane_y_minus: "<<num_occplane_plan[3]<<std::endl;
  
  
  std::cout<<"**********Evaluate the +z side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_z_plus;
  rotation_z_plus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(MY_PI, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_z_plus;
  camera_origins_z_plus.clear();
  camera_origins_z_plus.resize(camera_origins_cur.size());
  
  for(int i = 0; i < camera_origins_cur.size(); ++i)
  {
    camera_origins_z_plus[i] = rotation_z_plus * (camera_origins_cur[i] - translate_voxel_map) + translate_voxel_map;

    #ifdef DEBUG_POSE_PLANNING
    pcl::PointXYZ origin_show(camera_origins_z_plus[i](0), camera_origins_z_plus[i](1), camera_origins_z_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin z_plus %03d", i);
    pcd_viewer_pose_planning->addSphere(origin_show, 0.05, 1.0, 1.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_POSE_PLANNING
  std::cout<<"Displayed the camera origins of +z side - Press q to continue"<<std::endl;
  pcd_viewer_pose_planning->spin();
  #endif
  
  //0 - unmarked; 
  //1 - empty; 
  //2 - occupied; 
  //3 - occluded; 
  //4 - occplane.
  int num_occupied_z_plus = 0;
  int num_occplane_z_plus = 0;
  for(int i = 1; i < num_grids_1st-1; ++i)
    for(int j = 1; j < num_grids_2nd-1; ++j)
      for(int k = 1; k < num_grids_3rd-1; ++k)
        if((voxel_map[i][j][k] == 2 || voxel_map[i][j][k] == 4) && !(check_if_occluded_for_voxel(voxel_map, i, j, k, camera_origins_z_plus, voxel_unit)))
        {
          if(voxel_map[i][j][k] == 2)
            ++num_occupied_plan[4];
          else if(voxel_map[i][j][k] == 4)
            ++num_occplane_plan[4];
        }
  std::cout<<"num_occupied_z_plus: "<<num_occupied_plan[4]<<std::endl;
  std::cout<<"num_occplane_z_plus: "<<num_occplane_plan[4]<<std::endl;
  
  int num_seen_voxels_max = 0;
  for(int i = 0; i < num_occupied_plan.size(); ++i)
    if(num_occupied_plan[i] + num_occplane_plan[i] > num_seen_voxels_max)
      num_seen_voxels_max = num_occupied_plan[i] + num_occplane_plan[i];
  std::cout<<"Maxinum number of seen voxels: "<<num_seen_voxels_max<<std::endl;
  
  const float ratio_occupied_required = 0.6f;
  const float ratio_occplane_required = 0.4f;
  
  for(int i = 0; i < area_evaluation.size(); ++i)
  {
    float ratio_occupied = num_occupied_plan[i] * 1.0f / (num_occupied_plan[i] + num_occplane_plan[i]);
    float ratio_occplane = num_occplane_plan[i] * 1.0f / (num_occupied_plan[i] + num_occplane_plan[i]);
    
    float score_occupied;
    if(ratio_occupied < ratio_occupied_required)
    {
      score_occupied = -2.0f * pow(ratio_occupied / ratio_occupied_required, 3) 
                     + 3.0f * pow(ratio_occupied / ratio_occupied_required, 2);
    }
    else
    {
      score_occupied = -2.0f * pow(ratio_occupied, 3) / pow((ratio_occupied_required-1.0f), 3)
                     + 3 * (ratio_occupied_required + 1) * pow(ratio_occupied, 2) / pow((ratio_occupied_required-1.0f), 3)
                     - 6 * ratio_occupied_required * ratio_occupied / pow((ratio_occupied_required-1.0f), 3)
                     + (3 * ratio_occupied_required - 1) / pow((ratio_occupied_required-1.0f), 3);
    }
    
    float score_occplane;
    if(ratio_occplane < ratio_occplane_required)
    {
      score_occplane = -2.0f * pow(ratio_occplane/ratio_occplane_required, 3)
                     + 3.0f * pow(ratio_occplane/ratio_occplane_required, 2);
    }
    else
    {
      score_occplane = -2.0f * pow(ratio_occplane, 3) / pow((ratio_occplane_required-1.0f), 3)
                     + 3 * (ratio_occplane_required + 1) * pow(ratio_occplane, 2) / pow((ratio_occplane_required-1.0f), 3)
                     - 6 * ratio_occplane_required * ratio_occplane / pow((ratio_occplane_required-1.0f), 3)
                     + (3 * ratio_occplane_required - 1) / pow((ratio_occplane_required-1.0f), 3);
    }
                
    area_evaluation[i] = (score_occupied + score_occplane) * (num_occupied_plan[i] + num_occplane_plan[i]) * 1.0f / num_seen_voxels_max;
    
    std::cout<<"score_occupied - score_occplane - area_evaluation : "<<score_occupied<<"  "<<score_occplane<<"  "<<area_evaluation[i]<<std::endl;
  }
  
  #ifdef DEBUG_POSE_PLANNING
  for(int i = 0; i < area_evaluation.size(); ++i)
    printf("%10f ", area_evaluation[i]);
  printf("\n");
  #endif
  
  return area_evaluation;
}

//#define DEBUG_MODEL_QUALITY
#ifdef DEBUG_MODEL_QUALITY
pcl::visualization::PCLVisualizer *pcd_viewer_model_quality = new pcl::visualization::PCLVisualizer("Model Quality");
#endif
/***********************************************************************
 * Evaluate the quality factor using other five sides as the new bottoms;
 * check more details from the paper "Volumetirc Next-best-view Planning 
 * for 3D Object Reconstruction with Positioning Error"
 * @param path_current_root: the root working directory
 * @param index_rotation: the index of the 360 degrees rotation procedure to read all the data
 * @param transform_to_robot_cs: the transformation from the camera CS to the robot CS
 * @return : the quality factor evaluation for each side
 **********************************************************************/
std::vector<float> evaluate_quality_of_model_for_using_other_five_sides_as_bottom(const std::string &path_current_root,
                                                                                  const int &index_rotation,
                                                                                  const Eigen::Matrix4f &transform_to_robot_cs)
{
  //+x side: grids[0][][]
  //-x side: grids[1][][]
  //+y side: grids[2][][]
  //-y side: grids[3][][]
  //+z side: grids[4][][]
  std::vector<float> quality_evaluation(5, 0.0f);
  
  printf("*************Start Quality Factor Evaluation****************\n");
  
  /*********************************************************************
   * read the begin and end indices first
   ********************************************************************/
  char path_model_indices_backup[1024];
  sprintf(path_model_indices_backup, "%s/model_indices.txt", path_current_root.c_str());
    
  std::ifstream cin_model_indices;
  cin_model_indices.open(path_model_indices_backup);
  if (!cin_model_indices.is_open())
  {
    std::cout<<"Error: fail to open the text file to read model indices information!"<<std::endl;
    return quality_evaluation;
  }
    
  int num_rotations;
  std::vector<int> indices_begin, indices_end;
  indices_begin.clear();
  indices_end.clear();
    
  cin_model_indices>>num_rotations;
  indices_begin.resize(num_rotations);
  indices_end.resize(num_rotations);
    
  int tmp;
  for (int i = 0; i < num_rotations; ++i)
    cin_model_indices>>tmp>>indices_begin[i]>>tmp>>indices_end[i];
    
  cin_model_indices.close();
    
  printf("Rotation %03d: View %03d - View %03d\n", index_rotation, indices_begin[index_rotation-1], indices_end[index_rotation-1]);
  
  /*********************************************************************
   * read the current model after the current 360 degrees rotation procedure;
   * the model is always in the CS of the last view.
   ********************************************************************/
  char path_model_after_rotations[1024];
  sprintf(path_model_after_rotations, "%s/object_model_after_360_rotation_%03d.pcd", path_current_root.c_str(), index_rotation);
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_after_rotations(new pcl::PointCloud<pcl::PointXYZRGBA>());
  pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_model_after_rotations, *ptr_model_after_rotations);
  
  //transform the object model from the camera coordinate system to the robot coordinate system
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
  pcl::transformPointCloud(*ptr_model_after_rotations, *ptr_model_robot_cs, transform_to_robot_cs);

  #ifdef DEBUG_MODEL_QUALITY
  pcd_viewer_model_quality->addCoordinateSystem(0.3);
  pcd_viewer_model_quality->initCameraParameters();
  pcd_viewer_model_quality->removePointCloud("point cloud"); 
  pcd_viewer_model_quality->addPointCloud(ptr_model_robot_cs, "point cloud");
  std::cout<<"Displayed the model in robot CS - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  //compute a bounding box for the point cloud of the modeling object in robot CS
  Eigen::Vector3f bbox_centroid;
  Eigen::Matrix3f bbox_directions;
  float length_1st, length_2nd, length_3rd;
  obtain_oriented_bounding_box_on_XY_plane(ptr_model_robot_cs, bbox_centroid, bbox_directions,
                                           length_1st, length_2nd, length_3rd);

  //transform the object model to the bounding box coordinate system
  Eigen::Matrix4f transform_to_bb_cs(Eigen::Matrix4f::Identity());
  transform_to_bb_cs.block<3, 3>(0, 0) = bbox_directions.transpose();
  transform_to_bb_cs.block<3, 1>(0, 3) = -1.f * (transform_to_bb_cs.block<3, 3>(0, 0) * bbox_centroid);
        
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_bb_cs(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::transformPointCloud(*ptr_model_robot_cs, *ptr_model_bb_cs, transform_to_bb_cs);
  
  #ifdef DEBUG_MODEL_QUALITY
  pcd_viewer_model_quality->removePointCloud("point cloud"); 
  pcd_viewer_model_quality->addPointCloud(ptr_model_bb_cs, "point cloud");
  std::cout<<"Displayed the model in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif

  //downsample the point cloud so that all the points are evenly distributed in the space
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_filtered_model_bb_cs(new pcl::PointCloud<pcl::PointXYZRGBA>);
  // Create the filtering object
  pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
  sor.setLeafSize(0.001f, 0.001f, 0.001f);
  sor.setInputCloud(ptr_model_bb_cs);
  sor.filter(*ptr_filtered_model_bb_cs);
  std::cout<<"Model Size Before Filtering: "<<ptr_model_bb_cs->points.size()<<std::endl;
  std::cout<<"Model Size After Filtering: "<<ptr_filtered_model_bb_cs->points.size()<<std::endl;

  #ifdef DEBUG_MODEL_QUALITY
  pcd_viewer_model_quality->removePointCloud("point cloud"); 
  pcd_viewer_model_quality->addPointCloud(ptr_filtered_model_bb_cs, "point cloud");
  std::cout<<"Displayed the filtered model in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif

  /*****************************************************************************
   * Compute the normals for all points
   ****************************************************************************/
  std::cout<<"In process of computing the normals, please wait......."<<std::endl;
  pcl::NormalEstimation<pcl::PointXYZRGBA, pcl::Normal> norm_est;
  pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr ptr_search_tree(new pcl::search::KdTree<pcl::PointXYZRGBA> ());
  norm_est.setSearchMethod(ptr_search_tree);
  norm_est.setRadiusSearch(0.01);   //0.01m = 10mm
  
  //the normals of the target point cloud are fixed
  pcl::PointCloud<pcl::Normal>::Ptr ptr_model_normals(new pcl::PointCloud<pcl::Normal> ());  
  norm_est.setInputCloud(ptr_filtered_model_bb_cs);
  norm_est.compute(*ptr_model_normals);
  
  //the center point for the current object model is the origin
  pcl::PointXYZ center_point(0.0f, 0.0f, 0.0f);
  std::cout<<"Center Point: "<<center_point.x<<"  "<<center_point.y<<"  "<<center_point.z<<std::endl;
  
  //adjust the normal directions so that the point normals always point to the outside of the object model
  //the current strategy only works on convex objects or the convex 
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
	    
    float dot_sign;
    dot_sign = (center_point.x - ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x + 
               (center_point.y - ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y +
               (center_point.z - ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z;
    if(dot_sign > 0.0f)
    {
      ptr_model_normals->points[i].normal_x = - ptr_model_normals->points[i].normal_x;
      ptr_model_normals->points[i].normal_y = - ptr_model_normals->points[i].normal_y;
      ptr_model_normals->points[i].normal_z = - ptr_model_normals->points[i].normal_z;
    }
  }

  #ifdef DEBUG_MODEL_QUALITY
  pcd_viewer_model_quality->addPointCloudNormals<pcl::PointXYZRGBA, pcl::Normal>(ptr_filtered_model_bb_cs, ptr_model_normals, 10, 0.05, "normals");
  std::cout<<"Displayed the normals in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  /*********************************************************************
   * Evaluate the quality factor using other five sides as the new bottoms;
   * use the camera origins in the current 360 degrees rotation procedure.
   ********************************************************************/
  //read all the camera origins from the current 360 degrees rotation procedure
  std::vector<Eigen::Vector3f> camera_origins;
  camera_origins.clear();
  
  for(int index_view = indices_begin[index_rotation-1]; index_view <= indices_end[index_rotation-1]; ++index_view)
  {
    Eigen::Matrix4f transform_to_model_cs;
    //transformation from the view_index view CS to the model CS in the final view
    std::ifstream cin_transform;
    char path_transform[1024];
    sprintf(path_transform, "%s/transformation_to_model_for_view_%03d.txt", path_current_root.c_str(), index_view);
    cin_transform.open(path_transform);
    if (!cin_transform.is_open())
    {
      std::cout<<"Error: fail to open the file to obtain the transform information from the begin view!"<<std::endl;
      return quality_evaluation;
    }
    for(int i = 0; i < 4; ++i)
      for(int j = 0; j < 4; ++j)
        cin_transform>>transform_to_model_cs(i, j);
    cin_transform.close();
    
    //the origin camera 
    Eigen::Vector4f camera_origin_view_cs(0.0f, 0.0f, 0.0f, 1.0f);
    Eigen::Vector4f camera_origin_bb_cs = transform_to_bb_cs * transform_to_robot_cs * transform_to_model_cs * camera_origin_view_cs;
    Eigen::Vector3f camera_origin(camera_origin_bb_cs(0), camera_origin_bb_cs(1), camera_origin_bb_cs(2));
    camera_origins.push_back(camera_origin);
    
    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origin(0), camera_origin(1), camera_origin(2));
    char origin_name[20];
    sprintf(origin_name, "origin %03d", index_view);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 1.0, 1.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of the current rotation procedure in bb CS - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  
  /*****************************************************************************
   * Evaluate the quality of the model by the angles of the normals
   ****************************************************************************/
  const float MY_PI = 3.14159f;
  int num_cosin;
  
  std::cout<<"**********Evaluate the +x side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_x_plus;
  rotation_x_plus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(-0.5*MY_PI, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_x_plus;
  camera_origins_x_plus.clear();
  camera_origins_x_plus.resize(camera_origins.size());
  
  for(int i = 0; i < camera_origins.size(); ++i)
  {
    camera_origins_x_plus[i] = rotation_x_plus * camera_origins[i];

    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origins_x_plus[i](0), camera_origins_x_plus[i](1), camera_origins_x_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin x_plus %03d", i);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 1.0, 0.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of +x side - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  num_cosin = 0;
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
    
    float cosin_max = -1.0f;
    for(int k = 0; k < camera_origins.size(); ++k)
    {
      float length = sqrt(pow(camera_origins_x_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x, 2.0f) 
                         +pow(camera_origins_x_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y, 2.0f)
                         +pow(camera_origins_x_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z, 2.0f));
      float cosin_cur = ((camera_origins_x_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x
                        +(camera_origins_x_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y
                        +(camera_origins_x_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z) / length;
      if(cosin_cur > cosin_max)
        cosin_max = cosin_cur;
    }
    
    if(cosin_max > 0.0f)
    {
      ++num_cosin;
      quality_evaluation[0] += cosin_max;
    }
  }
  if(num_cosin)
    quality_evaluation[0] /= num_cosin;
    
    
    
    
  
  std::cout<<"**********Evaluate the -x side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_x_minus;
  rotation_x_minus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                   * Eigen::AngleAxisf(0.5*MY_PI, Eigen::Vector3f::UnitY()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_x_minus;
  camera_origins_x_minus.clear();
  camera_origins_x_minus.resize(camera_origins.size());
  
  for(int i = 0; i < camera_origins.size(); ++i)
  {
    camera_origins_x_minus[i] = rotation_x_minus * camera_origins[i];

    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origins_x_minus[i](0), camera_origins_x_minus[i](1), camera_origins_x_minus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin x_minus %03d", i);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 0.0, 1.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of -x side - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  num_cosin = 0;
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
    
    float cosin_max = -1.0f;
    for(int k = 0; k < camera_origins.size(); ++k)
    {
      float length = sqrt(pow(camera_origins_x_minus[k](0)-ptr_filtered_model_bb_cs->points[i].x, 2.0f) 
                         +pow(camera_origins_x_minus[k](1)-ptr_filtered_model_bb_cs->points[i].y, 2.0f)
                         +pow(camera_origins_x_minus[k](2)-ptr_filtered_model_bb_cs->points[i].z, 2.0f));
      float cosin_cur = ((camera_origins_x_minus[k](0)-ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x
                        +(camera_origins_x_minus[k](1)-ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y
                        +(camera_origins_x_minus[k](2)-ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z) / length;
      if(cosin_cur > cosin_max)
        cosin_max = cosin_cur;
    }
    
    if(cosin_max > 0.0f)
    {
      ++num_cosin;
      quality_evaluation[1] += cosin_max;
    }
  }
  if(num_cosin)
    quality_evaluation[1] /= num_cosin;
  
  
  
  
  
  std::cout<<"**********Evaluate the +y side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_y_plus;
  rotation_y_plus = Eigen::AngleAxisf(0.5*MY_PI, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_y_plus;
  camera_origins_y_plus.clear();
  camera_origins_y_plus.resize(camera_origins.size());
  
  for(int i = 0; i < camera_origins.size(); ++i)
  {
    camera_origins_y_plus[i] = rotation_y_plus * camera_origins[i];

    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origins_y_plus[i](0), camera_origins_y_plus[i](1), camera_origins_y_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin y_plus %03d", i);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 0.0, 0.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of +y side - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  num_cosin = 0;
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
    
    float cosin_max = -1.0f;
    for(int k = 0; k < camera_origins.size(); ++k)
    {
      float length = sqrt(pow(camera_origins_y_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x, 2.0f) 
                         +pow(camera_origins_y_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y, 2.0f)
                         +pow(camera_origins_y_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z, 2.0f));
      float cosin_cur = ((camera_origins_y_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x
                        +(camera_origins_y_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y
                        +(camera_origins_y_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z) / length;
      if(cosin_cur > cosin_max)
        cosin_max = cosin_cur;
    }
    
    if(cosin_max > 0.0f)
    {
      ++num_cosin;
      quality_evaluation[2] += cosin_max;
    }
  }
  if(num_cosin)
    quality_evaluation[2] /= num_cosin;





  std::cout<<"**********Evaluate the -y side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_y_minus;
  rotation_y_minus = Eigen::AngleAxisf(-0.5*MY_PI, Eigen::Vector3f::UnitX()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY()) 
                   * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_y_minus;
  camera_origins_y_minus.clear();
  camera_origins_y_minus.resize(camera_origins.size());
  
  for(int i = 0; i < camera_origins.size(); ++i)
  {
    camera_origins_y_minus[i] = rotation_y_minus * camera_origins[i];

    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origins_y_minus[i](0), camera_origins_y_minus[i](1), camera_origins_y_minus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin y_minus %03d", i);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 1.0, 0.0, 1.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of -y side - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  num_cosin = 0;
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
    
    float cosin_max = -1.0f;
    for(int k = 0; k < camera_origins.size(); ++k)
    {
      float length = sqrt(pow(camera_origins_y_minus[k](0)-ptr_filtered_model_bb_cs->points[i].x, 2.0f) 
                         +pow(camera_origins_y_minus[k](1)-ptr_filtered_model_bb_cs->points[i].y, 2.0f)
                         +pow(camera_origins_y_minus[k](2)-ptr_filtered_model_bb_cs->points[i].z, 2.0f));
      float cosin_cur = ((camera_origins_y_minus[k](0)-ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x
                        +(camera_origins_y_minus[k](1)-ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y
                        +(camera_origins_y_minus[k](2)-ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z) / length;
      if(cosin_cur > cosin_max)
        cosin_max = cosin_cur;
    }
    
    if(cosin_max > 0.0f)
    {
      ++num_cosin;
      quality_evaluation[3] += cosin_max;
    }
  }
  if(num_cosin)
    quality_evaluation[3] /= num_cosin;
  
  




  std::cout<<"**********Evaluate the +z side as the new bottom**********"<<std::endl;
  Eigen::Matrix3f rotation_z_plus;
  rotation_z_plus = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX()) 
                  * Eigen::AngleAxisf(MY_PI, Eigen::Vector3f::UnitY()) 
                  * Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

  std::vector<Eigen::Vector3f> camera_origins_z_plus;
  camera_origins_z_plus.clear();
  camera_origins_z_plus.resize(camera_origins.size());
  
  for(int i = 0; i < camera_origins.size(); ++i)
  {
    camera_origins_z_plus[i] = rotation_z_plus * camera_origins[i];

    #ifdef DEBUG_MODEL_QUALITY
    pcl::PointXYZ origin_show(camera_origins_z_plus[i](0), camera_origins_z_plus[i](1), camera_origins_z_plus[i](2));
    char origin_name[20];
    sprintf(origin_name, "origin z_plus %03d", i);
    pcd_viewer_model_quality->addSphere(origin_show, 0.05, 1.0, 1.0, 0.0, origin_name);
    #endif
  }
  #ifdef DEBUG_MODEL_QUALITY
  std::cout<<"Displayed the camera origins of +z side - Press q to continue"<<std::endl;
  pcd_viewer_model_quality->spin();
  #endif
  
  num_cosin = 0;
  for(int i = 0; i < ptr_filtered_model_bb_cs->points.size(); ++i)
  {
    if(isnan(ptr_filtered_model_bb_cs->points[i].x) || isnan(ptr_model_normals->points[i].normal_x))
      continue;
    
    float cosin_max = -1.0f;
    for(int k = 0; k < camera_origins.size(); ++k)
    {
      float length = sqrt(pow(camera_origins_z_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x, 2.0f) 
                         +pow(camera_origins_z_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y, 2.0f)
                         +pow(camera_origins_z_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z, 2.0f));
      float cosin_cur = ((camera_origins_z_plus[k](0)-ptr_filtered_model_bb_cs->points[i].x) * ptr_model_normals->points[i].normal_x
                        +(camera_origins_z_plus[k](1)-ptr_filtered_model_bb_cs->points[i].y) * ptr_model_normals->points[i].normal_y
                        +(camera_origins_z_plus[k](2)-ptr_filtered_model_bb_cs->points[i].z) * ptr_model_normals->points[i].normal_z) / length;
      if(cosin_cur > cosin_max)
        cosin_max = cosin_cur;
    }
    
    if(cosin_max > 0.0f)
    {
      ++num_cosin;
      quality_evaluation[4] += cosin_max;
    }
  }
  if(num_cosin)
    quality_evaluation[4] /= num_cosin;
  
  #ifdef DEBUG_MODEL_QUALITY
  printf("Quality Evaluation Results for (+x, -x, +y, -y, +z): \n");
  for(int i = 0; i < quality_evaluation.size(); ++i)
    printf("%10f ", quality_evaluation[i]);
  printf("\n");
  #endif

  return quality_evaluation;
}



