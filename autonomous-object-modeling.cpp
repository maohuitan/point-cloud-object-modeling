//C++ header files
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <deque>

//Project header files
#include "objects-localization.h"
#include "modeling-and-manipulation.h"
#include "pose-planning.h"

#include "pcl/io/pcd_io.h"
#include "pcl/point_types.h"
#include "pcl/visualization/cloud_viewer.h"
#include "pcl/visualization/pcl_visualizer.h"
#include "pcl/features/normal_3d.h"
#include "pcl/registration/icp.h"
#include "pcl/registration/icp_nl.h"
#include "pcl/registration/transforms.h"
#include "pcl/kdtree/kdtree_flann.h"
#include "pcl/filters/voxel_grid.h"

#include "pcl/registration/ndt.h"
#include "pcl/filters/approximate_voxel_grid.h"
#include "pcl/filters/statistical_outlier_removal.h"
#include "pcl/filters/voxel_grid.h"


int main(int argc, char* argv[])
{
	
  int choice;
  
  printf("*****************************************************************\n");
  printf("*****Please choose what you wanna do:\n");
  printf("*****00: exit\n");
  printf("*****01: show a point cloud\n");
  printf("*****02: object modeling with multiple perception continously\n");
  printf("*****03: object modeling with multiple perception step by step\n");
  printf("*****04: removing outliers of a point cloud using a StatisticalOutlierRemoval filter\n");
  printf("*****05: show the normals of a point cloud after a specific transformation\n");
  printf("*****06: obtain a oriented bounding box which is parallel to the XY plane (the table plane)\n");
  printf("*****07: register a partial object point cloud to an object model point cloud\n"); 
  printf("*****08: test optimize_globally_registration_in_360_rotation\n"); 
  printf("*****09: test STL deque\n"); 
  printf("*****10: test the transformation matrix between the rotations\n"); 
  printf("*****11: evaluate the overlap and new unseen area factors for poses\n"); 
  printf("*****12: evaluate the model quality factors for poses\n");
  printf("*****13: evaluate the mean k-nearest neighbours distance distribution of points\n");
  printf("*****14: evaluate the average number of neighbours within a radius\n");
  printf("*****15: temporalily compute the transformation matrix\n"); 
  printf("*****16: register two point clouds by myself\n"); 
  printf("*****************************************************************\n");
  printf("Choose >> ");
  
  std::cin>>choice;
  switch(choice)
  {
	case 0:
	  return 0;
	case 1:
	{
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Autonomous Object Modeling");
      
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd);
      
      // Create the filtering object
      pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
      sor.setLeafSize(0.001f, 0.001f, 0.001f);
  
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      sor.setInputCloud(ptr_pcd);
      sor.filter(*ptr_pcd_filtered);
      
      pcd_viewer->addPointCloud(ptr_pcd_filtered, "point cloud");
      std::cout<<"Point Size:"<<ptr_pcd_filtered->points.size()<<std::endl;
      
      PCL_INFO("Press q to stop.\n");
      pcd_viewer->spin();
      
	  break;
    }
	case 2:
	{
	  for(int index_perception = 1; index_perception <= 12; ++index_perception)
	  {
		std::cout<<"index_perception:"<<index_perception<<std::endl;
        char path_current_perception[1024];
        sprintf(path_current_perception, "coffee_bottle_images/scene_1_%d.pcd", index_perception);
        autonomous_modeling_and_manipulation("test-results-coffee-bottle-001", 
                                             "test-results-coffee-bottle-001/object_model_updating_real_time.pcd", 
                                             path_current_perception, index_perception);
	  }
      break;
    }
	case 3:
	{
      char path_current_perception[1024];
      printf("*****Please choose the perception index:\n");
      int index_perception;
      std::cin>>index_perception;
      sprintf(path_current_perception, "%s", argv[1]);
      autonomous_modeling_and_manipulation("test-results-001", "test-results-001/object_model_updating_real_time.pcd", path_current_perception, index_perception);
      break;
    }
	case 4:
	{
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Show a Point Cloud");
      
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd);
      std::cout<<"Original Size:"<<ptr_pcd->points.size()<<std::endl;
      
      pcd_viewer->addPointCloud(ptr_pcd, "point cloud");
      
      PCL_INFO("Press q to stop.\n");
      pcd_viewer->spin();
      
      std::cout<<"Removing outliers using a StatisticalOutlierRemoval filter"<<std::endl;
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      // Create the filtering object
      pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor;
      sor.setInputCloud(ptr_pcd);
      sor.setMeanK(50);
      sor.setStddevMulThresh(3.0);
      sor.filter(*ptr_pcd);
      std::cout<<"After Filtering Size:"<<ptr_pcd->points.size()<<std::endl;
      
      pcd_viewer->removePointCloud("point cloud");
      pcd_viewer->addPointCloud(ptr_pcd, "point cloud");
      
      PCL_INFO("Press q to stop after filtering.\n");
      pcd_viewer->spin();
      
	  break;
    }
    case 5:
    {
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Show a transformed Point Cloud with normals");
      pcd_viewer->addCoordinateSystem(1.0);
      pcd_viewer->initCameraParameters();
      
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd);
      std::cout<<"Original Point Cloud Size:"<<ptr_pcd->points.size()<<std::endl;
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
      Eigen::Matrix4f transform_to_robot_cs;
      transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                            -0.0303733,   0.628287,  -0.777389,   0.597786,
                            -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                     0,          0,          0,          1;
      std::cout<<"*****transform_to_robot_cs:*****"<<std::endl<<transform_to_robot_cs<<std::endl;
      
      //transform the object model point cloud to the robot coordinate system
      pcl::transformPointCloud(*ptr_pcd, *ptr_model_robot_cs, transform_to_robot_cs);
      
      /*
      //compute the point normals
      pcl::NormalEstimation<pcl::PointXYZRGBA, pcl::Normal> norm_est;
      pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr ptr_search_tree(new pcl::search::KdTree<pcl::PointXYZRGBA> ());
      norm_est.setSearchMethod(ptr_search_tree);
      norm_est.setRadiusSearch(0.02);   //0.02m = 20mm
  
      pcl::PointCloud<pcl::Normal>::Ptr ptr_model_robot_cs_normals(new pcl::PointCloud<pcl::Normal> ());  
      norm_est.setInputCloud(ptr_model_robot_cs);
      norm_est.compute(*ptr_model_robot_cs_normals);
      
      //compute the XYZ position of the center point
      pcl::PointXYZ center_point(0.0f, 0.0f, 0.0f);
      std::cout<<center_point.x<<"  "<<center_point.y<<"  "<<center_point.z<<std::endl;
      int num_points_average = 0;
      for(int i = 0; i < ptr_model_robot_cs->points.size(); ++i)
      {
	    if(isnan(ptr_model_robot_cs->points[i].x))
	      continue;
	    
	    ++num_points_average;
	    center_point.x += ptr_model_robot_cs->points[i].x;
	    center_point.y += ptr_model_robot_cs->points[i].y;
	    center_point.z += ptr_model_robot_cs->points[i].z;
	  }
	  
	  if(num_points_average)
	  {
	    center_point.x /= num_points_average;
	    center_point.y /= num_points_average;
	    center_point.z /= num_points_average;
	  }
	  std::cout<<"num_points_average:  "<<num_points_average<<std::endl;
      std::cout<<"Updated Center Point Position:  "<<center_point.x<<"  "<<center_point.y<<"  "<<center_point.z<<std::endl;
      
      //adjust the normal directions so that they always point to the outside of the object
      //the current strategy only works on convex objects
      for(int i = 0; i < ptr_model_robot_cs->points.size(); ++i)
      {
	    if(isnan(ptr_model_robot_cs->points[i].x) || isnan(ptr_model_robot_cs_normals->points[i].normal_x))
	      continue;
	    
	    float dot_sign;
	    dot_sign = (center_point.x - ptr_model_robot_cs->points[i].x) * ptr_model_robot_cs_normals->points[i].normal_x + 
	               (center_point.y - ptr_model_robot_cs->points[i].y) * ptr_model_robot_cs_normals->points[i].normal_y +
	               (center_point.z - ptr_model_robot_cs->points[i].z) * ptr_model_robot_cs_normals->points[i].normal_z;
	    if(dot_sign > 0.0f)
	    {
		  ptr_model_robot_cs_normals->points[i].normal_x = - ptr_model_robot_cs_normals->points[i].normal_x;
		  ptr_model_robot_cs_normals->points[i].normal_y = - ptr_model_robot_cs_normals->points[i].normal_y;
		  ptr_model_robot_cs_normals->points[i].normal_z = - ptr_model_robot_cs_normals->points[i].normal_z;
		}
	  }
	  */
      
      pcd_viewer->addPointCloud(ptr_model_robot_cs, "final model in robot cs");
      //pcd_viewer->addPointCloudNormals<pcl::PointXYZRGBA, pcl::Normal>(ptr_model_robot_cs, ptr_model_robot_cs_normals, 10, 0.05, "normals");
      
      PCL_INFO("Press q to stop.\n");
      pcd_viewer->spin();    
      
	  break;
	}
	case 6:
	{  
      Eigen::Matrix4f transform_to_robot_cs;
      transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                            -0.0303733,   0.628287,  -0.777389,   0.597786,
                            -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                     0,          0,          0,          1;
                                     
      pcl::visualization::PCLVisualizer *pcd_viewer_robot;
      pcd_viewer_robot = new pcl::visualization::PCLVisualizer("Show the object model in the robot CS");
      pcd_viewer_robot->addCoordinateSystem(0.3);
      pcd_viewer_robot->initCameraParameters();
      
      //read the point cloud of the model
	  char path_model_after_rotations[1024];
      sprintf(path_model_after_rotations, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_after_rotations(new pcl::PointCloud<pcl::PointXYZRGBA>());
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_model_after_rotations, *ptr_model_after_rotations);

      //transform the refined model to the robot coordinate system
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_refined_model_robot_cs(new pcl::PointCloud<pcl::PointXYZRGBA>());
      pcl::transformPointCloud(*ptr_model_after_rotations, *ptr_refined_model_robot_cs, transform_to_robot_cs);
      
      //compute a bounding box for the point cloud of the modeling object in robot CS
      Eigen::Vector3f bbox_centroid;
      Eigen::Matrix3f bbox_directions;
      float length_1st, length_2nd, length_3rd;
      obtain_oriented_bounding_box_on_XY_plane(ptr_refined_model_robot_cs, bbox_centroid, bbox_directions,
                                               length_1st, length_2nd, length_3rd);
      
      std::cout<<"length_1st:"<<length_1st<<std::endl;
      std::cout<<"length_2nd:"<<length_2nd<<std::endl;
      std::cout<<"length_3rd:"<<length_3rd<<std::endl;
      
      const Eigen::Quaternionf bbox_quaternion(bbox_directions); 
      pcd_viewer_robot->addPointCloud(ptr_refined_model_robot_cs, "final model in robot cs");
      pcd_viewer_robot->addCube(bbox_centroid, bbox_quaternion, length_1st, length_2nd, length_3rd, "bbox");
      PCL_INFO("Press q to continue.\n");
      pcd_viewer_robot->spin();

      //transform the object model to the bounding box coordinate system
      Eigen::Matrix4f bb_transform(Eigen::Matrix4f::Identity());
      bb_transform.block<3, 3>(0, 0) = bbox_directions.transpose();
      bb_transform.block<3, 1>(0, 3) = -1.f * (bb_transform.block<3, 3>(0, 0) * bbox_centroid);
        
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_bb_cs(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::transformPointCloud(*ptr_refined_model_robot_cs, *ptr_model_bb_cs, bb_transform);
      
      std::cout<<"bbox_centroid in bb CS: "<<std::endl<<bb_transform.block<3, 3>(0, 0)*bbox_centroid+bb_transform.block<3, 1>(0, 3)<<std::endl;
      std::cout<<"bbox_directions in bb CS: "<<std::endl<<bb_transform.block<3, 3>(0, 0)*bbox_directions<<std::endl;
      
      //transform the object model to the bounding box coordinate system
      //visualize the point cloud in the PCA axes
      pcl::visualization::PCLVisualizer *pcd_viewer_bb;
      pcd_viewer_bb = new pcl::visualization::PCLVisualizer("Show the object model in bounding box axes");
      pcd_viewer_bb->addCoordinateSystem(0.3);
      pcd_viewer_bb->initCameraParameters(); 

      pcd_viewer_bb->addPointCloud(ptr_model_bb_cs, "final model in PCA axes");
      const Eigen::Quaternionf bbox_quaternion_in_bb(bb_transform.block<3, 3>(0, 0)*bbox_directions); 
      pcd_viewer_bb->addCube(bb_transform.block<3, 3>(0, 0)*bbox_centroid+bb_transform.block<3, 1>(0, 3), 
                            bbox_quaternion_in_bb, length_1st, length_2nd, length_3rd, "bbox");
      PCL_INFO("Press q to continue.\n");
      pcd_viewer_bb->spin();
                                          
	  break;
	}
	case 7:
	{
	  char path_load_pcd[1024];
	  
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd_part(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd_part);
      std::cout<<"Partial Point Cloud Size:"<<ptr_pcd_part->points.size()<<std::endl;
      
      sprintf(path_load_pcd, "%s", argv[2]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd_model(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd_model);
      std::cout<<"Model Point Cloud Size:"<<ptr_pcd_model->points.size()<<std::endl;
      
      double register_accuracy;
      Eigen::Matrix4f transform = align_point_clouds_using_icp_plus_color(ptr_pcd_part, ptr_pcd_model, Eigen::Matrix4f::Identity(), register_accuracy);
      std::cout<<"register_accuracy: "<<register_accuracy<<std::endl;
      
      break;
	}
	case 8:
	{
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Object Model Optimization");
      
	  std::vector<Eigen::Matrix4f> transforms_pairwise;
      std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> ptr_obj_pcls;
      
      transforms_pairwise.clear();
      ptr_obj_pcls.clear();
      
      const int index_begin = 20;
      const int index_end = 33;
      const int num_views = index_end - index_begin + 1;
      
      //read all the point clouds
      for (int index_view = index_begin; index_view <= index_end; ++index_view)
      {
        //load the object point cloud in the perception
        char path_pcl[1024];
        sprintf(path_pcl, "%s/object_point_cloud_in_scene_%03d.pcd", path_load_pcd, index_view);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcl(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_pcl, *ptr_pcl);
        
        if (ptr_pcl == NULL)
        {
          printf("Fail to load the object point cloud %03d\n", index_view);
          return 0;
	    }
        
        ptr_obj_pcls.push_back(ptr_pcl);
        
        pcd_viewer->removePointCloud("point cloud");
        pcd_viewer->addPointCloud(ptr_obj_pcls[index_view-index_begin], "point cloud");
        printf("Loaded View %03d \n", index_view);
        //pcd_viewer->spin();
        //pcd_viewer->spinOnce(1000);
	  }
	  
	  //read all the transformation matrices
	  Eigen::Matrix4f transform = register_point_clouds_from_two_views(path_load_pcd, index_end, index_begin);
	  std::cout<<"Last To First:"<<std::endl<<transform<<std::endl;
	  transforms_pairwise.push_back(transform);            
                                                  
	  for (int index_view = index_begin; index_view <= index_end-1; ++index_view)
	  {
	    std::ifstream cin_transform;
        char path_transform[1024];
        sprintf(path_transform, "%s/transformation_from_%03d_to_%03d.txt", path_load_pcd, index_view, index_view + 1); 
        cin_transform.open(path_transform);
        if(!cin_transform.is_open())
	      std::cout<<"Fail to open the file to obtain the transform information!"<<std::endl;
	    
	    Eigen::Matrix4f transform;
	    for(int i = 0; i < 4; ++i)
	      for(int j = 0; j < 4; ++j)
	        cin_transform>>transform(i, j);
	
	    cin_transform.close();
	    
	    transforms_pairwise.push_back(transform);
	  }
	  
	  for(int i = 0; i < transforms_pairwise.size(); ++i)
	  {
	    std::cout<<"To View : "<<i + 1<<std::endl;
	    std::cout<<transforms_pairwise[i]<<std::endl;
	  }
	  
	  std::vector<Eigen::Matrix4f> poses = optimize_globally_registration_in_360_rotation(transforms_pairwise, ptr_obj_pcls);
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_new_model(new pcl::PointCloud<pcl::PointXYZRGBA>());
      char path_model_before_optimization[1024];
      sprintf(path_model_before_optimization, "%s/object_model_backup_after_scene_%03d.pcd", argv[1], index_end);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_model_before_optimization, *ptr_new_model);
      
      std::cout<<"Show the partial model before the optimization"<<std::endl;
      pcd_viewer->removePointCloud("point cloud"); 
      pcd_viewer->addPointCloud(ptr_new_model, "point cloud");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      std::cout<<"Show the partial model after the optimization"<<std::endl;
      ptr_new_model->points.clear();
      ptr_new_model->height = 1;
      ptr_new_model->width = ptr_new_model->points.size();
      ptr_new_model->is_dense = true;
      
	  for(int i = 0; i < poses.size(); ++i)
	  {
	    std::cout<<"View : "<<i + 1<<std::endl;
	    std::cout<<poses[i]<<std::endl;
	  }
    
      for(int i = 0; i < num_views; ++i)
      {
        //transform the point cloud in each view to the base coordinate system
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_to_base(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::transformPointCloud(*ptr_obj_pcls[i], *ptr_to_base, poses[i]);
        *ptr_new_model += *ptr_to_base;
      }
      pcd_viewer->removePointCloud("point cloud"); 
      pcd_viewer->addPointCloud(ptr_new_model, "point cloud");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
	  break;
	}
	case 9:
	{
	  std::deque<int> deque_views;
	  deque_views.clear();
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  
	  deque_views.push_back(100);
	  deque_views.push_back(200);
	  deque_views.push_back(300);
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  for(int i = 0; i < deque_views.size(); ++i)
	    std::cout<<deque_views[i]<<" ";
	  std::cout<<std::endl;
	  
	  std::cout<<"pop out :"<<deque_views[0]<<std::endl;
	  deque_views.pop_front();
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  for(int i = 0; i < deque_views.size(); ++i)
	    std::cout<<deque_views[i]<<" ";
	  std::cout<<std::endl;
	  
	  deque_views.push_back(300);
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  for(int i = 0; i < deque_views.size(); ++i)
	    std::cout<<deque_views[i]<<" ";
	  std::cout<<std::endl;
	  
	  std::cout<<"pop out :"<<deque_views[0]<<std::endl;
	  deque_views.push_back(100);
	  deque_views.pop_front();
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  for(int i = 0; i < deque_views.size(); ++i)
	    std::cout<<deque_views[i]<<" ";
	  std::cout<<std::endl;
	  
	  while(!deque_views.empty())
	    deque_views.pop_front();
	  std::cout<<"deque size: "<<deque_views.size()<<std::endl;
	  
	  break;
	}
    case 10:
    {
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Optimization Between Rotations");
      
      //transformation from the end view CS of the last partial model to the begin view CS of the current partial model
      Eigen::Matrix4f transform_from_last_model;
      //transformation from the begin view CS of the current partial model to the (end view) CS of the current partial model
      Eigen::Matrix4f transform_from_begin_view;
      
      std::ifstream cin_transform;
      char path_transform[1024];
      
      //transformation from the end view CS of the last partial model to the begin view CS of the current partial model;
      //Attention: this is with respect to the robot coordinate system
      sprintf(path_transform, "%s/transformation_from_009_to_010_in_robot_cs.txt", argv[1]); 
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information from last rotation in robot CS!"<<std::endl;
        return 0;
      }
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform_from_last_model(i, j);
      cin_transform.close();
      
      //transformation from the begin view CS of the current partial model to the (end view) CS of the current partial model
      sprintf(path_transform, "%s/transformation_to_model_for_view_020.txt", argv[1]); 
      cin_transform.open(path_transform);
      if (!cin_transform.is_open())
      {
        std::cout<<"Error: fail to open the file to obtain the transform information from the begin view!"<<std::endl;
        return 0;
      }
      for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
          cin_transform>>transform_from_begin_view(i, j);
      cin_transform.close();
      
      Eigen::Matrix4f transform_to_robot_cs;
      transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                            -0.0303733,   0.628287,  -0.777389,   0.597786,
                            -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                     0,          0,          0,          1;
                                     
      Eigen::Matrix4f transform =  transform_from_begin_view * transform_to_robot_cs.inverse() * transform_from_last_model * transform_to_robot_cs;
                                                        
      //save the refined partial model for future use
      char path_refined_model_backup[1024];
      
      sprintf(path_refined_model_backup, "%s/refined_partial_object_model_001.pcd", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_old);
      
      sprintf(path_refined_model_backup, "%s/refined_partial_object_model_002.pcd", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_new(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_new);

      pcd_viewer->addPointCloud(ptr_model_old, "old");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old_transformed(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::transformPointCloud(*ptr_model_old, *ptr_model_old_transformed, transform);
      
      pcd_viewer->removePointCloud("old");
      pcd_viewer->addPointCloud(ptr_model_old_transformed, "old");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      std::cout<<"Old Model Size: "<<ptr_model_old_transformed->points.size()<<std::endl;
      std::cout<<"New Model Size: "<<ptr_model_new->points.size()<<std::endl;
      
      // Create the filtering object
      pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
      sor.setLeafSize(0.002f, 0.002f, 0.002f);
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_new_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      sor.setInputCloud(ptr_model_new);
      sor.filter(*ptr_model_new_filtered);
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      sor.setInputCloud(ptr_model_old_transformed);
      sor.filter(*ptr_model_old_filtered);
      
      std::cout<<"Old Model Size After Filtering: "<<ptr_model_old_filtered->points.size()<<std::endl;
      std::cout<<"New Model Size After Filtering: "<<ptr_model_new_filtered->points.size()<<std::endl;
      
      std::cout<<"after filtering"<<std::endl;
      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_old_filtered, "old");
      pcd_viewer->addPointCloud(ptr_model_new_filtered, "new");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      Eigen::Matrix4f transform1, transform2;
      double register_accuracy1, register_accuracy2;
  
      //register A to B
      Eigen::Matrix4f inv_transform = align_point_clouds_using_icp_plus_color_between_models(ptr_model_new_filtered, ptr_model_old_filtered, Eigen::Matrix4f::Identity(), register_accuracy1);
      transform1 = inv_transform.inverse();
      std::cout<<"register_accuracy1: "<<register_accuracy1<<std::endl;
  
      //register B to A
      transform2 = align_point_clouds_using_icp_plus_color_between_models(ptr_model_old_filtered, ptr_model_new_filtered, Eigen::Matrix4f::Identity(), register_accuracy2);
      std::cout<<"register_accuracy2: "<<register_accuracy2<<std::endl;
    
      Eigen::Matrix4f transform_accurate = Eigen::Matrix4f::Identity();
      //choose the better one
      transform_accurate = (register_accuracy1 < register_accuracy2) ? transform1 : transform2;
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old_transformed_2nd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::transformPointCloud(*ptr_model_old_transformed, *ptr_model_old_transformed_2nd, transform_accurate);
      *ptr_model_new += *ptr_model_old_transformed_2nd;
      
      pcl::io::savePCDFileASCII("final_refined_model.pcd", *ptr_model_new);
      
      std::cout<<"the refined transformation:"<<std::endl;
      std::cout<<transform_accurate * transform<<std::endl;
      
      std::cout<<"show the complete model"<<std::endl;
      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      break;
    }
    case 11:
    {
      Eigen::Matrix4f transform_to_robot_cs;
      transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                            -0.0303733,   0.628287,  -0.777389,   0.597786,
                            -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                     0,          0,          0,          1;
                                     
      std::vector<float> area_evaluation = evaluate_overlap_and_new_for_using_other_five_sides_as_bottom(argv[1], 1, transform_to_robot_cs);
      printf("Area Evaluation Results for using (+x, -x, +y, -y, +z) sides as the new bottoms: \n");
      for(int i = 0; i < area_evaluation.size(); ++i)
        printf("%10f ", area_evaluation[i]);
      printf("\n");
      break;
    }
    case 12:
    {
      Eigen::Matrix4f transform_to_robot_cs;
      transform_to_robot_cs<<-0.999523, -0.0147982,  0.0270933,    0.58127,
                            -0.0303733,   0.628287,  -0.777389,   0.597786,
                            -0.0055187,  -0.777841,  -0.628437,   0.839331,
                                     0,          0,          0,          1;
                                     
      //The quality of the model ,using 5 different bounding box sides (+x side, -x side, +y side, -y side, +z side) as the new bottom sides;
      //generate the same bounding box and evluate with the camera origins.
      std::vector<float> quality_evaluation = evaluate_quality_of_model_for_using_other_five_sides_as_bottom(argv[1], 2, transform_to_robot_cs);
      printf("Quality Evaluation Results for (+x, -x, +y, -y, +z): \n");
      for(int i = 0; i < quality_evaluation.size(); ++i)
        printf("%10f ", quality_evaluation[i]);
      printf("\n");
      break;
    }
	case 13:
	{
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Autonomous Object Modeling");
      
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd);
      
      pcd_viewer->addPointCloud(ptr_pcd, "point cloud");
      std::cout<<"Point Size:"<<ptr_pcd->points.size()<<std::endl;
      PCL_INFO("Press q to continue.\n");
      pcd_viewer->spin();
      
      //create a kdtree using the target point cloud for searching
      pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
      kdtree.setInputCloud(ptr_pcd);
      
      std::vector<int> dist_vote(1000, 0);
      
      for(int index_point = 0; index_point < ptr_pcd->points.size(); ++index_point)
      {
        pcl::PointXYZRGBA search_point = ptr_pcd->points[index_point];
        
        int num_knn = 100;
        
        std::vector<int> knn_indices(num_knn);
        std::vector<float> knn_squared_distance(num_knn);
        kdtree.nearestKSearch(search_point, num_knn, knn_indices, knn_squared_distance);
        
        float dist_sum = 0; 
        for(int i = 0; i < num_knn; ++i)
          dist_sum += sqrt(knn_squared_distance[i]);
        
        float dist_average = dist_sum / num_knn;
        
        int entry_id = (int)(dist_average * 10000);
        ++dist_vote[entry_id];
      }
      
      for(int i = 0; i < 100; ++i)
        std::cout<<dist_vote[i]<<"  ";
      std::cout<<std::endl;

      //save all the information
      std::ofstream cout_vote;
      cout_vote.open("dist_vote_vert.txt");
      if (!cout_vote.is_open())
      {
        std::cout<<"Error: fail to open the text file to save the vote information!"<<std::endl;
        break;
      }
      for(int i = 1; i < 100; ++i)
        cout_vote<<dist_vote[i]<<std::endl;
      cout_vote.close();

      cout_vote.open("dist_vote_hori.txt");
      if (!cout_vote.is_open())
      {
        std::cout<<"Error: fail to open the text file to save the vote information!"<<std::endl;
        break;
      }
      for(int i = 1; i < 100; ++i)
        cout_vote<<(0.1f*i)<<std::endl;
      cout_vote.close();
      
      break;
    }
	case 14:
	{
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Autonomous Object Modeling");
      
	  char path_load_pcd[1024];
      sprintf(path_load_pcd, "%s", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_pcd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_load_pcd, *ptr_pcd);
      
      pcd_viewer->addPointCloud(ptr_pcd, "point cloud");
      std::cout<<"Point Size:"<<ptr_pcd->points.size()<<std::endl;
      PCL_INFO("Press q to continue.\n");
      pcd_viewer->spin();
      
      //create a kdtree using the target point cloud for searching
      pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
      kdtree.setInputCloud(ptr_pcd);
      
      std::vector<int> neighbours_vote(50, 0);
      
      int max_num_neighbours = 0;
        
      for(int index_point = 0; index_point < ptr_pcd->points.size(); ++index_point)
      {
        std::vector<int> result_indices;
        std::vector<float> result_square_distance;
        kdtree.radiusSearch(ptr_pcd->points[index_point], 0.003f, result_indices, result_square_distance);
        if(result_indices.size() > max_num_neighbours)
          max_num_neighbours = result_indices.size();
        ++neighbours_vote[result_indices.size()/5];
      }
      std::cout<<"max_num_neighbours: "<<max_num_neighbours<<std::endl;
      for(int i = 1; i < 50; ++i)
        std::cout<<neighbours_vote[i]<<"  ";
      std::cout<<std::endl;

      //save all the information
      std::ofstream cout_vote;
      cout_vote.open("neighbours_vote_vert.txt");
      if (!cout_vote.is_open())
      {
        std::cout<<"Error: fail to open the text file to save the vote information!"<<std::endl;
        break;
      }
      for(int i = 1; i < 50; ++i)
        cout_vote<<neighbours_vote[i]<<std::endl;
      cout_vote.close();

      cout_vote.open("neighbours_vote_hori.txt");
      if (!cout_vote.is_open())
      {
        std::cout<<"Error: fail to open the text file to save the vote information!"<<std::endl;
        break;
      }
      for(int i = 1; i < 50; ++i)
        cout_vote<<i*5<<std::endl;
      cout_vote.close();
      
      break;
    }
	case 15:
	{
      double x_angle = 0.0 * 3.1415 / 180;
      double y_angle = 0.0 * 3.1415 / 180;
      double z_angle = 270.0 * 3.1415 / 180;
      Eigen::Matrix4f transformation_between_loops;
      transformation_between_loops<<  0.641461,  -0.470134,  0.606219,  -0.459145,
                                      0.519135,   0.847818,   0.108183, -0.0692915,
                                     -0.564824,   0.245315,   0.787905,   0.145716,
                                     -0,         -0,         -0,          1;

      Eigen::Matrix4f rotation_x, rotation_y, rotation_z;
      rotation_x<<1,      0,      0,   0,
                  0,   cos(x_angle), -sin(x_angle), 0,
                  0,   sin(x_angle),  cos(x_angle), 0,
                  0,              0,             0, 1;
      rotation_y<<cos(y_angle), 0, sin(y_angle), 0,
                             0, 1,            0, 0,
                 -sin(y_angle), 0, cos(y_angle), 0,
                             0, 0,            0, 1;
      rotation_z<<cos(z_angle), -sin(z_angle), 0, 0,
                  sin(z_angle),  cos(z_angle), 0, 0,
                             0,             0, 1, 0,
                             0,             0, 0, 1;
      std::cout<<rotation_z*rotation_y*rotation_x*transformation_between_loops<<std::endl;
      
      break;
    }
    case 16:
    {
      pcl::visualization::PCLVisualizer *pcd_viewer;
      pcd_viewer = new pcl::visualization::PCLVisualizer("Optimization Between Rotations");
      pcd_viewer->addCoordinateSystem(0.3);
      pcd_viewer->initCameraParameters(); 
      
      char path_refined_model_backup[1024];
      
      sprintf(path_refined_model_backup, "%s/refined_partial_object_model_001.pcd", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_old);
      
      sprintf(path_refined_model_backup, "%s/refined_partial_object_model_002.pcd", argv[1]);
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_new(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::io::loadPCDFile<pcl::PointXYZRGBA>(path_refined_model_backup, *ptr_model_new);

      pcd_viewer->addPointCloud(ptr_model_old, "old");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      //compute a bounding box for the point cloud of the model new
      Eigen::Vector3f bbox_centroid;
      Eigen::Matrix3f bbox_directions;
      float length_1st, length_2nd, length_3rd;
      obtain_oriented_bounding_box_on_XY_plane(ptr_model_new, bbox_centroid, bbox_directions,
                                               length_1st, length_2nd, length_3rd);

      //transform the object model to the bounding box coordinate system
      Eigen::Matrix4f bb_transform(Eigen::Matrix4f::Identity());
      bb_transform.block<3, 3>(0, 0) = bbox_directions.transpose();
      bb_transform.block<3, 1>(0, 3) = -1.f * (bb_transform.block<3, 3>(0, 0) * bbox_centroid);
        
      pcl::transformPointCloud(*ptr_model_old, *ptr_model_old, bb_transform);
      pcl::transformPointCloud(*ptr_model_new, *ptr_model_new, bb_transform);

      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_old, "old");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Transform to BB CS:"<<std::endl<<bb_transform<<std::endl;
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      double x_angle, y_angle, z_angle;
      Eigen::Matrix4f rotation_x, rotation_y, rotation_z;

      //set them on the table XY plane
      x_angle = 0.0 * 3.1415 / 180;   //for spray
      y_angle = 45.0 * 3.1415 / 180;  //for spray
      z_angle = 0.0 * 3.1415 / 180;   //for spray
      //x_angle = -20.0 * 3.1415 / 180;   //for coffee box, milkbox
      //y_angle = 45.0 * 3.1415 / 180;   //for coffee box, milkbox
      //z_angle = 15.0 * 3.1415 / 180;   //for coffee box, milkbox
      
      rotation_x<<1,      0,      0,   0,
                  0,   cos(x_angle), -sin(x_angle), 0,
                  0,   sin(x_angle),  cos(x_angle), 0,
                  0,              0,             0, 1;
      rotation_y<<cos(y_angle), 0, sin(y_angle), 0,
                             0, 1,            0, 0,
                 -sin(y_angle), 0, cos(y_angle), 0,
                             0, 0,            0, 1;
      rotation_z<<cos(z_angle), -sin(z_angle), 0, 0,
                  sin(z_angle),  cos(z_angle), 0, 0,
                             0,             0, 1, 0,
                             0,             0, 0, 1;
      Eigen::Matrix4f XY_transform = rotation_z*rotation_y*rotation_x; //for spray
      pcl::transformPointCloud(*ptr_model_old, *ptr_model_old, rotation_z*rotation_y*rotation_x); //for spray
      pcl::transformPointCloud(*ptr_model_new, *ptr_model_new, rotation_z*rotation_y*rotation_x); //for spray
      //Eigen::Matrix4f XY_transform = rotation_x*rotation_z*rotation_y; //for coffee box, milkbox
      //pcl::transformPointCloud(*ptr_model_old, *ptr_model_old, rotation_x*rotation_z*rotation_y); //for coffee box, milkbox
      //pcl::transformPointCloud(*ptr_model_new, *ptr_model_new, rotation_x*rotation_z*rotation_y); //for coffee box, milkbox
      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_old, "old");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Transform to XY table plane: "<<std::endl<<XY_transform<<std::endl;
      std::cout<<"Set on XY table plane, Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      //transform the old one for the situable position for the registration
      x_angle = 0 * 3.1415 / 180;   //for spray
      y_angle = 180.0 * 3.1415 / 180;    //for spray
      z_angle = -10.0 * 3.1415 / 180;    //for spray
      //x_angle = -20.0 * 3.1415 / 180;   //for coffee box, milkbox
      //y_angle = -180.0 * 3.1415 / 180;   //for coffee box, milkbox
      //z_angle = 0.0 * 3.1415 / 180;   //for coffee box, milkbox
      
      rotation_x<<1,      0,      0,   0,
                  0,   cos(x_angle), -sin(x_angle), 0,
                  0,   sin(x_angle),  cos(x_angle), 0,
                  0,              0,             0, 1;
      rotation_y<<cos(y_angle), 0, sin(y_angle), 0,
                             0, 1,            0, 0,
                 -sin(y_angle), 0, cos(y_angle), 0,
                             0, 0,            0, 1;
      rotation_z<<cos(z_angle), -sin(z_angle), 0, 0,
                  sin(z_angle),  cos(z_angle), 0, 0,
                             0,             0, 1, 0,  //for coffee box -0.05; for spray, 0
                             0,             0, 0, 1;
      
      Eigen::Matrix4f rough_transform = rotation_z*rotation_y*rotation_x; //for spray
      pcl::transformPointCloud(*ptr_model_old, *ptr_model_old, rotation_z*rotation_y*rotation_x);  //for spray
      //Eigen::Matrix4f rough_transform = rotation_z*rotation_x*rotation_y; //for coffee box, milkbox
      //pcl::transformPointCloud(*ptr_model_old, *ptr_model_old, rotation_z*rotation_x*rotation_y);  //for coffee box, milkbox
      pcd_viewer->removePointCloud("old");
      pcd_viewer->addPointCloud(ptr_model_old, "old");
      std::cout<<"Rough transformation for registration: "<<std::endl<<rough_transform<<std::endl;
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      // Create the filtering object
      pcl::VoxelGrid<pcl::PointXYZRGBA> sor;
      sor.setLeafSize(0.002f, 0.002f, 0.002f);
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_new_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      sor.setInputCloud(ptr_model_new);
      sor.filter(*ptr_model_new_filtered);
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old_filtered(new pcl::PointCloud<pcl::PointXYZRGBA>);
      sor.setInputCloud(ptr_model_old);
      sor.filter(*ptr_model_old_filtered);
      
      std::cout<<"Old Model Size After Filtering: "<<ptr_model_old_filtered->points.size()<<std::endl;
      std::cout<<"New Model Size After Filtering: "<<ptr_model_new_filtered->points.size()<<std::endl;
      
      std::cout<<"after filtering"<<std::endl;
      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_old_filtered, "old");
      pcd_viewer->addPointCloud(ptr_model_new_filtered, "new");
      std::cout<<"Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      Eigen::Matrix4f transform1, transform2;
      double register_accuracy1, register_accuracy2;
  
      //register A to B
      Eigen::Matrix4f inv_transform = align_point_clouds_using_icp_plus_color_between_models(ptr_model_new_filtered, ptr_model_old_filtered, Eigen::Matrix4f::Identity(), register_accuracy1);
      transform1 = inv_transform.inverse();
      std::cout<<"register_accuracy1: "<<register_accuracy1<<std::endl;
  
      //register B to A
      transform2 = align_point_clouds_using_icp_plus_color_between_models(ptr_model_old_filtered, ptr_model_new_filtered, Eigen::Matrix4f::Identity(), register_accuracy2);
      std::cout<<"register_accuracy2: "<<register_accuracy2<<std::endl;
    
      Eigen::Matrix4f transform_accurate = Eigen::Matrix4f::Identity();
      //choose the better one
      transform_accurate = (register_accuracy1 < register_accuracy2) ? transform1 : transform2;
      transform_accurate = transform1;

      std::cout<<"Accurate transformation from registration: "<<std::endl<<transform_accurate<<std::endl;
      
      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr ptr_model_old_transformed_2nd(new pcl::PointCloud<pcl::PointXYZRGBA>);
      pcl::transformPointCloud(*ptr_model_old, *ptr_model_old_transformed_2nd, transform_accurate);
      *ptr_model_new += *ptr_model_old_transformed_2nd;
      //transform the point cloud of the model to the original CS of ptr_model_new
      pcl::transformPointCloud(*ptr_model_new, *ptr_model_new, bb_transform.inverse()*XY_transform.inverse());
      pcl::io::savePCDFileASCII("final_refined_model.pcd", *ptr_model_new);
      
      Eigen::Matrix4f transform_between_original_models = bb_transform.inverse() * XY_transform.inverse() * transform_accurate * rough_transform * XY_transform * bb_transform;
      std::cout<<"Transformation between original models: "<<std::endl<<transform_between_original_models<<std::endl;
      
      std::cout<<"show the complete model"<<std::endl;
      pcd_viewer->removePointCloud("old");
      pcd_viewer->removePointCloud("new");
      pcd_viewer->addPointCloud(ptr_model_new, "new");
      std::cout<<"Finish the registration, Press q to continue"<<std::endl;
      pcd_viewer->spin();
      
      //transform the object model to the bounding box coordinate system
      //visualize the point cloud in the PCA axes
      pcl::visualization::PCLVisualizer *pcd_viewer_bb;
      pcd_viewer_bb = new pcl::visualization::PCLVisualizer("Show the object model in bounding box axes");
      //pcd_viewer_bb->addCoordinateSystem(0.3);
      //pcd_viewer_bb->initCameraParameters(); 

      obtain_oriented_bounding_box_on_XY_plane(ptr_model_new, bbox_centroid, bbox_directions,
                                               length_1st, length_2nd, length_3rd);
                                               
      const Eigen::Quaternionf bbox_quaternion(bbox_directions); 
      pcd_viewer_bb->addPointCloud(ptr_model_new, "final model in robot cs");
      pcd_viewer_bb->addCube(bbox_centroid, bbox_quaternion, length_1st, length_2nd, length_3rd, "bbox");
      PCL_INFO("Press q to continue.\n");
      pcd_viewer_bb->spin();
      
      break;
    }
	default:
	  break;
  }

  return 0;
}
